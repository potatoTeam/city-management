import {BasicColumn} from '/@/components/Table';
import {FormSchema} from '/@/components/Table';
import { rules} from '/@/utils/helper/validator';
import { render } from '/@/utils/common/renderUtils';
//列表数据
export const columns: BasicColumn[] = [
  {
    title: '区划类型',
    align: 'center',
    dataIndex: 'areaType_dictText'
  },
  {
    title: '区划名称',
    align: 'left',
    dataIndex: 'areaName'
  },
  {
    title: '编码',
    align: 'center',
    dataIndex: 'areaNo'
  },
  {
    title: '显示顺序',
    align: 'center',
    dataIndex: 'orderNum'
  },
  {
    title: '状态',
    align: 'center',
    dataIndex: 'status_dictText'
  },
];

//查询数据
export const searchFormSchema: FormSchema[] = [
  {
    label: "区划名称",
    field: "areaName",
    component: 'Input',
    //colProps: {span: 6},
  },
  {
    label: "编码",
    field: "areaNo",
    component: 'Input',
    //colProps: {span: 6},
  },
];

//表单数据
export const formSchema: FormSchema[] = [
  {
    label: '区划类型',
    field: 'areaType',
    component: 'JDictSelectTag',
    componentProps:{
        dictCode: "area_type"
     },
    dynamicRules: ({ model, schema }) => {
      return [
              { required: true, message: '请输入区划类型!' },
             ];
    },
  },
  {
    label: '区划名称',
    field: 'areaName',
    component: 'Input',
    dynamicRules: ({ model, schema }) => {
      return [
              { required: true, message: '请输入区划名称!' },
             ];
    },
  },
  {
    label: '编码',
    field: 'areaNo',
    component: 'Input',
    dynamicRules: ({ model, schema }) => {
      return [
              { required: true, message: '请输入编码!' },
             ];
    },
  },
  {
    label: '显示顺序',
    field: 'orderNum',
    component: 'InputNumber',
  },
  {
    label: '状态',
    field: 'status',
    component: 'JDictSelectTag',
    componentProps:{
        dictCode: "common_status"
     },
    dynamicRules: ({ model, schema }) => {
      return [
              { required: true, message: '请输入状态!' },
             ];
    },
  },
  {
    label: '父级节点',
    field: 'parentId',
    component: 'JTreeSelect',
    componentProps: {
      dict: "b_area,area_name,id",
      pidField: "parent_id",
      pidValue: "0",
      hasChildField: "has_child",
    },
  },
	// TODO 主键隐藏字段，目前写死为ID
  {
    label: '',
    field: 'id',
    component: 'Input',
    show: false,
  },
];
