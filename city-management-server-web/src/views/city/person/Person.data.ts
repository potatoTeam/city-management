import {BasicColumn} from '/@/components/Table';
import {FormSchema} from '/@/components/Table';
import { rules} from '/@/utils/helper/validator';
import { render } from '/@/utils/common/renderUtils';
//列表数据
export const columns: BasicColumn[] = [
  {
    title: '小区编号',
    align: "center",
    dataIndex: 'townNo'
  },
  {
    title: '小区名称',
    align: "center",
    dataIndex: 'townName'
  },
  {
    title: '姓名',
    align: "center",
    dataIndex: 'name'
  },
  {
    title: '性别',
    align: "center",
    dataIndex: 'sex_dictText'
  },
  {
    title: '政治面貌',
    align: "center",
    dataIndex: 'zzmm_dictText'
  },
  {
    title: '证件类型',
    align: "center",
    dataIndex: 'cardType_dictText'
  },
  {
    title: '证件号码',
    align: "center",
    dataIndex: 'idNumber'
  },
  {
    title: '出生日期',
    align: "center",
    dataIndex: 'bornDate',
    customRender:({text}) =>{
      return !text?"":(text.length>10?text.substr(0,10):text);
    },
  },
  {
    title: '身份证地址',
    align: "center",
    dataIndex: 'idCardAddr'
  },
  {
    title: '职业',
    align: "center",
    dataIndex: 'job'
  },
  {
    title: '楼栋',
    align: "center",
    dataIndex: 'buildName'
  },
  {
    title: '房屋',
    align: "center",
    dataIndex: 'houseName'
  },
  {
    title: '人员类型',
    align: "center",
    dataIndex: 'personType_dictText'
  },
  {
    title: '是否房主',
    align: "center",
    dataIndex: 'isOwner_dictText'
  },
  {
    title: '是否负责人',
    align: "center",
    dataIndex: 'isLead_dictText'
  },
  {
    title: '联系电话',
    align: "center",
    dataIndex: 'phone'
  },
  {
    title: '照片',
    align: "center",
    dataIndex: 'photo',
    customRender: render.renderImage,
  },
  {
    title: '新大陆CTID认证bid',
    align: "center",
    dataIndex: 'bid'
  },
  {
    title: 'CTID认证(0认证1不认证)',
    align: "center",
    dataIndex: 'ctidFlag_dictText'
  },
  {
    title: '车牌号码',
    align: "center",
    dataIndex: 'carNumber'
  },
  {
    title: '是否特殊人员',
    align: "center",
    dataIndex: 'isSpecial_dictText'
  },
  {
    title: '特殊情况说明',
    align: "center",
    dataIndex: 'specialInfo'
  },
  {
    title: '关注类型',
    align: "center",
    dataIndex: 'stare_dictText'
  },
  {
    title: '关怀类型',
    align: "center",
    dataIndex: 'care_dictText'
  },
  {
    title: '关怀原因',
    align: "center",
    dataIndex: 'careInfo'
  },
  {
    title: '登记方式',
    align: "center",
    dataIndex: 'signWay_dictText'
  },
  {
    title: '状态',
    align: "center",
    dataIndex: 'status_dictText'
  },
  {
    title: '备注',
    align: "center",
    dataIndex: 'remark'
  },
];

//查询数据
export const searchFormSchema: FormSchema[] = [
  {
    label: "小区编号",
    field: 'townNo',
    component: 'JPopup',
    componentProps: ({ formActionType }) => {
        const {setFieldsValue} = formActionType;
        return{
            setFieldsValue:setFieldsValue,
            code:"b_town",
            fieldConfig: [
                { source: 'town_no', target: 'townNo' },
                { source: 'town_name', target: 'townName' },
            ],
            multi:true
        }
    },

    //colProps: {span: 6},
  },
  {
    label: "小区名称",
    field: 'townName',
    component: 'Input',
    //colProps: {span: 6},
  },
  {
    label: "姓名",
    field: 'name',
    component: 'Input',
    //colProps: {span: 6},
  },
  {
    label: "性别",
    field: 'sex',
    component: 'JDictSelectTag',
    componentProps:{
      dictCode: "info_sex"
    },
    //colProps: {span: 6},
  },
  {
    label: "政治面貌",
    field: 'zzmm',
    component: 'JDictSelectTag',
    componentProps:{
      dictCode: "info_zzmm"
    },
    //colProps: {span: 6},
  },
  {
    label: "证件类型",
    field: 'cardType',
    component: 'JDictSelectTag',
    componentProps:{
      dictCode: "info_card_type"
    },
    //colProps: {span: 6},
  },
  {
    label: "证件号码",
    field: 'idNumber',
    component: 'Input',
    //colProps: {span: 6},
  },
  {
    label: "出生日期",
    field: "bornDate",
    component: 'RangePicker',
    //colProps: {span: 6},
  },
  {
    label: "身份证地址",
    field: 'idCardAddr',
    component: 'Input',
    //colProps: {span: 6},
  },
  {
    label: "职业",
    field: 'job',
    component: 'Input',
    //colProps: {span: 6},
  },
  {
    label: "楼栋",
    field: 'buildName',
    component: 'Input',
    //colProps: {span: 6},
  },
  {
    label: "房屋",
    field: 'houseName',
    component: 'Input',
    //colProps: {span: 6},
  },
  {
    label: "人员类型",
    field: 'personType',
    component: 'JDictSelectTag',
    componentProps:{
      dictCode: "person_type"
    },
    //colProps: {span: 6},
  },
  {
    label: "是否房主",
    field: 'isOwner',
    component: 'JDictSelectTag',
    componentProps:{
      dictCode: "yes_no_status"
    },
    //colProps: {span: 6},
  },
  {
    label: "是否负责人",
    field: 'isLead',
    component: 'JDictSelectTag',
    componentProps:{
      dictCode: "yes_no_status"
    },
    //colProps: {span: 6},
  },
  {
    label: "联系电话",
    field: 'phone',
    component: 'Input',
    //colProps: {span: 6},
  },
  {
    label: "新大陆CTID认证bid",
    field: 'bid',
    component: 'Input',
    //colProps: {span: 6},
  },
  {
    label: "CTID认证(0认证1不认证)",
    field: 'ctidFlag',
    component: 'JDictSelectTag',
    componentProps:{
      dictCode: "ctid_flag"
    },
    //colProps: {span: 6},
  },
  {
    label: "车牌号码",
    field: 'carNumber',
    component: 'Input',
    //colProps: {span: 6},
  },
  {
    label: "是否特殊人员",
    field: 'isSpecial',
    component: 'JDictSelectTag',
    componentProps:{
      dictCode: "yes_no_status"
    },
    //colProps: {span: 6},
  },
  {
    label: "关注类型",
    field: 'stare',
    component: 'JDictSelectTag',
    componentProps:{
      dictCode: "info_stare_type"
    },
    //colProps: {span: 6},
  },
  {
    label: "关怀类型",
    field: 'care',
    component: 'JDictSelectTag',
    componentProps:{
      dictCode: "info_care_type"
    },
    //colProps: {span: 6},
  },
  {
    label: "登记方式",
    field: 'signWay',
    component: 'JDictSelectTag',
    componentProps:{
      dictCode: "sign_way"
    },
    //colProps: {span: 6},
  },
  {
    label: "状态",
    field: 'status',
    component: 'JDictSelectTag',
    componentProps:{
      dictCode: "common_status"
    },
    //colProps: {span: 6},
  },
];

//表单数据
export const formSchema: FormSchema[] = [
  {
    label: '小区编号',
    field: 'townNo',
    component: 'JPopup',
    componentProps: ({ formActionType }) => {
        const {setFieldsValue} = formActionType;
        return{
            setFieldsValue:setFieldsValue,
            code:"b_town",
            fieldConfig: [
                { source: 'town_no', target: 'townNo' },
                { source: 'town_name', target: 'townName' },
            ],
            multi:true
        }
    },

  },
  {
    label: '小区名称',
    field: 'townName',
    component: 'Input',
  },
  {
    label: '姓名',
    field: 'name',
    component: 'Input',
    dynamicRules: ({model,schema}) => {
      return [
              { required: true, message: '请输入姓名!'},
             ];
    },
  },
  {
    label: '性别',
    field: 'sex',
    component: 'JDictSelectTag',
    componentProps:{
      dictCode: "info_sex"
    },
  },
  {
    label: '政治面貌',
    field: 'zzmm',
    component: 'JDictSelectTag',
    componentProps:{
      dictCode: "info_zzmm"
    },
  },
  {
    label: '证件类型',
    field: 'cardType',
    component: 'JDictSelectTag',
    componentProps:{
      dictCode: "info_card_type"
    },
  },
  {
    label: '证件号码',
    field: 'idNumber',
    component: 'Input',
  },
  {
    label: '出生日期',
    field: 'bornDate',
    component: 'DatePicker',
  },
  {
    label: '身份证地址',
    field: 'idCardAddr',
    component: 'Input',
  },
  {
    label: '职业',
    field: 'job',
    component: 'Input',
  },
  {
    label: '楼栋',
    field: 'buildName',
    component: 'Input',
  },
  {
    label: '房屋',
    field: 'houseName',
    component: 'InputTextArea',
  },
  {
    label: '人员类型',
    field: 'personType',
    component: 'JDictSelectTag',
    componentProps:{
      dictCode: "person_type"
    },
  },
  {
    label: '是否房主',
    field: 'isOwner',
    component: 'JDictSelectTag',
    componentProps:{
      dictCode: "yes_no_status"
    },
  },
  {
    label: '是否负责人',
    field: 'isLead',
    component: 'JDictSelectTag',
    componentProps:{
      dictCode: "yes_no_status"
    },
  },
  {
    label: '联系电话',
    field: 'phone',
    component: 'Input',
  },
  {
    label: '照片',
    field: 'photo',
    component: 'JImageUpload',
    componentProps:{
    },
  },
  {
    label: '新大陆CTID认证bid',
    field: 'bid',
    component: 'Input',
  },
  {
    label: 'CTID认证(0认证1不认证)',
    field: 'ctidFlag',
    component: 'JDictSelectTag',
    componentProps:{
      dictCode: "ctid_flag"
    },
  },
  {
    label: '车牌号码',
    field: 'carNumber',
    component: 'Input',
  },
  {
    label: '是否特殊人员',
    field: 'isSpecial',
    component: 'JDictSelectTag',
    componentProps:{
      dictCode: "yes_no_status"
    },
  },
  {
    label: '特殊情况说明',
    field: 'specialInfo',
    component: 'Input',
  },
  {
    label: '关注类型',
    field: 'stare',
    component: 'JDictSelectTag',
    componentProps:{
      dictCode: "info_stare_type"
    },
  },
  {
    label: '关怀类型',
    field: 'care',
    component: 'JDictSelectTag',
    componentProps:{
      dictCode: "info_care_type"
    },
  },
  {
    label: '关怀原因',
    field: 'careInfo',
    component: 'Input',
  },
  {
    label: '登记方式',
    field: 'signWay',
    component: 'JDictSelectTag',
    componentProps:{
      dictCode: "sign_way"
    },
  },
  {
    label: '状态',
    field: 'status',
    component: 'JDictSelectTag',
    componentProps:{
      dictCode: "common_status"
    },
  },
  {
    label: '备注',
    field: 'remark',
    component: 'Input',
  },
	// TODO 主键隐藏字段，目前写死为ID
  {
    label: '',
    field: 'id',
    component: 'Input',
    show: false,
  },
];
