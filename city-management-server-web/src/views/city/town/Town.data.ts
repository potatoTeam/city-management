import {BasicColumn} from '/@/components/Table';
import {FormSchema} from '/@/components/Table';
import { rules} from '/@/utils/helper/validator';
import { render } from '/@/utils/common/renderUtils';
//列表数据
export const columns: BasicColumn[] = [
  {
    title: '所属区划',
    align: "center",
    dataIndex: 'areaId_dictText'
  },
  {
    title: '市局小区编码',
    align: "center",
    dataIndex: 'ssxqbm'
  },
  {
    title: '市局停车场编码',
    align: "center",
    dataIndex: 'tccbh'
  },
  {
    title: '政法委小区标识',
    align: "center",
    dataIndex: 'ztxqbm'
  },
  {
    title: '辖区公安机构',
    align: "center",
    dataIndex: 'psNo'
  },
  {
    title: '公安机构标识',
    align: "center",
    dataIndex: 'psLevel'
  },
  {
    title: '小区类别',
    align: "center",
    dataIndex: 'townType_dictText'
  },
  {
    title: '小区编码',
    align: "center",
    dataIndex: 'townNo'
  },
  {
    title: '小区名称',
    align: "center",
    dataIndex: 'townName'
  },
  {
    title: '小区门牌号',
    align: "center",
    dataIndex: 'townSn'
  },
  {
    title: '地址标准码',
    align: "center",
    dataIndex: 'addrCode'
  },
  {
    title: 'ZONE编码',
    align: "center",
    dataIndex: 'zoneBm'
  },
  {
    title: '数据归属单位代码',
    align: "center",
    dataIndex: 'gajgjgdm'
  },
  {
    title: '数据归属单位名称',
    align: "center",
    dataIndex: 'gajgjgmc'
  },
  {
    title: '警务区编号',
    align: "center",
    dataIndex: 'jwwgdm'
  },
  {
    title: '警务区名称',
    align: "center",
    dataIndex: 'jwwgmc'
  },
  {
    title: '地名编号',
    align: "center",
    dataIndex: 'dmdm'
  },
  {
    title: '地名',
    align: "center",
    dataIndex: 'dmmc'
  },
  {
    title: '负责人姓名',
    align: "center",
    dataIndex: 'leadName'
  },
  {
    title: '负责人电话',
    align: "center",
    dataIndex: 'leadPhone'
  },
  {
    title: '经度',
    align: "center",
    dataIndex: 'mapx'
  },
  {
    title: '纬度',
    align: "center",
    dataIndex: 'mapy'
  },
  {
    title: '边界点',
    align: "center",
    dataIndex: 'points'
  },
  {
    title: '附件文件',
    align: "center",
    dataIndex: 'resFiles'
  },
  {
    title: '附件名称',
    align: "center",
    dataIndex: 'resNames'
  },
  {
    title: '市局上报标识',
    align: "center",
    dataIndex: 'tcStatus_dictText'
  },
  {
    title: '政法委上报标识',
    align: "center",
    dataIndex: 'ztStatus_dictText'
  },
  {
    title: '闽清上报标识',
    align: "center",
    dataIndex: 'mqStatus_dictText'
  },
  {
    title: '状态(0无效1有效)',
    align: "center",
    dataIndex: 'status_dictText'
  },
  {
    title: '创建时间',
    align: "center",
    dataIndex: 'createTime'
  },
  {
    title: '更新时间',
    align: "center",
    dataIndex: 'updateTime'
  },
  {
    title: '备注',
    align: "center",
    dataIndex: 'remark'
  },
];

//查询数据
export const searchFormSchema: FormSchema[] = [
  {
    label: "所属区划",
    field: 'areaId',
    component: 'Input',
    //colProps: {span: 6},
  },
  {
    label: "市局小区编码",
    field: 'ssxqbm',
    component: 'Input',
    //colProps: {span: 6},
  },
  {
    label: "市局停车场编码",
    field: 'tccbh',
    component: 'Input',
    //colProps: {span: 6},
  },
  {
    label: "政法委小区标识",
    field: 'ztxqbm',
    component: 'Input',
    //colProps: {span: 6},
  },
  {
    label: "辖区公安机构",
    field: 'psNo',
    component: 'Input',
    //colProps: {span: 6},
  },
  {
    label: "公安机构标识",
    field: 'psLevel',
    component: 'Input',
    //colProps: {span: 6},
  },
  {
    label: "小区类别",
    field: 'townType',
    component: 'JDictSelectTag',
    componentProps:{
      dictCode: "town_type"
    },
    //colProps: {span: 6},
  },
  {
    label: "小区编码",
    field: 'townNo',
    component: 'Input',
    //colProps: {span: 6},
  },
  {
    label: "小区名称",
    field: 'townName',
    component: 'Input',
    //colProps: {span: 6},
  },
  {
    label: "小区门牌号",
    field: 'townSn',
    component: 'Input',
    //colProps: {span: 6},
  },
  {
    label: "地址标准码",
    field: 'addrCode',
    component: 'Input',
    //colProps: {span: 6},
  },
  {
    label: "ZONE编码",
    field: 'zoneBm',
    component: 'Input',
    //colProps: {span: 6},
  },
  {
    label: "数据归属单位代码",
    field: 'gajgjgdm',
    component: 'Input',
    //colProps: {span: 6},
  },
  {
    label: "数据归属单位名称",
    field: 'gajgjgmc',
    component: 'Input',
    //colProps: {span: 6},
  },
  {
    label: "警务区编号",
    field: 'jwwgdm',
    component: 'Input',
    //colProps: {span: 6},
  },
  {
    label: "警务区名称",
    field: 'jwwgmc',
    component: 'Input',
    //colProps: {span: 6},
  },
  {
    label: "地名编号",
    field: 'dmdm',
    component: 'Input',
    //colProps: {span: 6},
  },
  {
    label: "地名",
    field: 'dmmc',
    component: 'Input',
    //colProps: {span: 6},
  },
  {
    label: "负责人姓名",
    field: 'leadName',
    component: 'Input',
    //colProps: {span: 6},
  },
  {
    label: "负责人电话",
    field: 'leadPhone',
    component: 'Input',
    //colProps: {span: 6},
  },
  {
    label: "市局上报标识",
    field: 'tcStatus',
    component: 'JDictSelectTag',
    componentProps:{
      dictCode: "report_status"
    },
    //colProps: {span: 6},
  },
  {
    label: "政法委上报标识",
    field: 'ztStatus',
    component: 'JDictSelectTag',
    componentProps:{
      dictCode: "report_status"
    },
    //colProps: {span: 6},
  },
  {
    label: "闽清上报标识",
    field: 'mqStatus',
    component: 'JDictSelectTag',
    componentProps:{
      dictCode: "report_status"
    },
    //colProps: {span: 6},
  },
  {
    label: "状态(0无效1有效)",
    field: 'status',
    component: 'JDictSelectTag',
    componentProps:{
      dictCode: "common_status"
    },
    //colProps: {span: 6},
  },
];

//表单数据
export const formSchema: FormSchema[] = [
  {
    label: '所属区划',
    field: 'areaId',
    component: 'JTreeSelect',
    componentProps:{
      dict: "b_area,area_name,id",
      pidValue: "0",
    },
  },
  {
    label: '市局小区编码',
    field: 'ssxqbm',
    component: 'Input',
  },
  {
    label: '市局停车场编码',
    field: 'tccbh',
    component: 'Input',
  },
  {
    label: '政法委小区标识',
    field: 'ztxqbm',
    component: 'Input',
  },
  {
    label: '辖区公安机构',
    field: 'psNo',
    component: 'Input',
  },
  {
    label: '公安机构标识',
    field: 'psLevel',
    component: 'Input',
  },
  {
    label: '小区类别',
    field: 'townType',
    component: 'JDictSelectTag',
    componentProps:{
      dictCode: "town_type"
    },
  },
  {
    label: '小区编码',
    field: 'townNo',
    component: 'Input',
  },
  {
    label: '小区名称',
    field: 'townName',
    component: 'Input',
    dynamicRules: ({model,schema}) => {
      return [
              { required: true, message: '请输入小区名称!'},
             ];
    },
  },
  {
    label: '小区门牌号',
    field: 'townSn',
    component: 'Input',
  },
  {
    label: '地址标准码',
    field: 'addrCode',
    component: 'Input',
  },
  {
    label: 'ZONE编码',
    field: 'zoneBm',
    component: 'Input',
  },
  {
    label: '数据归属单位代码',
    field: 'gajgjgdm',
    component: 'Input',
  },
  {
    label: '数据归属单位名称',
    field: 'gajgjgmc',
    component: 'Input',
  },
  {
    label: '警务区编号',
    field: 'jwwgdm',
    component: 'Input',
  },
  {
    label: '警务区名称',
    field: 'jwwgmc',
    component: 'Input',
  },
  {
    label: '地名编号',
    field: 'dmdm',
    component: 'Input',
  },
  {
    label: '地名',
    field: 'dmmc',
    component: 'Input',
  },
  {
    label: '负责人姓名',
    field: 'leadName',
    component: 'Input',
  },
  {
    label: '负责人电话',
    field: 'leadPhone',
    component: 'Input',
  },
  {
    label: '经度',
    field: 'mapx',
    component: 'Input',
  },
  {
    label: '纬度',
    field: 'mapy',
    component: 'Input',
  },
  {
    label: '边界点',
    field: 'points',
    component: 'Input',
  },
  {
    label: '附件文件',
    field: 'resFiles',
    component: 'JUpload',
    componentProps:{
     },
  },
  {
    label: '附件名称',
    field: 'resNames',
    component: 'Input',
  },
  {
    label: '市局上报标识',
    field: 'tcStatus',
    component: 'JDictSelectTag',
    componentProps:{
      dictCode: "report_status"
    },
  },
  {
    label: '政法委上报标识',
    field: 'ztStatus',
    component: 'JDictSelectTag',
    componentProps:{
      dictCode: "report_status"
    },
  },
  {
    label: '闽清上报标识',
    field: 'mqStatus',
    component: 'JDictSelectTag',
    componentProps:{
      dictCode: "report_status"
    },
  },
  {
    label: '状态(0无效1有效)',
    field: 'status',
    component: 'JDictSelectTag',
    componentProps:{
      dictCode: "common_status"
    },
  },
  {
    label: '备注',
    field: 'remark',
    component: 'Input',
  },
	// TODO 主键隐藏字段，目前写死为ID
  {
    label: '',
    field: 'id',
    component: 'Input',
    show: false,
  },
];
