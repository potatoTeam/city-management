import {BasicColumn} from '/@/components/Table';
import {FormSchema} from '/@/components/Table';
import { rules} from '/@/utils/helper/validator';
import { render } from '/@/utils/common/renderUtils';
//列表数据
export const columns: BasicColumn[] = [
  {
    title: '小区编号',
    align: "center",
    dataIndex: 'townNo'
  },
  {
    title: '小区名称',
    align: "center",
    dataIndex: 'townName'
  },
  {
    title: '门禁编号',
    align: "center",
    dataIndex: 'entNo'
  },
  {
    title: '门禁名称',
    align: "center",
    dataIndex: 'entName'
  },
  {
    title: '门禁平台',
    align: "center",
    dataIndex: 'entPlatform_dictText'
  },
  {
    title: '人员ID',
    align: "center",
    dataIndex: 'personId'
  },
  {
    title: '人员姓名',
    align: "center",
    dataIndex: 'personName'
  },
  {
    title: '证件号码',
    align: "center",
    dataIndex: 'idNumber'
  },
  {
    title: '人员类型',
    align: "center",
    dataIndex: 'personType_dictText'
  },
  {
    title: '人员相片',
    align: "center",
    dataIndex: 'personPhoto',
    customRender: render.renderImage,
  },
  {
    title: '通行时间',
    align: "center",
    dataIndex: 'accessTime'
  },
  {
    title: '通行类型',
    align: "center",
    dataIndex: 'accessType_dictText'
  },
  {
    title: '比对方式',
    align: "center",
    dataIndex: 'cmpType_dictText'
  },
  {
    title: '比对分值',
    align: "center",
    dataIndex: 'cmpScore'
  },
  {
    title: '比对结果',
    align: "center",
    dataIndex: 'cmpResult_dictText'
  },
  {
    title: '体温',
    align: "center",
    dataIndex: 'temperature'
  },
  {
    title: '背景图',
    align: "center",
    dataIndex: 'backImg',
    customRender: render.renderImage,
  },
  {
    title: '通行相片',
    align: "center",
    dataIndex: 'captureImg',
    customRender: render.renderImage,
  },
  {
    title: 'tc上报状态',
    align: "center",
    dataIndex: 'statusPost_dictText'
  },
  {
    title: 'zt上报状态',
    align: "center",
    dataIndex: 'statusZt_dictText'
  },
  {
    title: '状态(0无效1有效)',
    align: "center",
    dataIndex: 'status_dictText'
  },
  {
    title: '创建时间',
    align: "center",
    dataIndex: 'createTime'
  },
  {
    title: '备注',
    align: "center",
    dataIndex: 'remark'
  },
];

//查询数据
export const searchFormSchema: FormSchema[] = [
  {
    label: "小区编号",
    field: 'townNo',
    component: 'JPopup',
    componentProps: ({ formActionType }) => {
        const {setFieldsValue} = formActionType;
        return{
            setFieldsValue:setFieldsValue,
            code:"b_town",
            fieldConfig: [
                { source: 'town_no', target: 'townNo' },
                { source: 'town_name', target: 'townName' },
            ],
            multi:true
        }
    },

    //colProps: {span: 6},
  },
  {
    label: "小区名称",
    field: 'townName',
    component: 'Input',
    //colProps: {span: 6},
  },
  {
    label: "门禁编号",
    field: 'entNo',
    component: 'Input',
    //colProps: {span: 6},
  },
  {
    label: "门禁名称",
    field: 'entName',
    component: 'Input',
    //colProps: {span: 6},
  },
  {
    label: "门禁平台",
    field: 'entPlatform',
    component: 'JDictSelectTag',
    componentProps:{
      dictCode: "ent_platform"
    },
    //colProps: {span: 6},
  },
  {
    label: "人员ID",
    field: 'personId',
    component: 'JPopup',
    componentProps: ({ formActionType }) => {
        const {setFieldsValue} = formActionType;
        return{
            setFieldsValue:setFieldsValue,
            code:"b_person",
            fieldConfig: [
                { source: 'id', target: 'personId' },
                { source: 'name', target: 'personName' },
                { source: 'id_number', target: 'idNumber' },
            ],
            multi:true
        }
    },

    //colProps: {span: 6},
  },
  {
    label: "人员姓名",
    field: 'personName',
    component: 'Input',
    //colProps: {span: 6},
  },
  {
    label: "证件号码",
    field: 'idNumber',
    component: 'Input',
    //colProps: {span: 6},
  },
  {
    label: "人员类型",
    field: 'personType',
    component: 'JDictSelectTag',
    componentProps:{
      dictCode: "access_person_type"
    },
    //colProps: {span: 6},
  },
  {
    label: "通行时间",
    field: "accessTime",
    component: 'RangePicker',
    componentProps: {
      showTime: true,
    },
    //colProps: {span: 6},
  },
  {
    label: "通行类型",
    field: 'accessType',
    component: 'JDictSelectTag',
    componentProps:{
      dictCode: "access_type"
    },
    //colProps: {span: 6},
  },
  {
    label: "比对方式",
    field: 'cmpType',
    component: 'JDictSelectTag',
    componentProps:{
      dictCode: "cmp_type"
    },
    //colProps: {span: 6},
  },
  {
    label: "比对结果",
    field: 'cmpResult',
    component: 'JDictSelectTag',
    componentProps:{
      dictCode: "cmp_result"
    },
    //colProps: {span: 6},
  },
  {
    label: "tc上报状态",
    field: 'statusPost',
    component: 'JDictSelectTag',
    componentProps:{
      dictCode: "report_status"
    },
    //colProps: {span: 6},
  },
  {
    label: "zt上报状态",
    field: 'statusZt',
    component: 'JDictSelectTag',
    componentProps:{
      dictCode: "report_status"
    },
    //colProps: {span: 6},
  },
  {
    label: "状态(0无效1有效)",
    field: 'status',
    component: 'JDictSelectTag',
    componentProps:{
      dictCode: "common_status"
    },
    //colProps: {span: 6},
  },
];

//表单数据
export const formSchema: FormSchema[] = [
  {
    label: '小区编号',
    field: 'townNo',
    component: 'JPopup',
    componentProps: ({ formActionType }) => {
        const {setFieldsValue} = formActionType;
        return{
            setFieldsValue:setFieldsValue,
            code:"b_town",
            fieldConfig: [
                { source: 'town_no', target: 'townNo' },
                { source: 'town_name', target: 'townName' },
            ],
            multi:true
        }
    },

  },
  {
    label: '小区名称',
    field: 'townName',
    component: 'Input',
  },
  {
    label: '门禁编号',
    field: 'entNo',
    component: 'Input',
    dynamicRules: ({model,schema}) => {
      return [
              { required: true, message: '请输入门禁编号!'},
             ];
    },
  },
  {
    label: '门禁名称',
    field: 'entName',
    component: 'Input',
  },
  {
    label: '门禁平台',
    field: 'entPlatform',
    component: 'JDictSelectTag',
    componentProps:{
      dictCode: "ent_platform"
    },
  },
  {
    label: '人员ID',
    field: 'personId',
    component: 'JPopup',
    componentProps: ({ formActionType }) => {
        const {setFieldsValue} = formActionType;
        return{
            setFieldsValue:setFieldsValue,
            code:"b_person",
            fieldConfig: [
                { source: 'id', target: 'personId' },
                { source: 'name', target: 'personName' },
                { source: 'id_number', target: 'idNumber' },
            ],
            multi:true
        }
    },

  },
  {
    label: '人员姓名',
    field: 'personName',
    component: 'Input',
  },
  {
    label: '证件号码',
    field: 'idNumber',
    component: 'Input',
  },
  {
    label: '人员类型',
    field: 'personType',
    component: 'JDictSelectTag',
    componentProps:{
      dictCode: "access_person_type"
    },
  },
  {
    label: '人员相片',
    field: 'personPhoto',
    component: 'JImageUpload',
    componentProps:{
    },
  },
  {
    label: '通行时间',
    field: 'accessTime',
    component: 'DatePicker',
    componentProps: {
      showTime: true,
      valueFormat: 'YYYY-MM-DD HH:mm:ss'
    },
  },
  {
    label: '通行类型',
    field: 'accessType',
    component: 'JDictSelectTag',
    componentProps:{
      dictCode: "access_type"
    },
  },
  {
    label: '比对方式',
    field: 'cmpType',
    component: 'JDictSelectTag',
    componentProps:{
      dictCode: "cmp_type"
    },
  },
  {
    label: '比对分值',
    field: 'cmpScore',
    component: 'InputNumber',
  },
  {
    label: '比对结果',
    field: 'cmpResult',
    component: 'JDictSelectTag',
    componentProps:{
      dictCode: "cmp_result"
    },
  },
  {
    label: '体温',
    field: 'temperature',
    component: 'InputNumber',
  },
  {
    label: '背景图',
    field: 'backImg',
    component: 'JImageUpload',
    componentProps:{
    },
  },
  {
    label: '通行相片',
    field: 'captureImg',
    component: 'JImageUpload',
    componentProps:{
    },
  },
  {
    label: 'tc上报状态',
    field: 'statusPost',
    component: 'JDictSelectTag',
    componentProps:{
      dictCode: "report_status"
    },
  },
  {
    label: 'zt上报状态',
    field: 'statusZt',
    component: 'JDictSelectTag',
    componentProps:{
      dictCode: "report_status"
    },
  },
  {
    label: '状态(0无效1有效)',
    field: 'status',
    component: 'JDictSelectTag',
    componentProps:{
      dictCode: "common_status"
    },
  },
  {
    label: '备注',
    field: 'remark',
    component: 'Input',
  },
	// TODO 主键隐藏字段，目前写死为ID
  {
    label: '',
    field: 'id',
    component: 'Input',
    show: false,
  },
];
