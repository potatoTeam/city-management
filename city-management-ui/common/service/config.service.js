let BASE_URL = ''


if (process.env.NODE_ENV == 'development') {
    BASE_URL = 'http://localhost:8080/jeecg-boot' // 开发环境
} else {
	// BASE_URL = 'https://502y400q34.yicp.fun/jeecg-boot' // 生产环境
	BASE_URL = 'https://79aef9d1.r12.cpolar.top/jeecg-boot' // 生产环境
	
}
let staticDomainURL = BASE_URL+ '/sys/common/static';

const configService = {
	apiUrl: BASE_URL,
	staticDomainURL: staticDomainURL
};

export default configService
