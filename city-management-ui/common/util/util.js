/** 判断是否是OAuth2APP环境 */
export function isOAuth2AppEnv() {
    // #ifdef H5
    return /wxwork|dingtalk/i.test(navigator.userAgent)
    // #endif
    return false;
}

// 获取url中的参数
export const getUrlParams = (url) => {
    let result = {
        url: '',
        params: {}
    };
    let list = url.split('?');
    result.url = list[0];
    let params = list[1];
    if (params) {
        let list = params.split('&');
        list.forEach(ele => {
            let dic = ele.split('=');
            let label = dic[0];
            let value = dic[1];
            result.params[label] = value;
        });
    }
    return result;
};

export function backUrl(pages){
	//跳转
	let beforePage = pages[pages.length - 2];  //上一个页面
	let tabUrls = ['/pages/index/index'] //tab页
	// console.log(beforePage)
	if(beforePage && pages.length>1){
		if(tabUrls.indexOf(beforePage.$page.fullPath) > -1){
			uni.reLaunch({
			  url: beforePage.$page.fullPath
			})
		}else{
			uni.navigateBack({
				url: beforePage.$page.fullPath
			})
		}
		
	}else{
        uni.navigateBack({
            url: '/pages/index/index',
          });
		// uni.switchTab({
		//   url: '/pages/index/index',
		// });
	}
}