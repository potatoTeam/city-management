package com.chjim.common.domain;

import java.util.ArrayList;
import java.util.List;

public class DictAccess {
    public static String CMP_TYPE_OATHER = "0";    //人脸比对
    public static String CMP_TYPE_FACE = "2";    //人脸比对
    public static String CMP_TYPE_HEALTH = "3";    //健康码
    public static String CMP_TYPE_ID_CARD = "5";    //身份证
    public static String CMP_TYPE_QR_CARD = "6";    //通行卡扫码
    public static String CMP_TYPE_STRANGER = "9";    //陌生人

    public static String CMP_RESULT_SUCCESS = "1";    //人脸比对

    public static void init() {
        //比对模式
        List<Conf> cmpType = new ArrayList<>();
        cmpType.add(new Conf("其他", "0"));
        cmpType.add(new Conf("人证比对", "1"));
        cmpType.add(new Conf("人脸比对", "2"));
        cmpType.add(new Conf("健康码", "3"));
        cmpType.add(new Conf("身份证", "5"));
        cmpType.add(new Conf("通行卡扫码", "6"));
        cmpType.add(new Conf("陌生人", "9"));
        ConfDict.confDictMap.put("CMP_TYPE", cmpType);

        //比对结果
        List<Conf> cmpResult = new ArrayList<>();
        cmpResult.add(new Conf("比对失败", "0"));
        cmpResult.add(new Conf("比对通过", "1"));
        ConfDict.confDictMap.put("CMP_RESULT", cmpResult);
    }
}