package com.chjim.basedata.domain;

import java.util.Date;

import com.chjim.device.domain.Entrance;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.chjim.common.core.domain.BaseEntity;
import com.chjim.common.annotation.Excel;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 通行记录对象 b_access_log
 *
 * @author chjim
 */
@Data
public class AccessLog extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 记录ID
     */
    private Long recordId;

    @Excel(name = "小区编号")
    private String townNo;
    @Excel(name = "小区名称")
    private String townName;

    /**
     * 门禁编号
     */
    @Excel(name = "门禁编号")
    private String entNo;

    @Excel(name = "门禁名称")
    private String entName;

    private Short entPlatform;

    private Entrance entrance;

    /**
     * 人员ID
     */
    @Excel(name = "人员ID")
    private Long personId;

    private Person person;

    @Excel(name = "人员姓名")
    private String personName;

    private String personType;

    private String personPhoto;

    @Excel(name = "证件号码")
    private String idNumber;

    private Long residentId;

    private Long visitId;

    /**
     * 通行时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "通行时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date accessTime;

    /**
     * 通行类型
     */
    @Excel(name = "通行类型")
    private String accessType;

    @Excel(name = "比对方式")
    private String cmpType;

    @Excel(name = "比对分数")
    private Integer cmpScore;

    @Excel(name = "比对结果")
    private String cmpResult;

    /**
     * 体温
     */
    private Integer temperature;

    /**
     * 背景图
     */
    private String backImg;

    /**
     * 通行相片
     */
    private String captureImg;

//    private Integer healthCode;
//
//    private Integer antiRet;
//
//    private String antiSrc;
//
//    private Integer rnaRet;
//
//    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
//    private Date rnaTime;

    /**
     * 上报状态
     */
    private String statusPost;

    /**
     * zt上报状态
     */
    private String statusZt;

    /**
     * 状态(0无效1有效)
     */
    private String status;

    /**
     * 删除标志(0存在2删除)
     */
    private String delFlag;

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("recordId", getRecordId())
                .append("entNo", getEntNo())
                .append("personId", getPersonId())
                .append("accessTime", getAccessTime())
                .append("accessType", getAccessType())
                .append("temperature", getTemperature())
                .append("captureImg", getCaptureImg())
                .append("status", getStatus())
                .append("delFlag", getDelFlag())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .append("remark", getRemark())
                .toString();
    }
}
