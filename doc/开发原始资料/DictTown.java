package com.chjim.common.domain;

import com.chjim.common.utils.Gps;

import java.util.ArrayList;
import java.util.List;

public class DictTown {
    //区划层次
    public static final int LV_AREA_JIEZHEN = 3;    //街镇
    public static final int LV_AREA_SHEQU = 4;      //社区 园区 村

    public static String TOWN_NO_MANAGE = "Q000";
    public static String TOWN_NAME_MANAGE = "管理区";
    public static final String CONF_YES = "0";
    public static final String CONF_NO = "1";
    //默认的GPS福州
    public static final Gps GPS_FZ = new Gps(26.07737, 119.29142);

    public static final String TYPE_XY = "5";   //校园

    //===================配置====================
    public static final String[] TOWN_CONF_KEY = new String[4];

    public static void init() {
        //居民类型
        List<Conf> townType = new ArrayList<>();
        townType.add(new Conf("居民小区", "0"));
        townType.add(new Conf("机关单位", "1"));
        townType.add(new Conf("企业", "2"));
//        townType.add(new Conf("医院", "4"));
        townType.add(new Conf("校园", "5"));
        townType.add(new Conf("娱乐场所", "6"));
        townType.add(new Conf("特殊单位", "7"));
        townType.add(new Conf("景区", "8"));
        townType.add(new Conf("大小场所", "9"));
        ConfDict.confDictMap.put("TOWN_TYPE", townType);

        TOWN_CONF_KEY[0] = "LEAD_AUDIT_RESIDENT";  //房屋负责人审核住户权限，默认可以
        TOWN_CONF_KEY[1] = "RESIDE_APPEND";  //普通住户添加同住权限，默认可以
        TOWN_CONF_KEY[2] = "VISIT_ENABLE";  //是否关闭访客系统，默认开
        TOWN_CONF_KEY[3] = "CTID_ENABLE";  //CTID开启，默认开
    }

    public static String getTownCate(String townType) {
        if (TYPE_XY.equals(townType))
            return "校园";
        if ("1".equals(townType) || "7".equals(townType))
            return "单位";
        if ("2".equals(townType) || "8".equals(townType))
            return "园区";
        if ("6".equals(townType) || "9".equals(townType))
            return "场所";
        return "社区";
    }
}
