package com.chjim.basedata.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.chjim.basedata.domain.*;
import com.chjim.basedata.service.*;
import com.chjim.common.annotation.DataScope;
import com.chjim.common.config.ProjConfig;
import com.chjim.common.core.domain.AjaxResult;
import com.chjim.common.core.domain.entity.SysUser;
import com.chjim.common.domain.Constant;
import com.chjim.common.domain.DictPerson;
import com.chjim.common.domain.DictTown;
import com.chjim.common.exception.BusinessException;
import com.chjim.common.utils.*;
import com.chjim.device.domain.Entrance;
import com.chjim.device.handler.EntDeviceHandler;
import com.chjim.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.chjim.basedata.mapper.PersonMapper;
import com.chjim.common.utils.Convert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 人员Service业务层处理
 *
 * @author chjim
 */
@Service
public class PersonServiceImpl implements IPersonService {
    private static final Logger log = LoggerFactory.getLogger(PersonServiceImpl.class);

    @Autowired
    private PersonMapper personMapper;

    @Autowired
    private ITownService townService;

    @Autowired
    private ISysUserService userService;

    @Autowired
    private ICardService cardService;

    /**
     * 查询人员
     *
     * @param recordId 人员ID
     * @return 人员
     */
    @Override
    public Person selectPersonById(Long recordId) {
        return personMapper.selectPersonById(recordId);
    }

    /**
     * 查询人员列表
     *
     * @param person 人员
     * @return 人员
     */
    @Override
    public List<Person> selectPersonList(Person person) {
        List<Person> personList = personMapper.selectPersonList(person);
        for (Person vo : personList) {
            vo.setIdNumber(Utils.hideIdNumber(vo.getIdNumber()));
            vo.setPhone(Utils.hidePhone(vo.getPhone()));
        }
        return personList;
    }

    @Override
    @DataScope(townAlias = "t")
    public List<Person> authSelectPersonList(Person person) {
        List<Person> personList = personMapper.selectPersonList(person);
        for (Person vo : personList) {
            vo.setIdNumber(Utils.hideIdNumber(vo.getIdNumber()));
            vo.setPhone(Utils.hidePhone(vo.getPhone()));
        }
        return personList;
    }

    @Override
    public long getCount(Person person) {
        return personMapper.getCount(person);
    }

    @Override
    public List<HashMap<String, Object>> getStatByTown(Person person) {
        return personMapper.getStatByTown(person);
    }

    @Override
    public List<Person> selectPersonLastTenList() {
        Person person = new Person();
        person.getParams().put("limit", 10);
        return personMapper.selectPersonList(person);
    }

    @Override
    public List<Person> selectCarePerson() {
        Person person = new Person();
        person.getParams().put("is_care", 1);
        return personMapper.selectPersonList(person);
    }

    @Override
    public List<Person> selectStarePerson() {
        Person person = new Person();
        person.getParams().put("is_stare", 1);
        return personMapper.selectPersonList(person);
    }

    @Override
    public List<Person> selectPersonsByIdNumber(String idNumber) {
        return personMapper.selectPersonsByIdNumber(Convert.toStr(idNumber).toUpperCase());
    }

    @Override
    public Person selectPersonByIdNumber(String idNumber) {
        if (!VerifyUtil.baseIdNumberVerify(idNumber))
            return null;
        return personMapper.selectPersonByIdNumber(Convert.toStr(idNumber).toUpperCase());
    }

    @Override
    public Person selectByIdNumber(String idNumber) {
        if (!VerifyUtil.baseIdNumberVerify(idNumber))
            return null;
        return personMapper.selectByIdNumber(Convert.toStr(idNumber).toUpperCase());
    }

    @Override
    public Person selectPersonByPhone(String phone) {
        return personMapper.selectPersonByPhone(phone);
    }

    @Override
    public List<Person> selectPersonByResident(List<Resident> residentList) {
        List<Person> personList = new ArrayList<>();
        for (Resident resident : residentList) {
//            Person person = selectPersonById(resident.getPersonId());
            personList.add(resident.getPerson());
        }
        return personList;
    }

    @Override
    public List<Person> selectPersonNoPhotoByTown(String townNo) {
        Person person = new Person();
        person.getParams().put("noPhoto", 1);
        return personMapper.selectPersonList(person);
    }

    @Override
    public List<HashMap<String, Object>> findDupliIdNumber() {
        return personMapper.findDupliIdNumber();
    }

    /**
     * 验证是否重复
     */
    @Override
    public AjaxResult checkDuplicate(Person person, String idNumber, String phone) {
        long personId = person == null ? 0 : person.getRecordId().longValue();
        Person tmp = selectPersonByIdNumber(idNumber);
        if (tmp != null && tmp.getRecordId().longValue() != personId)
            return AjaxResult.error(DictPerson.MSG_ERR_ID_NUMBER_);

        if (VerifyUtil.phoneNumberVerify(phone)) {
            tmp = selectPersonByPhone(phone);
            if (tmp != null && tmp.getRecordId().longValue() != personId)
                return AjaxResult.error(DictPerson.MSG_ERR_PHONE_);
        }

        return AjaxResult.success();
    }

    /**
     * 基本校验
     */
    @Override
    public AjaxResult checkBase(String cardType, String idNumber, String phone) {
        if (cardType.equals(DictPerson.ID_TYPE_CHINESE) && !VerifyUtil.chineseIDVerify(idNumber))
            return AjaxResult.error(DictPerson.MSG_ERR_CN_ID_NUMBER);
        if (!cardType.equals(DictPerson.ID_TYPE_CHINESE) && !VerifyUtil.baseIdNumberVerify(idNumber))
            return AjaxResult.error(DictPerson.MSG_ERR_ID_NUMBER);

        if (StringUtils.isNotBlank(phone) && !VerifyUtil.phoneNumberVerify(phone))
            return AjaxResult.error(DictPerson.MSG_ERR_PHONE);

        return AjaxResult.success();
    }

    @Override
    public AjaxResult checkIdNumber(String cardType, String idNumber) {
        if (StringUtil.isBlank(cardType) || StringUtil.isBlank(idNumber))
            return AjaxResult.error(DictPerson.MSG_ERR_ID_NUMBER);
        if (!VerifyUtil.baseIdNumberVerify(idNumber))
            return AjaxResult.error(DictPerson.MSG_ERR_ID_NUMBER);
        if (selectPersonByIdNumber(idNumber) != null) {
            return AjaxResult.error(DictPerson.MSG_ERR_ID_NUMBER_);
        }
        if (DictPerson.ID_TYPE_CHINESE.equals(cardType)
                && !VerifyUtil.chineseIDVerify(idNumber))
            return AjaxResult.error(DictPerson.MSG_ERR_CN_ID_NUMBER);

        return AjaxResult.success();
    }

    @Override
    public AjaxResult checkUserBind(String idNumber) {
        SysUser oldUser = userService.selectUserByIdNumber(idNumber);
        if (oldUser != null && StringUtils.isNotBlank(oldUser.getWxid())) {
            return AjaxResult.error(DictPerson.ERR_USER_WX_BIND);
        }
        return AjaxResult.success();
    }

    @Override
    public AjaxResult checkPersonBind(Person oldPerson, SysUser modal) {
        if (oldPerson != null) {
            //验证姓名、电话、证件类型必须一致
            if (!oldPerson.getName().equals(modal.getUserName())
                    || !oldPerson.getCardType().equals(modal.getCardType())
                    || !modal.getIdNumber().equals(oldPerson.getIdNumber())) {
//                    || (StringUtils.isNotBlank(modal.getPhonenumber())
//                    && !modal.getPhonenumber().equals(oldPerson.getPhone())
                return AjaxResult.error(DictPerson.ERR_PERSON_BIND);
            }
        }
        return AjaxResult.success();
    }

    /**
     * 查询人员在指定小区里的卡片，用于下发
     */
    @Override
    public void searchPersonCard(Person person, String townNo) {
        if (person == null) return;
        Card card = new Card();
        card.setTownNo(townNo);
        card.setPersonId(person.getRecordId());
        person.setCardList(cardService.selectCardList(card));
    }

    /**
     * 陌生人对象
     */
    @Override
    public Person getStranger(String idNumber, String name) {
        Person person = new Person();
        person.setRecordId(DictPerson.PERSON_DEFAULT);
        person.setIdNumber(idNumber);
        person.setName(name);
        return person;
    }

    @Override
    public boolean setTown(Person person, Town town) {
        if (town == null)
            town = townService.selectTownByNo(person.getTownNo());
        if (town == null) {
            person.setTownNo(DictTown.TOWN_NO_MANAGE);
            person.setTownName(DictTown.TOWN_NAME_MANAGE);
        } else {
            person.setTownName(town.getTownName());
        }
        return true;
    }

    /**
     * 新增人员
     *
     * @param person 人员
     * @return 结果
     */
    @Override
    public int insertPerson(Person person) {
        if (StringUtils.isBlank(person.getTownNo()))
            person.setTownNo(DictTown.TOWN_NO_MANAGE);
        Town town = townService.selectTownByNo(person.getTownNo());
        if (town != null)
            person.setTownName(town.getTownName());
        if (person.getBornDate() == null)
            person.setBornDate(Utils.getBornDateFromID(person.getIdNumber()));
        if (person.getCreateTime() == null)
            person.setCreateTime(DateUtils.getNowDate());
        person.setIdNumber(Convert.toStr(person.getIdNumber()).toUpperCase());
        person.setSex(VerifyUtil.getSexByIdNumber(person.getIdNumber()));
        int result = personMapper.insertPerson(person);
//        System.out.println("insertPerson,id=" + person.getRecordId());
        return result;
    }

    /**
     * 从有效身份证信息注册人员（不经过CTID）
     */
    @Override
    public Person regPerson(Entrance entrance, String name, String idNumber, String faceImg) {
        //简单校验StringUtils.isBlank(faceImg) ||
        if (!VerifyUtil.chineseIDVerify(idNumber) || !VerifyUtil.baseNameVerify(name)) {
            System.out.println("[reg]ent=" + entrance.getEntName() + ",name=" + name + ",id=" + idNumber + ",face=" + faceImg);
            return null;
        }

        Person person = new Person();
        person.setTownNo(entrance.getTownNo());
        person.setName(name);
        person.setIdNumber(idNumber);
        person.setCardType(DictPerson.ID_TYPE_CHINESE);
        //保存头像图片
        if (!StringUtils.isBlank(faceImg)) {
            String photo = FileUtil.saveImageByBase64(ProjConfig.getUploadPhotoPath(), faceImg);
            //保存缩略图
            String photoThumb = FileUtil.getFileNameThumb(FileUtil.getFileName(photo)) + ".jpg";
            FileUtil.saveImageByBase64(ProjConfig.getUploadPhotoPath(), photoThumb, faceImg);
            person.setPhoto(photo);
        }
        person.setSignWay(DictPerson.Sign_Way_SFZ_AUTO);
        person.setStatus(Constant.STATUS_PASS);
        if (insertPerson(person) > 0)
            return person;
        return null;
    }

    /**
     * 修改人员
     *
     * @param person 人员
     * @return 结果
     */
    @Override
    public int updatePerson(Person person) {
        if (StringUtils.isNotBlank(person.getTownNo())) {
            Town town = townService.selectTownByNo(person.getTownNo());
            if (town != null)
                person.setTownName(town.getTownName());
        }
        if (StringUtils.isNotBlank(person.getIdNumber())) {
            person.setIdNumber(Convert.toStr(person.getIdNumber()).toUpperCase());
            person.setSex(VerifyUtil.getSexByIdNumber(person.getIdNumber()));
            person.setBornDate(Utils.getBornDateFromID(person.getIdNumber()));
        }
        person.setUpdateTime(DateUtils.getNowDate());
        return personMapper.updatePerson(person);
    }

    /**
     * 删除人员对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deletePersonByIds(String ids) {
        return personMapper.deletePersonByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除人员信息
     *
     * @param recordId 人员ID
     * @return 结果
     */
    @Override
    public int deletePersonById(Long recordId) {
        return personMapper.deletePersonById(recordId);
    }

    /**
     * 导入数据
     *
     * @param personList 数据列表
     * @param operName   操作用户
     * @return 结果
     */
    @Override
    public String importPerson(List<Person> personList, String operName) {
        if (StringUtils.isNull(personList) || personList.size() == 0) {
            throw new BusinessException("导入数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (Person person : personList) {
            try {
                person.setCreateBy(operName);
                person.setSignWay(DictPerson.Sign_Way_MGR_IMPORT);
                this.insertPerson(person);
                successNum++;
            } catch (Exception e) {
                failureNum++;
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "导入成功 " + successNum + " 条数据，失败 " + failureNum + " 条，错误如下：");
            throw new BusinessException(failureMsg.toString());
        } else {
            successMsg.insert(0, "导入成功 " + successNum + " 条数据！");
        }
        return successMsg.toString();
    }

    @Override
    public int __sendPerson(Entrance entrance, int lib, Person person) {
        List<Person> personList = new ArrayList<>();
        personList.add(person);
        return __sendPerson(entrance, lib, personList);
    }

    @Override
    public int __sendPerson(Entrance entrance, int lib, List<Person> personList) {
        EntDeviceHandler deviceHandler = EntDeviceHandler.getHandler(entrance);
        return deviceHandler.sendPerson(entrance, personList, String.valueOf(lib));
    }
}
