package com.chjim.basedata.domain;

import com.chjim.common.core.domain.BaseEntity;
import com.chjim.common.core.domain.IDatum;
import com.chjim.common.annotation.Excel;
import com.chjim.common.annotation.Excel.Type;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 小区对象 b_town
 *
 * @author chjim
 */
@Data
public class Town extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 记录ID
     */
    private Long recordId;

    /**
     * 所属区划
     */
    @Excel(name = "区划ID")
    private Long areaId;

    /**
     * 所属区划
     */
    private Area area;

    @Excel(name = "小区类别")
    private String townType;

    private String townCate;

    /**
     * 小区编码
     */
    @Excel(name = "小区编码")
    private String townNo;

    private int countJm;

    private int countCzr;

    private int countZczr;

    private int countGzry;

    /**
     * 市局小区编码
     */
    private String ssxqbm;

    /**
     * 市局停车场编码
     */
    private String tccbh;

    /**
     * 政法委小区标识
     */
    private String ztxqbm;

    /**
     * 小区名称
     */
    @Excel(name = "小区名称")
    private String townName;

    /**
     * 小区门牌号
     */
    @Excel(name = "小区门牌号")
    private String townSn;

    /**
     * 地址标准码
     */
    @Excel(name = "地址标准码")
    private String addrCode;

    /**
     * 公安管理机构
     */
    @Excel(name = "公安管理机构")
    private String psNo;

    /**
     * 公安机构级别
     */
    @Excel(name = "公安机构级别")
    private String psLevel;

    /**
     * zoneBm
     */
    @Excel(name = "zone")
    private String zoneBm;

    /**
     * 地名
     */
    @Excel(name = "地名")
    private String dmmc;

    /**
     * 地名编号
     */
    @Excel(name = "地名编号")
    private String dmdm;

    /**
     * 责任派出所
     */
    @Excel(name = "责任派出所")
    private String gajgjgmc;

    /**
     * 负责警官
     */
    @Excel(name = "负责警官")
    private String jwwgmc;

    /**
     * 负责人
     */
    @Excel(name = "负责人")
    private String leadName;

    /**
     * 数据归属单位代码 弃用
     */
    private String gajgjgdm;

    /**
     * 警务区编号 弃用
     */
    @Excel(name = "警务区编号")
    private String jwwgdm;

    /**
     * 负责人电话 弃用
     */
    @Excel(name = "负责人电话")
    private String leadPhone;

    /**
     * 经度
     */
    @Excel(name = "经度")
    private String mapx;

    /**
     * 纬度
     */
    @Excel(name = "纬度")
    private String mapy;

    /**
     * 边界坐标点
     */
    @Excel(name = "边界坐标点")
    private String points;

    /**
     * 附件文件
     */
    private String resFiles;

    /**
     * 附件名称
     */
    private String resNames;

    /**
     * 市局上报标识
     */
    private String tcStatus;

    /**
     * 政法委上报标识
     */
    private String ztStatus;

    private String mqStatus;

    /**
     * 状态(0无效1有效)
     */
    private String status;

    /**
     * 删除标志(0存在2删除)
     */
    private String delFlag;

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("recordId", getRecordId())
                .append("areaId", getAreaId())
                .append("townNo", getTownNo())
                .append("townName", getTownName())
                .append("townSn", getTownSn())
                .append("gajgjgdm", getGajgjgdm())
                .append("gajgjgmc", getGajgjgmc())
                .append("jwwgdm", getJwwgdm())
                .append("jwwgmc", getJwwgmc())
                .append("dmdm", getDmdm())
                .append("dmmc", getDmmc())
                .append("mapx", getMapx())
                .append("mapy", getMapy())
                .append("resFiles", getResFiles())
                .append("resNames", getResNames())
                .append("status", getStatus())
                .append("delFlag", getDelFlag())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .append("remark", getRemark())
                .toString();
    }
}
