package com.chjim.basedata.service.impl;

import cn.hutool.core.date.DateUtil;
import com.chjim.basedata.domain.*;
import com.chjim.basedata.mapper.AccessLogMapper;
import com.chjim.basedata.service.*;
import com.chjim.care.domain.CareMsg;
import com.chjim.care.domain.CareRule;
import com.chjim.care.service.ICareMsgService;
import com.chjim.care.service.ICareRuleService;
import com.chjim.common.annotation.DataScope;
import com.chjim.common.utils.Convert;
import com.chjim.common.domain.*;
import com.chjim.common.utils.*;
import com.chjim.device.domain.Entrance;
import com.chjim.nongda.util.ChongDianRet;
import com.chjim.nongda.util.ChongDianUtil;
import com.chjim.system.service.ISysConfigService;
import com.chjim.wx.util.WxUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.weixin4j.Weixin;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * 通行记录Service业务层处理
 *
 * @author chjim
 */
@Service
public class AccessLogServiceImpl implements IAccessLogService {
    private static final Logger log = LoggerFactory.getLogger(AccessLogServiceImpl.class);

    @Autowired
    private AccessLogMapper accessLogMapper;

    @Autowired
    private IPersonService personService;

    @Autowired
    private ICareRuleService careRuleService;

    @Autowired
    private ICareMsgService careMsgService;

    @Autowired
    private ISysConfigService configService;

    @Override
    public void preSearch(AccessLog accessLog) {
        String personInfo = Convert.toStr(accessLog.getParams().get("personInfo"));
        if (StringUtil.isNotBlank(personInfo)) {
            Person person = new Person();
            person.getParams().put("personInfo", personInfo);
            List<Person> list = personService.selectPersonList(person);
            Person none = new Person();
            none.setRecordId(0l);
            list.add(none);     //添加0，防止查无此人时，返回所有人员
            accessLog.getParams().put("personIds", ArrayUtil.toRecordIds(list));
//                if (!list.isEmpty()) {                }
        }
    }

    private void convertParams(AccessLog accessLog) {
        if (accessLog.getParams().get("townNos") instanceof String) {
            accessLog.getParams().put("townNos", ((String) accessLog.getParams().get("townNos")).split(","));
        }
    }

    /**
     * 查询通行记录
     *
     * @param recordId 通行记录ID
     * @return 通行记录
     */
    @Override
    public AccessLog selectAccessLogById(Long recordId) {
        return format(accessLogMapper.selectAccessLogById(recordId));
    }

    private AccessLog format(AccessLog accessLog) {
        if (accessLog != null && accessLog.getCaptureImg() != null)
            accessLog.setCaptureImg(Convert.toStr(accessLog.getCaptureImg()).replaceAll("\\\\", "/"));
        return accessLog;
    }

    private List<AccessLog> format(List<AccessLog> list) {
        for (AccessLog accessLog : list) {
            format(accessLog);
        }
        return list;
    }

    /**
     * 查询通行记录列表
     *
     * @param accessLog 通行记录
     * @return 通行记录
     */
    @Override
    @DataScope(townAlias = "t")
    public List<AccessLog> authSelectAccessLogList(AccessLog accessLog) {
        convertParams(accessLog);
//        System.out.println("getPersonId=" + accessLog.getPersonId());
        List<AccessLog> accessLogList;
        if (Convert.toInt(accessLog.getParams().get("distinct")) == 1)
            accessLogList = accessLogMapper.selectDistinctAccessLogList(accessLog);
        else
            accessLogList = accessLogMapper.selectAccessLogList(accessLog);
        for (AccessLog vo : accessLogList) {
            vo.setIdNumber(Utils.hideIdNumber(vo.getIdNumber()));
        }
        return format(accessLogList);
    }

    @Override
    public List<AccessLog> selectAccessLogList(AccessLog accessLog) {
        convertParams(accessLog);
        return format(accessLogMapper.selectAccessLogList(accessLog));
    }

    @Override
    public List<AccessLog> selectAccessLogListByMap(HashMap map) {
        return new ArrayList<>();//accessLogMapper.selectAccessLogListByMap(map);
    }

    @Override
    public long getCount(AccessLog accessLog) {
        convertParams(accessLog);
        return accessLogMapper.getCount(accessLog);
    }

    @Override
    public Long getCountDistinct(AccessLog accessLog) {
        convertParams(accessLog);
        return accessLogMapper.getCountDistinct(accessLog);
    }

    @Override
    public List<HashMap<String, Object>> getStatByHour(AccessLog accessLog) {
        return accessLogMapper.getStatByHour(accessLog);
    }

    @Override
    public List<HashMap<String, Object>> getStatByRnaTime(AccessLog accessLog) {
        convertParams(accessLog);
        return accessLogMapper.getStatByRnaTime(accessLog);
    }

    @Override
    public List<HashMap<String, Object>> createCheckinReport(AccessLog accessLog) {
        return accessLogMapper.createCheckinReport(accessLog);
    }

    @Override
    public List<HashMap<String, Object>> todayNotify(AccessLog accessLog) {
        return accessLogMapper.todayNotify(accessLog);
    }

    @Override
    public List<AccessLog> selectStatisticsAccessLogList(AccessLog accessLog, String startTime, String endTime) {
        return format(accessLogMapper.selectStatisticsAccessLogList(accessLog, startTime, endTime));
    }

//    @Override
//    public List<AccessLog> selectAccessLogLastTenList(AccessLog accessLog) {
//        return accessLogMapper.selectAccessLogLastTenList(accessLog);
//    }

    public AccessLog selectLastByPerson(Long personId) {
        return format(accessLogMapper.selectLastByPerson(personId));
    }

    public List<AccessLog> selectByPersonId(Long personId) {
        return format(accessLogMapper.selectByPersonId(personId));
    }

    /*@Override
    public List<String> selectentNoList() {
        return accessLogMapper.selectentNoList();
    }*/

    /**
     * 关怀人员，出现通行记录，发送通知
     */
    @Override
    public int findCarePersonPass(AccessLog accessLog) {
        int count = 0;
        if (accessLog == null || accessLog.getPersonId() == null)
            return count;
        Person person = personService.selectPersonById(accessLog.getPersonId());
        if (person == null)// || Convert.toInt(person.getCare()) == DictCare.PersonCareType_No)
            return count;

//        System.out.println("[findCarePersonPass]" + person.getName() +
//                DateUtil.formatDateTime(accessLog.getAccessTime()));
        //遍历此人的通行通知的订阅
        CareRule vo = new CareRule();
        vo.setPersonId(person.getRecordId());
        vo.setPassCall(Constant.YES);
        vo.setStatus(Constant.STATUS_PASS);
        List<CareRule> careRuleList = careRuleService.selectCareRuleList(vo);
        for (CareRule careRule : careRuleList) {
            //发送微信通知
            CareMsg careMsg = careMsgService.sendCareMsgPass(careRule, accessLog, true);
            String url = "http://" + Constant.WEB_DOMAIN + "/wx/manage/care/careMsg/" + careMsg.getRecordId();
            boolean flagSend = WxUtil.sendCareMessagePass(new Weixin().message(), careRule, accessLog, url);
//            if (!flagSend) {
//                careMsg = careMsgService.selectCareMsgById(careMsg.getRecordId());
//                if (careMsg != null) {
//                    careMsg.setStatus(Constant.STATUS_READY);
//                    careMsgService.updateCareMsg(careMsg);
//                }
//            }

            System.out.println("[wx通知]" + flagSend + "," + person.getName() +
                    ", wx=" + careRule.getUser().getUserName() + careRule.getUser().getWxid());
            count++;
        }
        return count;
    }

    /**
     * 关怀人员，查询长时间未通行的人员
     */
    @Override
    public int findCarePersonNoPass() {
        int count = 0;
        List<Person> personList = personService.selectCarePerson();
        System.out.println("[findCarePersonNoPass]" + personList.size());
        //遍历关注人群
        for (Person person : personList) {
            AccessLog accessLog = selectLastByPerson(person.getRecordId());
            //最近一次通行时间，无记录则记为注册时间
            Date lastDate = accessLog == null ? person.getCreateTime() : accessLog.getAccessTime();
            int noPassDay = (int) DateUtil.betweenDay(new Date(), lastDate, false);
            System.out.println("[关注]" + person.getName() + " day=" + noPassDay);

            //遍历此人的无通行通知的订阅
            CareRule vo = new CareRule();
            vo.setPersonId(person.getRecordId());
            vo.setNoPassCall(Constant.YES);
            vo.setMsgHour(DateUtil.thisHour(true)); //当前小时
            vo.setStatus(Constant.STATUS_PASS);
            List<CareRule> careRuleList = careRuleService.selectCareRuleList(vo);
            for (CareRule careRule : careRuleList) {
                System.out.println("订阅人=" + careRule.getUser().getUserName() + " day=" + careRule.getNoPassDay());
                //未达到连续无通行记录天数
                if (noPassDay < careRule.getNoPassDay())
                    continue;
                //发送微信通知
                boolean flagSend = WxUtil.sendCareMessageNoPass(new Weixin().message(), careRule, noPassDay);
                careMsgService.sendCareMsgNoPass(careRule, flagSend);

                System.out.println("[无通行通知]" + careRule.getUser().getUserName() + careRule.getUser().getWxid());
                count++;
            }
        }
        return count;
    }

    @Override
    public AccessLog saveToAccess(Entrance entrance, Person person) {
        AccessLog accessLog = new AccessLog();
        accessLog.setTownNo(entrance.getTown().getTownNo());
        accessLog.setTownName(entrance.getTown().getTownName());
        accessLog.setEntNo(entrance.getEntNo());
        accessLog.setEntName(entrance.getEntName());
        accessLog.setEntPlatform(entrance.getDevicePlatform());
        accessLog.setPersonId(person.getRecordId());
        accessLog.setPersonName(person.getName());
        accessLog.setIdNumber(person.getIdNumber());
        accessLog.setAccessType(entrance.getBuildId() + "");
        accessLog.setPersonPhoto(person.getPhoto());
        accessLog.setPersonType(DictPerson.PersonType_Stranger);
//        accessLog.setStatus(Constant.STATUS_SEND);

        accessLog.setPerson(person);
        accessLog.setEntrance(entrance);
        return accessLog;
    }

    @Override
    public int handleAfterSave(AccessLog accessLog) {
        findCarePersonPass(accessLog);
        postNdAccess(accessLog);
        return 1;
    }

    /**
     * 新增通行记录
     *
     * @param accessLog 通行记录
     * @return 结果
     */
    @Override
    public int insertAccessLog(AccessLog accessLog) {
        accessLog.setCreateTime(DateTool.nowDate());
//        System.out.println("[insertAccessLog.person]" + Convert.toLong(accessLog.getPersonId()));
        int result = accessLogMapper.insertAccessLog(accessLog);
//        System.out.println("insertAccessLog,id=" + accessLog.getRecordId());
        return result;
    }

    /**
     * 修改通行记录
     *
     * @param accessLog 通行记录
     * @return 结果
     */
    @Override
    public int updateAccessLog(AccessLog accessLog) {
        accessLog.setUpdateTime(DateTool.nowDate());
        return accessLogMapper.updateAccessLog(accessLog);
    }

    @Override
    public int updateByIds(HashMap<String, Object> map) {
        return accessLogMapper.updateByIds(map);
    }

    /**
     * 删除通行记录对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteAccessLogByIds(String ids) {
        return accessLogMapper.deleteAccessLogByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除通行记录信息
     *
     * @param recordId 通行记录ID
     * @return 结果
     */
    @Override
    public int deleteAccessLogById(Long recordId) {
        return accessLogMapper.deleteAccessLogById(recordId);
    }

    @Override
    public int deleteAccessLogByPersonId(Long personId){
        return accessLogMapper.deleteAccessLogByPersonId(personId);
    }
}
