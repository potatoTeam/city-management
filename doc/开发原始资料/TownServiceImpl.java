package com.chjim.basedata.service.impl;

import com.chjim.basedata.domain.*;
import com.chjim.basedata.mapper.AreaMapper;
import com.chjim.basedata.mapper.TownMapper;
import com.chjim.basedata.mapper.UserTownMapper;
import com.chjim.basedata.service.*;
import com.chjim.common.annotation.DataScope;
import com.chjim.common.core.domain.Ztree;
import com.chjim.common.core.domain.entity.SysUser;
import com.chjim.common.domain.Constant;
import com.chjim.common.domain.DictTown;
import com.chjim.common.exception.BusinessException;
import com.chjim.common.utils.Convert;
import com.chjim.common.utils.DateTool;
import com.chjim.common.utils.DateUtils;
import com.chjim.common.utils.StringUtils;
import com.chjim.common.utils.uuid.IdUtils;
import com.chjim.device.domain.Entrance;
import com.chjim.device.service.IEntranceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 小区Service业务层处理
 *
 * @author chjim
 */
@Service
public class TownServiceImpl implements ITownService {
    private static final Logger log = LoggerFactory.getLogger(TownServiceImpl.class);

    @Autowired
    private TownMapper townMapper;

    @Autowired
    private UserTownMapper userTownMapper;

    @Autowired
    private AreaMapper areaMapper;

    @Autowired
    private IAreaService areaService;

    @Autowired
    private IEntranceService entranceService;

    @Autowired
    private IBuildingService buildingService;

    @Autowired
    private IHouseService houseService;

    /**
     * 查询小区
     *
     * @param recordId 小区ID
     * @return 小区
     */
    @Override
    public Town selectTownById(Long recordId) {
        return townMapper.selectTownById(recordId);
    }

    /**
     * 查询小区列表
     *
     * @param town 小区
     * @return 小区
     */
    @Override
    public List<Town> selectTownList(Town town) {
        if (town.getAreaId() != null) {
            town.getParams().put("ancestorId", town.getAreaId());
            town.setAreaId(null);
        }
        return townMapper.selectTownList(town);
    }

    @Override
    @DataScope(townAlias = "tw")
    public List<Town> authSelectTownList(Town town) {
        if (town.getAreaId() != null) {
            town.getParams().put("ancestorId", town.getAreaId());
            town.setAreaId(null);
        }
        return townMapper.selectTownList(town);
    }

//    @Override
//    public long getCountByMap(HashMap map) {
//        return townMapper.getCountByMap(map);
//    }

    @Override
    @DataScope(townAlias = "tw")
    public Long getCount(Town town) {
        return townMapper.getCount(town);
    }

    @Override
    public Town selectTownByNo(String townNo) {
        Town town = townMapper.selectTownByNo(townNo);
        if (town != null) {
            town.setTownCate(DictTown.getTownCate(town.getTownType()));
        }
        return town;
    }

    @Override
    public Town selectTownByXqdm(String xqdm) {
        return townMapper.selectTownByXqdm(xqdm);
    }

    @Override
    public List<Town> selectTownByNos(String townNos) {
        return townMapper.selectTownByNos(Convert.toStrArray(townNos));
    }

    @Override
    public List<Town> selectTownListByUserId(SysUser user) {
        return user == null ? new ArrayList<>() : townMapper.selectTownListByUserId(user.getUserId());
    }

    @Override
    public String selectTownNosByUser(SysUser user) {
        if (user == null) return "";
        List<Town> townList = selectTownListByUserId(user);
        String townNos = "0 ";  //防止无分配小区时，查询到所有小区
        for (Town town : townList) {
            townNos += town.getTownNo() + " ";
        }
        return townNos.trim().replaceAll(" ", ",");
    }

    @Override
    public Town selectTownByAddrCode(String addrCode) {
        return townMapper.selectTownByAddrCode(addrCode);
    }

    @Override
    public Town selectTownBySubAddrCode(String addrCode) {
        Town town = selectTownByAddrCode(addrCode);
        if (town != null) return town;
        Entrance entrance = entranceService.selectEntranceByAddrCode(addrCode);
        if (entrance != null) return selectTownByNo(entrance.getTownNo());
//        Building building = buildingService.selectBuildingByAddrCode(addrCode);
//        if (building != null) return selectTownByNo(building.getTownNo());
//        House house = houseService.selectHouseByAddrCode(addrCode);
//        if (house != null) return selectTownByNo(house.getTownNo());
        return null;
    }

    @Override
    @DataScope(townAlias = "tw")
    public List<HashMap<String, Object>> getStatByType(Town town) {
        return townMapper.getStatByType(town);
    }

    //用于前端select数据 加上权限
    @Override
    @DataScope(townAlias = "tw")
    public List<Town> getTownSelectGroup(Town town) {
        return townMapper.selectTownList(town);
    }

//    @Override
//    //@DataScope(areaAlias = "d")
//    public List<Ztree> selectTownTree() {
//        List<Area> areaList = areaMapper.selectAreaList(new Area());
//        List<Ztree> ztrees = initZtree(areaList);
//        return ztrees;
//    }
//
//    public List<Ztree> initZtree(List<Area> areaList) {
//        return initZtree(areaList, null);
//    }

    @Override
    public List<String> getUserTownList(SysUser user) {
        List<String> userTownList = townMapper.selectUserTownTree(user.getUserId());
        return userTownList;
    }

    /**
     * 根据用户ID查询小区（数据权限）
     */
    @Override
    public List<Ztree> userTownTreeData(SysUser user) {
        List<Area> areaList = areaMapper.selectAreaList(new Area());
        List<String> userTownList = townMapper.selectUserTownTree(user.getUserId());
        List<Ztree> ztrees = initZtree(areaList, userTownList);
        return ztrees;
    }

    public List<Ztree> initZtree(List<Area> areaList, List<String> userTownList) {
        List<Ztree> ztrees = new ArrayList<>();
        boolean isCheck = StringUtils.isNotNull(userTownList);
        for (Area area : areaList) {
            if (Constant.STATUS_PASS.equals(area.getStatus())) {
            }
            Ztree ztree = new Ztree();
            //行政区划和小区同在列表树里，区划编号加上9万，跟小区编号区分开
            ztree.setId(Constant.AREA_ID_START + area.getAreaId());
            ztree.setpId(Constant.AREA_ID_START + area.getParentId());
            ztree.setName(area.getAreaName());
            ztree.setTitle(area.getAreaName());
            ztree.setNocheck(true);
            ztrees.add(ztree);

            //查询该区划下的小区
            Town _town = new Town();
            _town.setAreaId(area.getAreaId());
            List<Town> townList = townMapper.selectTownList(_town);
            for (Town town : townList) {
                ztree = new Ztree();
                ztree.setId(town.getRecordId());
                ztree.setpId(Constant.AREA_ID_START + town.getAreaId());
                ztree.setName(town.getTownName());
                ztree.setTitle(town.getTownName());
                ztree.setNocheck(false);
                if (isCheck) {
                    ztree.setChecked(userTownList.contains(town.getRecordId() + "_" + town.getTownName()));
                }
                ztrees.add(ztree);
            }
        }
        return ztrees;
    }

    /**
     * 修改数据权限信息
     */
    @Override
    @Transactional
    public int authTownScope(SysUser user, String townIds) {
        System.out.println("[authTownScope]" + user.getUserName() + ",townIds=" + townIds);
        // 修改角色信息
//        userMapper.updateRole(role);
        // 删除用户与小区关联
        userTownMapper.deleteUserTownByUserId(user.getUserId());
        // 新增角色和部门信息（数据权限）
        return insertUserTown(user, townIds);
    }

    @Override
    public String[] getTownNoArr(List<Town> townList) {
        String townNos = "";
        for (Town town : townList) {
            townNos += town.getTownNo() + " ";
        }
        return townNos.trim().split(" ");
    }

    @Override
    public int insertUserTown(SysUser user, Town town) {
        if (user == null || town == null || town.getRecordId() == null)
            return 0;
        UserTown userTown = new UserTown();
        userTown.setUserId(user.getUserId());
        userTown.setTownId(town.getRecordId());
        userTown.setTownNo(town.getTownNo());
        return userTownMapper.insertUserTown(userTown);
    }

    /**
     * 新增用户小区信息(数据权限)
     */
    public int insertUserTown(SysUser user, String townIds) {
        int rows = 1;
        // 新增角色与部门（数据权限）管理
        List<UserTown> list = new ArrayList<>();
        String[] ids = townIds.split(",");
        for (String id : ids) {
            UserTown userTown = new UserTown();
            userTown.setUserId(user.getUserId());
            userTown.setTownId(Convert.toLong(id));
            Town town = selectTownById(Convert.toLong(id));
            if (town != null)
                userTown.setTownNo(town.getTownNo());
            list.add(userTown);
        }
        if (list.size() > 0) {
            rows = userTownMapper.batchUserTown(list);
        }
        return rows;
    }

    /**
     * 获取小区所属区划名称路径
     */
    @Override
    public String getAreaNames(Town town) {
        String areas = "";
        if (town == null || town.getArea() == null)
            return areas;
        String[] area_ids = town.getArea().getAncestors().split(",");
        for (String id : area_ids) {
            Area area = areaService.selectAreaById(Convert.toLong(id));
            if (area == null) continue;
            areas += area.getAreaName() + " ";
        }
        areas += town.getArea().getAreaName();
        return areas.trim();
    }

    /**
     * 获取小区所属的区县、街道、社区
     */
    public String[] getArea3Names(Town town) {
        String[] areas = getAreaNames(town).split(" ");
        String[] arr = {"区县", "街道", "社区"};
        if (areas.length > 0) arr[2] = areas[areas.length - 1];
        if (areas.length > 1) arr[1] = areas[areas.length - 2];
        if (areas.length > 2) arr[0] = areas[areas.length - 3];
        return arr;
    }

    /**
     * 新增小区
     *
     * @param town 小区
     * @return 结果
     */
    @Override
    public int insertTown(Town town) {
        if (Convert.toInt(town.getAreaId()) == 0)
            town.setAreaId(100l);
        if (StringUtils.isBlank(town.getTownNo())) {
            town.setTownNo(getNextNo());
        }
        if (StringUtils.isBlank(town.getAddrCode())) {
            town.setAddrCode(Constant.PRE_FWE_CODE + IdUtils.fastUUID().toUpperCase());
        }
        town.setPoints(Convert.toStr(town.getPoints()).trim());
        updTownGps(town);
        town.setCreateTime(DateTool.nowDate());
        int result = townMapper.insertTown(town);
        System.out.println("insertTown,id=" + town.getRecordId());
        return result;
    }

    /**
     * 修改小区
     *
     * @param town 小区
     * @return 结果
     */
    @Override
    public int updateTown(Town town) {
        town.setUpdateTime(DateTool.nowDate());
        town.setPoints(Convert.toStr(town.getPoints()).trim());
        updTownGps(town);
        return townMapper.updateTown(town);
    }

    private void updTownGps(Town town) {
        double x1 = 1000, x2 = 0, y1 = 1000, y2 = 0;
        String[] points = Convert.toStr(town.getPoints()).trim().split(" ");
        for (String point : points) {
            String[] arr = point.split(",");
            if (arr.length != 2)
                continue;
            double lng = Convert.toDouble(arr[0]);
            double lat = Convert.toDouble(arr[1]);
            if (lng < x1) x1 = lng;
            if (lng > x2) x2 = lng;
            if (lat < y1) y1 = lat;
            if (lat > y2) y2 = lat;
        }
        town.setMapx(String.valueOf(x1 + (x2 - x1) / 2.0d));
        town.setMapy(String.valueOf(y1 + (y2 - y1) / 2.0d));
    }

    /**
     * 删除小区对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteTownByIds(String ids) {
        return townMapper.deleteTownByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除小区信息
     *
     * @param recordId 小区ID
     * @return 结果
     */
    @Override
    public int deleteTownById(Long recordId) {
        return townMapper.deleteTownById(recordId);
    }

    /**
     * 导入数据
     *
     * @param townList 数据列表
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importTown(List<Town> townList, String operName) {
        if (StringUtils.isNull(townList) || townList.size() == 0) {
            throw new BusinessException("导入数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (Town town : townList) {
            if (selectTownByAddrCode(town.getAddrCode()) != null)
                continue;
            try {
                town.setCreateBy(operName);
                this.insertTown(town);
                successNum++;
            } catch (Exception e) {
                failureNum++;
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "导入成功 " + successNum + " 条数据，失败 " + failureNum + " 条，错误如下：");
            throw new BusinessException(failureMsg.toString());
        } else {
            successMsg.insert(0, "导入成功 " + successNum + " 条数据！");
        }
        return successMsg.toString();
    }

    @Override
    public String getMaxNo() {
        String max = Convert.toStr(townMapper.getMaxNo());
        return max;
    }

    /**
     * 根据序号新建楼栋编号
     * 用于单次
     */
    @Override
    public String getNextNo() {
        int idx = getNextIdx() + 1;
        String no_pre = Constant.PRE_TOWN + DateUtils.nowYYMM();
        String no = no_pre + StringUtils.leftPad(idx + "", 5, '0');
        return no;
    }

    /**
     * 根据序号新建楼栋编号
     * 用于批量
     */
    @Override
    public String getNextNo(int idx) {
        String no_pre = Constant.PRE_TOWN + DateUtils.nowYYMM();
        String no = no_pre + StringUtils.leftPad(idx + "", 5, '0');
        return no;
    }

    /**
     * 获取当前月份最大编号
     */
    @Override
    public int getNextIdx() {
        String no_build = getMaxNo();
        String no_pre = Constant.PRE_TOWN + DateUtils.nowYYMM();
        int idx_build = 0;
        if (no_build.startsWith(no_pre)) {
            idx_build = Convert.toInt(no_build.substring(no_pre.length()));
        }
        return idx_build;
    }

}
