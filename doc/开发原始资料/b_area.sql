/*
 Navicat MySQL Data Transfer

 Source Server         : mysql8
 Source Server Type    : MySQL
 Source Server Version : 80031
 Source Host           : localhost:3806
 Source Schema         : smartpark

 Target Server Type    : MySQL
 Target Server Version : 80031
 File Encoding         : 65001

 Date: 11/12/2023 10:30:23
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for b_area
-- ----------------------------
DROP TABLE IF EXISTS `b_area`;
CREATE TABLE `b_area`  (
  `area_id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '区划id',
  `parent_id` bigint(0) NOT NULL DEFAULT 0 COMMENT '父区划id',
  `ancestors` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT '祖级列表',
  `level` int(0) NULL DEFAULT 0 COMMENT '层级',
  `area_type` char(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '0' COMMENT '区划类型',
  `area_name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '' COMMENT '区划名称',
  `area_no` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '编码',
  `order_num` int(0) NULL DEFAULT 0 COMMENT '显示顺序',
  `status` char(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`area_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 314 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci COMMENT = '行政区划' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of b_area
-- ----------------------------
INSERT INTO `b_area` VALUES (100, 0, '0', 1, '0', '福建省福州市', '35', 0, '0', '0', 'admin', '2021-02-22 10:53:57', 'ysyang', '2023-08-24 13:45:22');
INSERT INTO `b_area` VALUES (101, 100, '0,100', 2, '0', '鼓楼区', '3500', 2, '0', '0', 'admin', '2021-02-22 10:53:57', 'ysyang', '2022-03-16 20:44:29');
INSERT INTO `b_area` VALUES (102, 0, '0', 1, '0', '福建省平潭', '3501', 15, '0', '0', 'admin', '2021-02-22 10:53:57', 'admin', '2022-05-07 10:07:01');
INSERT INTO `b_area` VALUES (120, 101, '0,100,101', 3, '0', '温泉街道', '350001', 1, '0', '2', 'admin', NULL, 'admin', '2022-03-16 20:44:29');
INSERT INTO `b_area` VALUES (121, 102, '0,102', 2, '0', '平潭', '350002', 2, '0', '0', 'admin', NULL, 'admin', NULL);
INSERT INTO `b_area` VALUES (122, 120, '0,100,101,120', 4, '1', '金汤社区', '35000101', 1, '0', '2', 'admin', NULL, 'admin', NULL);
INSERT INTO `b_area` VALUES (123, 120, '0,100,101,120', 4, '1', '东大社区', '3500010102', 1, '0', '2', 'admin', NULL, 'admin', NULL);
INSERT INTO `b_area` VALUES (124, 120, '0,100,101,120', 4, '1', '河东社区', '3500010103', 2, '0', '2', 'admin', NULL, 'admin', NULL);
INSERT INTO `b_area` VALUES (125, 100, '0,100', 2, '', '闽侯县', '3503', 3, '0', '0', 'admin', NULL, 'ysyang', '2022-12-15 19:02:13');
INSERT INTO `b_area` VALUES (126, 125, '0,100,125', 3, '', '闽侯经济开发区', '350301', 1, '0', '0', 'admin', NULL, 'ysyang', '2022-06-07 13:39:36');
INSERT INTO `b_area` VALUES (127, 126, '0,100,125,126', 4, '', '经济开发区一期', '35030101', 1, '0', '0', 'admin', NULL, 'admin', NULL);
INSERT INTO `b_area` VALUES (128, 126, '0,100,125,126', 4, '', '开发区二期', '35010102', 2, '0', '0', 'admin', NULL, 'admin', NULL);
INSERT INTO `b_area` VALUES (129, 100, '0,100', 2, '', '仓山区', '103', 1, '0', '0', 'admin', NULL, 'ysyang', '2023-08-22 09:08:42');
INSERT INTO `b_area` VALUES (130, 129, '0,100,129', 3, '', '对湖街道', '350007', 1, '0', '0', 'admin', NULL, 'ysyang', '2023-08-14 16:34:24');
INSERT INTO `b_area` VALUES (131, 129, '0,100,129', 3, '', '上渡街道', '131', 2, '0', '0', 'yunyiadmin', NULL, 'ysyang', '2023-08-21 02:00:19');
INSERT INTO `b_area` VALUES (132, 100, '0,100', 2, '', '城门区', '132', 4, '1', '0', 'yangnan', NULL, 'yunyiadmin', NULL);
INSERT INTO `b_area` VALUES (133, 138, '0,100,129,138', 4, '', '洪光村', '150', 18, '0', '0', 'yangnan', NULL, 'zgb', NULL);
INSERT INTO `b_area` VALUES (134, 131, '0,100,129,131', 4, '', '新成社区', '3500080202', 2, '0', '0', 'chenhui', NULL, '', NULL);
INSERT INTO `b_area` VALUES (135, 129, '0,100,129', 3, '', '城门镇', '135', 3, '0', '0', 'yunyiadmin', NULL, 'zgb', '2023-07-17 15:26:56');
INSERT INTO `b_area` VALUES (136, 135, '0,100,129,135', 4, '', '胪雷村', '136', 1, '0', '0', 'yunyiadmin', NULL, 'yunyiadmin', NULL);
INSERT INTO `b_area` VALUES (137, 135, '0,100,129,135', 4, '', '清富村', '137', 2, '0', '0', 'yunyiadmin', NULL, 'yunyiadmin', NULL);
INSERT INTO `b_area` VALUES (138, 129, '0,100,129', 3, '', '建新镇', '137', 5, '0', '0', 'yunyiadmin', NULL, 'ysyang', '2023-08-22 09:08:42');
INSERT INTO `b_area` VALUES (139, 138, '0,100,129,138', 4, '', '农大社区', '138', 1, '0', '0', 'yunyiadmin', NULL, 'yunyiadmin', NULL);
INSERT INTO `b_area` VALUES (140, 100, '0,100', 2, '', '台江区', '140', 5, '0', '0', 'yunyiadmin', NULL, '', NULL);
INSERT INTO `b_area` VALUES (141, 140, '0,100,140', 3, '', '义洲街道', '141', 1, '0', '0', 'yunyiadmin', NULL, '', NULL);
INSERT INTO `b_area` VALUES (142, 141, '0,100,140,141', 4, '', '浦西社区', '142', 1, '0', '0', 'yunyiadmin', NULL, '', NULL);
INSERT INTO `b_area` VALUES (143, 140, '0,100,140', 3, '', '宁化街道', '143', 2, '0', '0', 'yunyiadmin', NULL, '', NULL);
INSERT INTO `b_area` VALUES (144, 143, '0,100,140,143', 4, '', '宁化社区', '144', 1, '0', '0', 'yunyiadmin', NULL, '', NULL);
INSERT INTO `b_area` VALUES (145, 258, '0,100,129,258', 4, '', '万春社区', '150', 2, '0', '0', 'yunyiadmin', NULL, 'zgb', NULL);
INSERT INTO `b_area` VALUES (146, 147, '0,100,129,147', 4, '', '三高社区', '146', 8, '0', '0', 'yunyiadmin', NULL, 'yunyiadmin', NULL);
INSERT INTO `b_area` VALUES (147, 129, '0,100,129', 3, '', '仓山镇', '146', 4, '0', '0', 'yunyiadmin', NULL, 'yunyiadmin', '2022-10-09 12:03:57');
INSERT INTO `b_area` VALUES (148, 147, '0,100,129,147', 4, '', '万里村', '148', 1, '0', '0', 'yunyiadmin', NULL, '', NULL);
INSERT INTO `b_area` VALUES (149, 130, '0,100,129,130', 4, '', '程厝社区', '148', 2, '0', '0', 'yunyiadmin', NULL, '', NULL);
INSERT INTO `b_area` VALUES (150, 129, '0,100,129', 3, '', '盖山镇', '149', 6, '0', '0', 'yunyiadmin', NULL, 'zgb', '2023-06-06 09:38:01');
INSERT INTO `b_area` VALUES (151, 150, '0,100,129,150', 4, '', '华彩社区', '151', 1, '0', '0', 'yunyiadmin', NULL, '', NULL);
INSERT INTO `b_area` VALUES (152, 147, '0,100,129,147', 4, '', '双湖社区', '151', 2, '0', '0', 'yunyiadmin', NULL, '', NULL);
INSERT INTO `b_area` VALUES (153, 147, '0,100,129,147', 4, '', '湖边村', '150', 3, '0', '0', 'yunyiadmin', NULL, '', NULL);
INSERT INTO `b_area` VALUES (154, 129, '0,100,129', 3, '', '螺洲镇', '152', 6, '0', '0', 'yunyiadmin', NULL, '', NULL);
INSERT INTO `b_area` VALUES (155, 154, '0,100,129,154', 4, '', '店前村', '153', 1, '0', '0', 'yunyiadmin', NULL, '', NULL);
INSERT INTO `b_area` VALUES (156, 100, '0,100', 2, '', '晋安区', '153', 6, '0', '0', 'yunyiadmin', NULL, 'yunyiadmin', NULL);
INSERT INTO `b_area` VALUES (157, 156, '0,100,156', 3, '', '鼓山镇', '153', 1, '0', '0', 'yunyiadmin', NULL, '', NULL);
INSERT INTO `b_area` VALUES (158, 157, '0,100,156,157', 4, '', '东山新苑社区', '157', 1, '0', '0', 'yunyiadmin', NULL, '', NULL);
INSERT INTO `b_area` VALUES (159, 157, '0,100,156,157', 4, '', '石鼓社区', '152', 2, '0', '0', 'yunyiadmin', NULL, '', NULL);
INSERT INTO `b_area` VALUES (160, 147, '0,100,129,147', 4, '', '下湖村', '150', 4, '0', '0', 'yunyiadmin', NULL, '', NULL);
INSERT INTO `b_area` VALUES (161, 130, '0,100,129,130', 4, '', '万里社区', '150', 3, '0', '0', 'yunyiadmin', NULL, '', NULL);
INSERT INTO `b_area` VALUES (162, 147, '0,100,129,147', 4, '', '联建社区', '150', 5, '0', '0', 'yunyiadmin', NULL, '', NULL);
INSERT INTO `b_area` VALUES (163, 129, '0,100,129', 3, '', '三叉街街道', '150', 7, '0', '0', 'yunyiadmin', NULL, '', NULL);
INSERT INTO `b_area` VALUES (164, 163, '0,100,129,163', 4, '', '绿岛社区', '150', 1, '0', '0', 'yunyiadmin', NULL, '', NULL);
INSERT INTO `b_area` VALUES (165, 147, '0,100,129,147', 4, '', '先锋村', '150', 6, '0', '0', 'yunyiadmin', NULL, '', NULL);
INSERT INTO `b_area` VALUES (166, 150, '0,100,129,150', 4, '', '北园村', '150', 2, '0', '0', 'yunyiadmin', NULL, '', NULL);
INSERT INTO `b_area` VALUES (167, 147, '0,100,129,147', 4, '', '迎霞社区', '150', 7, '0', '0', 'yunyiadmin', NULL, '', NULL);
INSERT INTO `b_area` VALUES (168, 130, '0,100,129,130', 4, '', '师大社区', '150', 4, '0', '0', 'yunyiadmin', NULL, '', NULL);
INSERT INTO `b_area` VALUES (169, 150, '0,100,129,150', 4, '', '叶厦村', '150', 3, '0', '0', 'yunyiadmin', NULL, 'zgb', NULL);
INSERT INTO `b_area` VALUES (170, 125, '0,100,125', 3, '', '荆溪镇', '150', 2, '0', '0', 'yunyiadmin', NULL, 'zgb', '2022-12-15 19:02:13');
INSERT INTO `b_area` VALUES (171, 170, '0,100,125,170', 4, '', '关口村', '150', 1, '0', '0', 'yunyiadmin', NULL, '', NULL);
INSERT INTO `b_area` VALUES (172, 170, '0,100,125,170', 4, '', '港头村', '150', 2, '0', '0', 'yunyiadmin', NULL, '', NULL);
INSERT INTO `b_area` VALUES (173, 131, '0,100,129,131', 4, '', '滨洲社区', '150', 1, '0', '0', 'yunyiadmin', NULL, 'yunyiadmin', NULL);
INSERT INTO `b_area` VALUES (174, 150, '0,100,129,150', 4, '', '浦江社区', '150', 4, '0', '0', 'yunyiadmin', NULL, '', NULL);
INSERT INTO `b_area` VALUES (175, 135, '0,100,129,135', 4, '', '潘墩村', '150', 3, '0', '0', 'yunyiadmin', NULL, '', NULL);
INSERT INTO `b_area` VALUES (176, 138, '0,100,129,138', 4, '', '金亭社区', '150', 2, '0', '0', 'yunyiadmin', NULL, '', NULL);
INSERT INTO `b_area` VALUES (177, 100, '0,100', 2, '', '连江县', '150', 7, '0', '0', 'yunyiadmin', NULL, '', NULL);
INSERT INTO `b_area` VALUES (178, 177, '0,100,177', 3, '', '贵安村', '150', 1, '0', '0', 'yunyiadmin', NULL, '', NULL);
INSERT INTO `b_area` VALUES (179, 125, '0,100,125', 3, '', '高新区', '150', 3, '0', '0', 'yunyiadmin', NULL, '', NULL);
INSERT INTO `b_area` VALUES (180, 179, '0,100,125,179', 4, '', '南屿镇', '150', 1, '0', '0', 'yunyiadmin', NULL, '', NULL);
INSERT INTO `b_area` VALUES (181, 180, '0,100,125,179,180', 5, '', '南井村', '150', 1, '0', '0', 'yunyiadmin', NULL, '', NULL);
INSERT INTO `b_area` VALUES (182, 138, '0,100,129,138', 4, '', '洪湾社区', '150', 3, '0', '0', 'yunyiadmin', NULL, 'yunyiadmin', NULL);
INSERT INTO `b_area` VALUES (183, 150, '0,100,129,150', 4, '', '江边村', '150', 5, '0', '0', 'yunyiadmin', NULL, '', NULL);
INSERT INTO `b_area` VALUES (184, 163, '0,100,129,163', 4, '', '村东社区', '150', 2, '0', '0', 'yunyiadmin', NULL, '', NULL);
INSERT INTO `b_area` VALUES (185, 135, '0,100,129,135', 4, '', '湖际村', '150', 4, '0', '0', 'yunyiadmin', NULL, 'zgb', NULL);
INSERT INTO `b_area` VALUES (186, 135, '0,100,129,135', 4, '', '下洋村', '150', 5, '0', '0', 'yunyiadmin', NULL, '', NULL);
INSERT INTO `b_area` VALUES (187, 135, '0,100,129,135', 4, '', '黄山村', '150', 6, '0', '0', 'yunyiadmin', NULL, '', NULL);
INSERT INTO `b_area` VALUES (188, 135, '0,100,129,135', 4, '', '樟岚村', '150', 7, '0', '0', 'yunyiadmin', NULL, '', NULL);
INSERT INTO `b_area` VALUES (189, 135, '0,100,129,135', 4, '', '鳌里村', '150', 8, '0', '0', 'yunyiadmin', NULL, '', NULL);
INSERT INTO `b_area` VALUES (190, 138, '0,100,129,138', 4, '', '金霞社区', '150', 4, '0', '0', 'yunyiadmin', NULL, '', NULL);
INSERT INTO `b_area` VALUES (191, 138, '0,100,129,138', 4, '', '透浦村', '150', 5, '0', '0', 'yunyiadmin', NULL, '', NULL);
INSERT INTO `b_area` VALUES (192, 150, '0,100,129,150', 4, '', '浦口村', '150', 6, '0', '0', 'yunyiadmin', NULL, '', NULL);
INSERT INTO `b_area` VALUES (193, 129, '0,100,129', 3, '', '金山街道', '150', 8, '0', '0', 'yunyiadmin', NULL, 'ysyang', '2022-11-20 12:10:28');
INSERT INTO `b_area` VALUES (194, 193, '0,100,129,193', 4, '', '金骏社区', '150', 1, '0', '0', 'yunyiadmin', NULL, '', NULL);
INSERT INTO `b_area` VALUES (195, 150, '0,100,129,150', 4, '', '首山村', '150', 7, '0', '0', 'yunyiadmin', NULL, '', NULL);
INSERT INTO `b_area` VALUES (196, 138, '0,100,129,138', 4, '', '福湾社区', '150', 6, '0', '0', 'yunyiadmin', NULL, '', NULL);
INSERT INTO `b_area` VALUES (197, 138, '0,100,129,138', 4, '', '浦江社区', '150', 7, '0', '0', 'yunyiadmin', NULL, '', NULL);
INSERT INTO `b_area` VALUES (198, 193, '0,100,129,193', 4, '', '水都社区', '150', 2, '0', '0', 'yunyiadmin', NULL, '', NULL);
INSERT INTO `b_area` VALUES (199, 100, '0,100', 2, '', '马尾区', '150', 8, '0', '0', 'yunyiadmin', NULL, '', NULL);
INSERT INTO `b_area` VALUES (200, 199, '0,100,199', 3, '', '琅岐镇', '150', 1, '0', '0', 'yunyiadmin', NULL, '', NULL);
INSERT INTO `b_area` VALUES (201, 200, '0,100,199,200', 4, '', '乐村村委会', '150', 1, '0', '0', 'yunyiadmin', NULL, '', NULL);
INSERT INTO `b_area` VALUES (202, 193, '0,100,129,193', 4, '', '幸福社区', '150', 3, '0', '0', 'yunyiadmin', NULL, '', NULL);
INSERT INTO `b_area` VALUES (203, 193, '0,100,129,193', 4, '', '中天社区', '150', 4, '0', '0', 'yunyiadmin', NULL, '', NULL);
INSERT INTO `b_area` VALUES (204, 125, '0,100,125', 3, '', '甘蔗街道', '150', 4, '0', '0', 'hyw123', NULL, '', NULL);
INSERT INTO `b_area` VALUES (205, 170, '0,100,125,170', 4, '', '荆溪社区', '150', 3, '0', '0', 'zgb', NULL, 'zgb', NULL);
INSERT INTO `b_area` VALUES (206, 193, '0,100,129,193', 4, '', '金洲社区', '150', 5, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (207, 138, '0,100,129,138', 4, '', '梅洲社区', '150', 8, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (208, 138, '0,100,129,138', 4, '', '江滨社区', '150', 9, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (209, 138, '0,100,129,138', 4, '', '观江社区', '150', 10, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (210, 138, '0,100,129,138', 4, '', '淮安社区', '150', 11, '0', '0', 'zgb', NULL, 'zgb', NULL);
INSERT INTO `b_area` VALUES (211, 193, '0,100,129,193', 4, '', '金河社区', '150', 6, '0', '0', 'zgb', NULL, 'zgb', NULL);
INSERT INTO `b_area` VALUES (212, 193, '0,100,129,193', 4, '', '刘宅村', '150', 7, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (213, 193, '0,100,129,193', 4, '', '新筑社区', '150', 8, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (214, 193, '0,100,129,193', 4, '', '金麟社区', '150', 9, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (215, 193, '0,100,129,193', 4, '', '香江社区', '150', 10, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (216, 193, '0,100,129,193', 4, '', '鑫龙社区', '150', 11, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (217, 157, '0,100,156,157', 4, '', '横屿村', '150', 3, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (218, 193, '0,100,129,193', 4, '', '金环社区', '150', 12, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (219, 193, '0,100,129,193', 4, '', '彼岸社区', '150', 13, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (220, 193, '0,100,129,193', 4, '', '建兴洋社区', '150', 14, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (221, 193, '0,100,129,193', 4, '', '百花社区', '150', 15, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (222, 193, '0,100,129,193', 4, '', '丽景社区', '150', 16, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (223, 138, '0,100,129,138', 4, '', '淮兴社区', '150', 12, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (224, 193, '0,100,129,193', 4, '', '建中社区', '150', 17, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (225, 193, '0,100,129,193', 4, '', '六江道社区', '150', 18, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (226, 193, '0,100,129,193', 4, '', '吉龙社区', '150', 19, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (227, 193, '0,100,129,193', 4, '', '潘厝社区', '227', 20, '0', '0', 'ysyang', NULL, 'ysyang', NULL);
INSERT INTO `b_area` VALUES (228, 138, '0,100,129,138', 4, '', '金港社区', '150', 13, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (229, 138, '0,100,129,138', 4, '', '状元社区', '150', 14, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (230, 154, '0,100,129,154', 4, '', '螺洲委', '150', 2, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (231, 138, '0,100,129,138', 4, '', '东岭村', '150', 15, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (232, 140, '0,100,140', 3, '', '鳌峰街道', '150', 3, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (233, 232, '0,100,140,232', 4, '', '鳌峰洲社区', '150', 1, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (234, 130, '0,100,129,130', 4, '', '马厂社区', '150', 5, '0', '0', 'zgb', NULL, 'ysyang', NULL);
INSERT INTO `b_area` VALUES (235, 100, '0,100', 2, '', '福建省闽清县', '150', 9, '0', '0', 'zgb', NULL, 'zgb', '2023-07-31 09:09:54');
INSERT INTO `b_area` VALUES (236, 235, '0,100,235', 3, '', '梅城镇', '150', 1, '0', '0', 'zgb', NULL, 'zgb', '2023-07-31 09:09:54');
INSERT INTO `b_area` VALUES (237, 236, '0,100,235,236', 4, '', '城北委', '150', 1, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (238, 236, '0,100,235,236', 4, '', '洋桃委', '150', 2, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (239, 129, '0,100,129', 3, '', '下渡街道', '150', 9, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (240, 239, '0,100,129,239', 4, '', '先锋委', '150', 1, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (241, 125, '0,100,125', 3, '', '电网信息打印', '150', 5, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (242, 138, '0,100,129,138', 4, '', '台山社区', '150', 16, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (243, 138, '0,100,129,138', 4, '', '马榕社区', '150', 17, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (244, 236, '0,100,235,236', 4, '', '西门街社区', '150', 3, '0', '0', 'zgb', NULL, 'zgb', NULL);
INSERT INTO `b_area` VALUES (245, 232, '0,100,140,232', 4, '', '福人社区', '150', 2, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (246, 129, '0,100,129', 3, '', '东兴街道', '150', 10, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (247, 246, '0,100,129,246', 4, '', '东兴委', '150', 1, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (248, 100, '0,100', 2, '', '永泰县', '3507', 11, '0', '0', 'admin', NULL, '', NULL);
INSERT INTO `b_area` VALUES (249, 150, '0,100,129,150', 4, '', '白湖村', '150', 8, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (250, 150, '0,100,129,150', 4, '', '高湖村', '150', 9, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (251, 193, '0,100,129,193', 4, '', '横江渡社区', '150', 21, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (252, 150, '0,100,129,150', 4, '', '齐安村', '150', 10, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (253, 150, '0,100,129,150', 4, '', '雁祥社区', '150', 11, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (254, 100, '0,100', 2, '', '未区分', '1000', 100, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (255, 239, '0,100,129,239', 4, '', '银桥社区', '254', 2, '0', '0', 'ysyang', NULL, '', NULL);
INSERT INTO `b_area` VALUES (256, 131, '0,100,129,131', 4, '', '建岭社区', '150', 3, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (257, 135, '0,100,129,135', 4, '', '白云村', '256', 9, '0', '0', 'ysyang', NULL, '', NULL);
INSERT INTO `b_area` VALUES (258, 129, '0,100,129', 3, '', '仓前街道', '150', 11, '0', '0', 'zgb', NULL, 'zgb', '2023-08-14 16:43:15');
INSERT INTO `b_area` VALUES (259, 258, '0,100,129,258', 4, '', '麦园社区', '150', 1, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (260, 154, '0,100,129,154', 4, '', '排下社区', '150', 3, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (261, 129, '0,100,129', 3, '', '临江街道', '258', 12, '0', '0', 'ysyang', NULL, '', NULL);
INSERT INTO `b_area` VALUES (262, 261, '0,100,129,261', 4, '', '下池社区', '262', 1, '0', '0', 'ysyang', NULL, '', NULL);
INSERT INTO `b_area` VALUES (263, 157, '0,100,156,157', 4, '', '前横社区', '266', 4, '0', '0', 'ysyang', NULL, '', NULL);
INSERT INTO `b_area` VALUES (264, 101, '0,100,101', 3, '', '鼓西街道', '150', 1, '0', '0', 'yangnan', NULL, '', NULL);
INSERT INTO `b_area` VALUES (265, 264, '0,100,101,264', 4, '', '西湖社区', '150', 1, '0', '0', 'yangnan', NULL, '', NULL);
INSERT INTO `b_area` VALUES (266, 141, '0,100,140,141', 4, '', '泰山社区', '270', 2, '0', '0', 'ysyang', NULL, '', NULL);
INSERT INTO `b_area` VALUES (267, 130, '0,100,129,130', 4, '', '郑安村', '280', 6, '0', '0', 'ysyang', NULL, '', NULL);
INSERT INTO `b_area` VALUES (268, 258, '0,100,129,258', 4, '', '劳工社区', '150', 3, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (269, 258, '0,100,129,258', 4, '', '公园社区', '150', 4, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (270, 130, '0,100,129,130', 4, '', '湖岭社区', '280', 7, '0', '0', 'ysyang', NULL, '', NULL);
INSERT INTO `b_area` VALUES (271, 131, '0,100,129,131', 4, '', '红星社区', '271', 4, '0', '0', 'ysyang', NULL, 'ysyang', NULL);
INSERT INTO `b_area` VALUES (272, 239, '0,100,129,239', 4, '', '港头委', '2222', 3, '0', '0', 'ysyang', NULL, '', NULL);
INSERT INTO `b_area` VALUES (273, 131, '0,100,129,131', 4, '', '江南社区', '273', 5, '0', '0', 'ysyang', NULL, 'ysyang', NULL);
INSERT INTO `b_area` VALUES (274, 135, '0,100,129,135', 4, '', '胪厦村', '150', 10, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (275, 135, '0,100,129,135', 4, '', '城门村', '150', 11, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (276, 135, '0,100,129,135', 4, '', '胪峰社区', '150', 12, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (277, 131, '0,100,129,131', 4, '', '洋洽社区', '150', 133, '0', '0', 'yangnan', NULL, '', NULL);
INSERT INTO `b_area` VALUES (278, 131, '0,100,129,131', 4, '', '联建社区', '150', 33, '0', '0', 'yangnan', NULL, '', NULL);
INSERT INTO `b_area` VALUES (279, 157, '0,100,156,157', 4, '', '融侨东区社区', '155', 34, '0', '0', 'yangnan', NULL, '', NULL);
INSERT INTO `b_area` VALUES (280, 157, '0,100,156,157', 4, '', '瑞圣社区', '155', 35, '0', '0', 'yangnan', NULL, '', NULL);
INSERT INTO `b_area` VALUES (281, 157, '0,100,156,157', 4, '', '鹿溪社区', '155', 35, '0', '0', 'yangnan', NULL, '', NULL);
INSERT INTO `b_area` VALUES (282, 150, '0,100,129,150', 4, '', '屿浦社区', '150', 12, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (283, 150, '0,100,129,150', 4, '', '天水村', '150', 13, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (284, 150, '0,100,129,150', 4, '', '中山村', '150', 14, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (285, 150, '0,100,129,150', 4, '', '后坂村', '150', 15, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (286, 150, '0,100,129,150', 4, '', '竹榄村', '150', 16, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (287, 138, '0,100,129,138', 4, '', '金洪社区', '150', 19, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (288, 261, '0,100,129,261', 4, '', '仓前山社区', '150', 2, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (289, 138, '0,100,129,138', 4, '', '长埕村', '289', 20, '0', '0', 'ysyang', NULL, 'ysyang', NULL);
INSERT INTO `b_area` VALUES (290, 150, '0,100,129,150', 4, '', '黎升村', '290', 17, '0', '0', 'ysyang', NULL, '', NULL);
INSERT INTO `b_area` VALUES (291, 163, '0,100,129,163', 4, '', '湖畔社区', '150', 3, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (292, 129, '0,100,129', 3, '', '下渡派出所', '155', 33, '1', '0', 'yangnan', NULL, 'yangnan', NULL);
INSERT INTO `b_area` VALUES (293, 239, '0,100,129,239', 4, '', '下藤社区', '155', 33, '0', '0', 'yangnan', NULL, '', NULL);
INSERT INTO `b_area` VALUES (294, 101, '0,100,101', 3, '', '五凤街道', '150', 2, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (295, 294, '0,100,101,294', 4, '', '湖前社区', '150', 1, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (296, 138, '0,100,129,138', 4, '', '浦溪社区', '150', 21, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (297, 138, '0,100,129,138', 4, '', '建华社区', '150', 22, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (298, 135, '0,100,129,135', 4, '', '龙江村', '150', 13, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (299, 150, '0,100,129,150', 4, '', '跃进村', '150', 18, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (300, 150, '0,100,129,150', 4, '', '中亭村', '150', 19, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (301, 150, '0,100,129,150', 4, '', '上岐村', '150', 20, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (302, 135, '0,100,129,135', 4, '', '浚边村', '150', 14, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (303, 150, '0,100,129,150', 4, '', '盘屿社区', '150', 21, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (304, 150, '0,100,129,150', 4, '', '新安村', '1111', 22, '0', '0', 'ysyang', NULL, '', NULL);
INSERT INTO `b_area` VALUES (305, 150, '0,100,129,150', 4, '', '红农社区', '150', 23, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (306, 150, '0,100,129,150', 4, '', '吴山村', '150', 24, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (307, 150, '0,100,129,150', 4, '', '洋下村', '150', 25, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (308, 163, '0,100,129,163', 4, '', '东辉社区', '150', 4, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (309, 150, '0,100,129,150', 4, '', '叶下村', '150', 26, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (310, 135, '0,100,129,135', 4, '', '前福社区', '150', 15, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (311, 135, '0,100,129,135', 4, '', '三江口社区', '150', 16, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (312, 150, '0,100,129,150', 4, '', '浦下村', '150', 27, '0', '0', 'zgb', NULL, '', NULL);
INSERT INTO `b_area` VALUES (313, 163, '0,100,129,163', 4, '', '东南社区', '150', 5, '0', '0', 'zgb', NULL, '', NULL);

SET FOREIGN_KEY_CHECKS = 1;
