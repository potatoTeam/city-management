package com.chjim.basedata.domain;

import com.chjim.common.annotation.Excel;
import com.chjim.common.core.domain.BaseEntity;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 行政区划对象 b_area
 *
 * @author chjim
 */
@Data
public class Area extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 区划id */
    private Long areaId;

    /** 父区划id */
    @Excel(name = "父区划id")
    private Long parentId;

    /** 祖级列表 */
    @Excel(name = "祖级列表")
    private String ancestors;

    private int level;

    /** 区划类型 */
    @Excel(name = "区划类型")
    private String areaType;

    /** 区划名称 */
    @Excel(name = "区划名称")
    private String areaName;

    /** 编码 */
    @Excel(name = "编码")
    private String areaNo;

    /** 显示顺序 */
    @Excel(name = "显示顺序")
    private Long orderNum;

    /** 状态（0正常 1停用） */
    private String status;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 父部门名称 */
    private String parentName;

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("areaId", getAreaId())
                .append("parentId", getParentId())
                .append("ancestors", getAncestors())
                .append("areaType", getAreaType())
                .append("areaName", getAreaName())
                .append("areaNo", getAreaNo())
                .append("orderNum", getOrderNum())
                .append("status", getStatus())
                .append("delFlag", getDelFlag())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .toString();
    }
}
