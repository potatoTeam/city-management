package com.chjim.common.domain;

import com.chjim.common.utils.Convert;

import java.util.ArrayList;
import java.util.List;

public class DictPerson {

    public static long PERSON_DEFAULT = 1;  //系统默认的人员ID
    //CTID阈值
    public static double CTID_SCORE = 600d;
    //人员相片
    public static double PHOTO_RATIO = 441d / 358d;      //标准相片高宽比 身份证照片358:441
    public static int PHOTO_WIDTH_MAX = 358;      //相片最大宽度600像素
    public static int PHOTO_SIZE_MAX = 100 * 1000;  //相片最大100K

    public static int IDCARD_WIDTH_MAX = 1200;      //相片最大宽度600像素
    public static int IDCARD_SIZE_MAX = 100 * 1000;  //相片最大200K
    //相片缩略图
    public static int PHOTO_THUMB_WIDTH_MAX = 100;      //相片最大宽度100像素
    public static int PHOTO_THUMB_SIZE_MAX = 30 * 1000;  //缩略图最大30K

    //男女
    public static String SEX_UNKNOWN = "0";
    public static String SEX_MALE = "1";
    public static String SEX_FEMALE = "2";
    public static String SEX_MALE_EN = "male";
    public static String SEX_FEMALE_EN = "female";

    //高龄
    public static int AGE_OLD = 70;

    //政治面貌
    public static String ZZMM_CPC = "1";

    //证件类型
    public static String ID_TYPE_CHINESE = "0";
    public static String ID_TYPE_GUARD = "4";
    public static String ID_TYPE_OTHER = "5";

    //人员类型：只用于通行记录中
    public static String PersonType_Stranger = "0"; //陌生人
    public static String PersonType_Visitor = "1"; //访客
    //居民类型为字典RESIDENT_TYPE

    public static int ERR_ID_NUMBER_ = 1010;
    public static int ERR_ID_NUMBER = 1011;
    public static int ERR_CN_ID_NUMBER = 1012;
    public static int ERR_PHONE_ = 1020;
    public static int ERR_PHONE = 1021;

    public static String MSG_ERR_ID_NUMBER_ = "该证件号码已被登记";
    public static String MSG_ERR_ID_NUMBER = "请填写正确的证件号码";
    public static String MSG_ERR_CN_ID_NUMBER = "请填写正确的身份证号";
    public static String MSG_ERR_PHONE_ = "该手机号码已被登记";
    public static String MSG_ERR_PHONE = "请填写正确的手机号码";

    public static String ERR_USER_WX_BIND = "该证件号码已绑定微信";
    public static String ERR_PERSON_BIND = "该证件号码已登记，与您填写的信息不一致！";

    public static String Sign_Way_WX = "0"; //微信登记
    public static String Sign_Way_WX_ROOM = "1"; //微信 同住添加
    public static String Sign_Way_WX_MGR = "2"; //微信 物业添加
    public static String Sign_Way_MGR_PERSON = "3"; //后台添加人员
    public static String Sign_Way_MGR_RESIDENT = "4"; //后台添加居民
    public static String Sign_Way_SFZ_AUTO = "5"; //身份证自动登记
    public static String Sign_Way_MGR_IMPORT = "6"; //后台导入
    public static String Sign_Way_WX_SFZ = "7"; //微信 身份证登记
    public static String Sign_Way_Other = "9"; //其他

    public static void init() {
        //性别
        List<Conf> sex = new ArrayList<>();
        sex.add(new Conf("-", "0"));
        sex.add(new Conf("男", "1"));
        sex.add(new Conf("女", "2"));
        ConfDict.confDictMap.put("INFO_SEX", sex);

        //政治面貌
        List<Conf> zzmm = new ArrayList<>();
        zzmm.add(new Conf("群众", "13"));
        zzmm.add(new Conf("中共党员", "1"));
        zzmm.add(new Conf("共青团员", "3"));
        zzmm.add(new Conf("民主党派", "15"));
        ConfDict.confDictMap.put("INFO_ZZMM", zzmm);

        //证件类型
        List<Conf> cardType = new ArrayList<>();
        cardType.add(new Conf("居民身份证", "0"));
        cardType.add(new Conf("港澳身份证件", "1"));
        cardType.add(new Conf("台湾身份证件", "2"));
        cardType.add(new Conf("护照", "3"));
        cardType.add(new Conf("军官证", "4"));
        cardType.add(new Conf("其他证件", "5"));
        ConfDict.confDictMap.put("INFO_CARD_TYPE", cardType);

        //登记方式
        List<Conf> signWay = new ArrayList<>();
        signWay.add(new Conf("微信登记", "0"));
        signWay.add(new Conf("同住微信添加", "1"));
        signWay.add(new Conf("物业微信添加", "2"));
        signWay.add(new Conf("后台添加人员", "3"));
        signWay.add(new Conf("后台添加居民", "4"));
        signWay.add(new Conf("身份证自动登记", "5"));
        signWay.add(new Conf("后台导入", "6"));
        signWay.add(new Conf("身份证微信登记", "7"));
        signWay.add(new Conf("其他方式", "9"));
        ConfDict.confDictMap.put("SIGN_WAY", signWay);

        //关怀类型
        List<Conf> careType = new ArrayList<>();
        careType.add(new Conf("无", "0"));
        careType.add(new Conf("高龄", "1"));
        careType.add(new Conf("精神问题", "2"));
        careType.add(new Conf("孤寡人员", "3"));
        careType.add(new Conf("留守儿童", "4"));
        careType.add(new Conf("其他", "9"));
        ConfDict.confDictMap.put("INFO_CARE_TYPE", careType);

        //关注类型
        List<Conf> stareType = new ArrayList<>();
        stareType.add(new Conf("无", "0"));
        stareType.add(new Conf("高龄", "1"));
        stareType.add(new Conf("精神问题", "2"));
        stareType.add(new Conf("孤寡人员", "3"));
        stareType.add(new Conf("留守儿童", "4"));
        stareType.add(new Conf("其他", "9"));
        ConfDict.confDictMap.put("INFO_STARE_TYPE", stareType);

    }

    //闽清上报 证件类型
    public static int toMqCardType(String cardType) {
        return Convert.toInt(cardType) + 1;
    }
}
