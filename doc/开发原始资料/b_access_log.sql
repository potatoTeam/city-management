/*
 Navicat MySQL Data Transfer

 Source Server         : mysql8
 Source Server Type    : MySQL
 Source Server Version : 80031
 Source Host           : localhost:3806
 Source Schema         : smartpark

 Target Server Type    : MySQL
 Target Server Version : 80031
 File Encoding         : 65001

 Date: 11/12/2023 10:29:56
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for b_access_log
-- ----------------------------
DROP TABLE IF EXISTS `b_access_log`;
CREATE TABLE `b_access_log`  (
  `record_id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '记录ID',
  `town_no` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '小区编号',
  `town_name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '小区名称',
  `ent_no` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '门禁编号',
  `ent_name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '门禁名称',
  `ent_platform` tinyint(0) NULL DEFAULT NULL COMMENT '门禁平台',
  `person_id` bigint(0) NULL DEFAULT 0 COMMENT '人员ID',
  `person_name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '人员姓名',
  `id_number` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '证件号码',
  `resident_id` bigint(0) NULL DEFAULT NULL COMMENT '居民ID',
  `visit_id` bigint(0) NULL DEFAULT NULL COMMENT '拜访记录ID',
  `person_type` char(2) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '0' COMMENT '人员类型',
  `person_photo` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '人员相片',
  `access_time` datetime(0) NULL DEFAULT NULL COMMENT '通行时间',
  `access_type` char(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '通行类型',
  `cmp_type` char(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '2' COMMENT '比对方式',
  `cmp_score` int(0) NULL DEFAULT 0 COMMENT '比对分值',
  `cmp_result` char(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '1' COMMENT '比对结果',
  `temperature` int(0) NULL DEFAULT NULL COMMENT '体温',
  `back_img` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL,
  `capture_img` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '通行相片',
  `status_post` char(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '0' COMMENT 'tc上报状态',
  `status_zt` char(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '0' COMMENT 'zt上报状态',
  `status` char(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '0' COMMENT '状态(0无效1有效)',
  `del_flag` char(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '0' COMMENT '删除标志(0存在2删除)',
  `create_by` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`record_id`) USING BTREE,
  INDEX `fk_person`(`person_id`) USING BTREE,
  INDEX `idx_access_time`(`access_time`) USING BTREE,
  INDEX `idx_town_access`(`town_no`, `access_time`) USING BTREE,
  INDEX `fk_ent`(`ent_no`) USING BTREE,
  CONSTRAINT `fk_ent` FOREIGN KEY (`ent_no`) REFERENCES `b_entrance` (`ent_no`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_person` FOREIGN KEY (`person_id`) REFERENCES `b_person` (`record_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_town` FOREIGN KEY (`town_no`) REFERENCES `b_town` (`town_no`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 9004707 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci COMMENT = '通行记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of b_access_log
-- ----------------------------
INSERT INTO `b_access_log` VALUES (6724615, 'Q220800037', '迎霞新城南区', 'E220900057', '迎霞南区大门入口', 1, 16455, '胡亮', '350702199112300318', 16584, NULL, '12', '/profile/photo/2022/09/15/da0713da-7f0e-42ab-8e14-5d1519e11c92.jpg', '2023-06-16 12:34:12', '0', '2', 0, '1', NULL, NULL, '/profile/access\\2023/06/16\\bc7190eb-a368-4fea-a707-b5e4fc300220.jpg', '1', '1', '0', '0', '', '2023-06-16 12:34:14', '', '2023-06-17 04:00:04', NULL);
INSERT INTO `b_access_log` VALUES (6724620, 'Q220800037', '迎霞新城南区', 'E220900057', '迎霞南区大门入口', 1, 16455, '胡亮', '350702199112300318', 16584, NULL, '12', '/profile/photo/2022/09/15/da0713da-7f0e-42ab-8e14-5d1519e11c92.jpg', '2023-06-16 12:34:18', '0', '2', 0, '1', NULL, NULL, '/profile/access\\2023/06/16\\9952f8d5-c1fb-4fc3-aac1-0edf69d79e4b.jpg', '1', '1', '0', '0', '', '2023-06-16 12:34:20', '', '2023-06-17 04:00:03', NULL);
INSERT INTO `b_access_log` VALUES (7083052, 'Q220800068', '金凤新苑', 'E220900037', '东南门出口', 1, 47195, '黄丽梅', '350125199204030062', 50626, NULL, '11', '/profile/photo/2022/10/04/33c96997-29f7-407c-8c8b-009a4c70b2d6.jpg', '2023-06-26 12:49:14', '1', '2', 0, '1', NULL, NULL, '/profile/access\\2023/06/28\\42186f0e-eb48-4f7d-adba-c66e0b4a6365.jpg', '1', '1', '0', '0', '', '2023-06-28 09:49:19', '', '2023-07-03 08:05:12', NULL);
INSERT INTO `b_access_log` VALUES (7086595, 'Q220800068', '金凤新苑', 'E220900037', '东南门出口', 1, 47195, '黄丽梅', '350125199204030062', 50626, NULL, '11', '/profile/photo/2022/10/04/33c96997-29f7-407c-8c8b-009a4c70b2d6.jpg', '2023-06-18 08:38:15', '1', '2', 0, '1', NULL, NULL, '/profile/access\\2023/06/28\\05a8a5d9-343a-4a35-8fec-1166c663c737.jpg', '0', '0', '0', '0', '', '2023-06-28 11:02:03', '', NULL, NULL);
INSERT INTO `b_access_log` VALUES (7087338, 'Q220800068', '金凤新苑', 'E220900037', '东南门出口', 1, 47195, '黄丽梅', '350125199204030062', 50626, NULL, '11', '/profile/photo/2022/10/04/33c96997-29f7-407c-8c8b-009a4c70b2d6.jpg', '2023-06-16 07:53:07', '1', '2', 0, '1', NULL, NULL, '/profile/access\\2023/06/28\\8a35d536-d3d9-44d5-a8ff-1a839502624a.jpg', '0', '0', '0', '0', '', '2023-06-28 11:17:03', '', NULL, NULL);
INSERT INTO `b_access_log` VALUES (7090390, 'Q220800068', '金凤新苑', 'E220900037', '东南门出口', 1, 47195, '黄丽梅', '350125199204030062', 50626, NULL, '11', '/profile/photo/2022/10/04/33c96997-29f7-407c-8c8b-009a4c70b2d6.jpg', '2023-06-08 12:31:04', '1', '2', 0, '1', NULL, NULL, '/profile/access\\2023/06/28\\b49f2ac6-1d7e-4f5f-a2fc-5054f34e61f1.jpg', '0', '0', '0', '0', '', '2023-06-28 12:19:02', '', NULL, NULL);
INSERT INTO `b_access_log` VALUES (7091121, 'Q220800068', '金凤新苑', 'E220900037', '东南门出口', 1, 47195, '黄丽梅', '350125199204030062', 50626, NULL, '11', '/profile/photo/2022/10/04/33c96997-29f7-407c-8c8b-009a4c70b2d6.jpg', '2023-06-06 13:28:29', '1', '2', 0, '1', NULL, NULL, '/profile/access\\2023/06/28\\1bc5a6c9-4a29-4c7e-81f3-666b435a57a0.jpg', '0', '0', '0', '0', '', '2023-06-28 12:33:01', '', NULL, NULL);
INSERT INTO `b_access_log` VALUES (7092371, 'Q220800068', '金凤新苑', 'E220900037', '东南门出口', 1, 47195, '黄丽梅', '350125199204030062', 50626, NULL, '11', '/profile/photo/2022/10/04/33c96997-29f7-407c-8c8b-009a4c70b2d6.jpg', '2023-06-02 09:03:21', '1', '2', 0, '1', NULL, NULL, '/profile/access\\2023/06/28\\dd77cc47-60c9-4b3a-811a-3fb3e27a9a0b.jpg', '0', '0', '0', '0', '', '2023-06-28 13:05:25', '', NULL, NULL);
INSERT INTO `b_access_log` VALUES (7094919, 'Q220800068', '金凤新苑', 'E220900037', '东南门出口', 1, 47195, '黄丽梅', '350125199204030062', 50626, NULL, '11', '/profile/photo/2022/10/04/33c96997-29f7-407c-8c8b-009a4c70b2d6.jpg', '2023-05-23 12:59:08', '1', '2', 0, '1', NULL, NULL, '/profile/access\\2023/06/28\\6eba849a-2c28-4df4-80bc-3e82fa4f4e19.jpg', '0', '0', '0', '0', '', '2023-06-28 13:59:24', '', NULL, NULL);
INSERT INTO `b_access_log` VALUES (7102244, 'Q220800068', '金凤新苑', 'E220900037', '东南门出口', 1, 47195, '黄丽梅', '350125199204030062', 50626, NULL, '11', '/profile/photo/2022/10/04/33c96997-29f7-407c-8c8b-009a4c70b2d6.jpg', '2023-05-14 12:23:02', '1', '2', 0, '1', NULL, NULL, '/profile/access\\2023/06/28\\4284e79e-e63f-4883-be50-01de11e48f06.jpg', '0', '0', '0', '0', '', '2023-06-28 15:07:57', '', NULL, NULL);
INSERT INTO `b_access_log` VALUES (7187674, 'Q220800068', '金凤新苑', 'E220900037', '东南门出口', 1, 47195, '黄丽梅', '350125199204030062', 50626, NULL, '11', '/profile/photo/2022/10/04/33c96997-29f7-407c-8c8b-009a4c70b2d6.jpg', '2023-06-30 08:38:12', '1', '2', 0, '1', NULL, NULL, '/profile/access\\2023/06/30\\3bcba8ce-459e-49da-8ba7-37ad0e5b0304.jpg', '1', '1', '0', '0', '', '2023-06-30 08:44:03', '', '2023-07-03 08:02:21', NULL);
INSERT INTO `b_access_log` VALUES (7348295, 'Q220800068', '金凤新苑', 'E220900037', '东南门出口', 1, 47195, '黄丽梅', '350125199204030062', 50626, NULL, '11', '/profile/photo/2022/10/04/33c96997-29f7-407c-8c8b-009a4c70b2d6.jpg', '2023-07-05 08:33:41', '1', '2', 0, '1', NULL, NULL, '/profile/access\\2023/07/05\\178add56-9feb-4c1a-a468-ac690e44d5ac.jpg', '1', '1', '0', '0', '', '2023-07-05 08:39:42', '', '2023-07-07 03:44:33', NULL);
INSERT INTO `b_access_log` VALUES (7539112, 'Q220800068', '金凤新苑', 'E220900037', '东南门出口', 1, 47195, '黄丽梅', '350125199204030062', 50626, NULL, '11', '/profile/photo/2022/10/04/33c96997-29f7-407c-8c8b-009a4c70b2d6.jpg', '2023-07-11 13:28:02', '1', '2', 0, '1', NULL, NULL, '/profile/access\\2023/07/11\\9ac26588-4471-47ae-8e69-ebc4ee280a52.jpg', '1', '1', '0', '0', '', '2023-07-11 13:34:19', '', '2023-07-13 03:29:04', NULL);
INSERT INTO `b_access_log` VALUES (7596462, 'Q220800068', '金凤新苑', 'E220900037', '东南门出口', 1, 47195, '黄丽梅', '350125199204030062', 50626, NULL, '11', '/profile/photo/2022/10/04/33c96997-29f7-407c-8c8b-009a4c70b2d6.jpg', '2023-07-13 12:21:38', '1', '2', 0, '1', NULL, NULL, '/profile/access\\2023/07/13\\3606bf17-3baa-4b45-990e-dbc8fa0b8060.jpg', '1', '1', '0', '0', '', '2023-07-13 12:27:59', '', '2023-07-15 03:12:30', NULL);
INSERT INTO `b_access_log` VALUES (7622196, 'Q220800068', '金凤新苑', 'E220900037', '东南门出口', 1, 47195, '黄丽梅', '350125199204030062', 50626, NULL, '11', '/profile/photo/2022/10/04/33c96997-29f7-407c-8c8b-009a4c70b2d6.jpg', '2023-07-14 13:29:48', '1', '2', 0, '1', NULL, NULL, '/profile/access\\2023/07/14\\4d550c49-2dd4-4d66-82e5-543e3e2b9722.jpg', '1', '1', '0', '0', '', '2023-07-14 13:36:12', '', '2023-07-16 03:13:22', NULL);
INSERT INTO `b_access_log` VALUES (7642580, 'Q220800068', '金凤新苑', 'E220900037', '东南门出口', 1, 47195, '黄丽梅', '350125199204030062', 50626, NULL, '11', '/profile/photo/2022/10/04/33c96997-29f7-407c-8c8b-009a4c70b2d6.jpg', '2023-07-15 08:45:45', '1', '2', 0, '1', NULL, NULL, '/profile/access\\2023/07/15\\2131c711-57a4-46b6-8781-20d7de54e8de.jpg', '1', '1', '0', '0', '', '2023-07-15 08:52:11', '', '2023-07-17 03:10:47', NULL);
INSERT INTO `b_access_log` VALUES (7681836, 'Q220800068', '金凤新苑', 'E220900037', '东南门出口', 1, 47195, '黄丽梅', '350125199204030062', 50626, NULL, '11', '/profile/photo/2022/10/04/33c96997-29f7-407c-8c8b-009a4c70b2d6.jpg', '2023-07-16 12:13:17', '1', '2', 0, '1', NULL, NULL, '/profile/access\\2023/07/16\\e8de26f5-2151-4e5f-a7cc-cbfcbfc8445a.jpg', '1', '1', '0', '0', '', '2023-07-16 12:19:45', '', '2023-07-18 03:25:34', NULL);
INSERT INTO `b_access_log` VALUES (7725012, 'Q220800068', '金凤新苑', 'E220900037', '东南门出口', 1, 47195, '黄丽梅', '350125199204030062', 50626, NULL, '11', '/profile/photo/2022/10/04/33c96997-29f7-407c-8c8b-009a4c70b2d6.jpg', '2023-07-18 08:03:30', '1', '2', 0, '1', NULL, NULL, '/profile/access\\2023/07/18\\b15d3741-3e69-41b9-84e2-948dd789f87f.jpg', '1', '1', '0', '0', '', '2023-07-18 08:10:01', '', '2023-07-20 03:29:17', NULL);
INSERT INTO `b_access_log` VALUES (7756729, 'Q220800068', '金凤新苑', 'E220900037', '东南门出口', 1, 47195, '黄丽梅', '350125199204030062', 50626, NULL, '11', '/profile/photo/2022/10/04/33c96997-29f7-407c-8c8b-009a4c70b2d6.jpg', '2023-07-19 13:18:31', '1', '2', 0, '1', NULL, NULL, '/profile/access\\2023/07/19\\746e8450-b69a-4326-845d-4486c88b5936.jpg', '1', '1', '0', '0', '', '2023-07-19 13:25:06', '', '2023-07-21 03:26:48', NULL);
INSERT INTO `b_access_log` VALUES (7774772, 'Q220800068', '金凤新苑', 'E220900037', '东南门出口', 1, 47195, '黄丽梅', '350125199204030062', 50626, NULL, '11', '/profile/photo/2022/10/04/33c96997-29f7-407c-8c8b-009a4c70b2d6.jpg', '2023-07-20 08:46:10', '1', '2', 0, '1', NULL, NULL, '/profile/access\\2023/07/20\\b3360f9c-2f30-4556-aa2d-56d0fcb29f12.jpg', '1', '1', '0', '0', '', '2023-07-20 08:52:45', '', '2023-07-22 03:41:56', NULL);
INSERT INTO `b_access_log` VALUES (7834692, 'Q220800068', '金凤新苑', 'E220900037', '东南门出口', 1, 47195, '黄丽梅', '350125199204030062', 50626, NULL, '11', '/profile/photo/2022/10/04/33c96997-29f7-407c-8c8b-009a4c70b2d6.jpg', '2023-07-22 08:48:42', '1', '2', 0, '1', NULL, NULL, '/profile/access\\2023/07/22\\7d12c9a6-900d-484d-b563-6da8f54abf47.jpg', '1', '1', '0', '0', '', '2023-07-22 08:55:20', '', '2023-07-24 03:44:26', NULL);
INSERT INTO `b_access_log` VALUES (7870981, 'Q220800068', '金凤新苑', 'E220900037', '东南门出口', 2, 47195, '黄丽梅', '350125199204030062', 50626, NULL, '11', '/profile/photo/2022/10/04/33c96997-29f7-407c-8c8b-009a4c70b2d6.jpg', '2023-07-23 13:22:38', '1', '2', 0, '1', NULL, NULL, '/profile/access\\2023/07/23\\42532063-30d7-4a5d-be4d-141239760b0e.jpg', '1', '1', '0', '0', '', '2023-07-23 13:29:18', '', '2023-07-25 11:21:50', NULL);
INSERT INTO `b_access_log` VALUES (7935306, 'Q220800068', '金凤新苑', 'E220900037', '东南门出口', 3, 47195, '黄丽梅', '350125199204030062', 50626, NULL, '11', '/profile/photo/2022/10/04/33c96997-29f7-407c-8c8b-009a4c70b2d6.jpg', '2023-07-25 13:34:10', '1', '2', 0, '1', NULL, NULL, '/profile/access\\2023/07/25\\f875503a-2281-4249-9c9b-4865cac948f4.jpg', '1', '1', '5', '0', '', '2023-07-25 13:40:55', '', '2023-09-27 15:54:35', 'sdfsf');
INSERT INTO `b_access_log` VALUES (8028563, 'Q220800068', '金凤新苑', 'E220900037', '东南门出口', 3, 47195, '黄丽梅', '350125199204030062', 50626, NULL, '11', '/profile/photo/2022/10/04/33c96997-29f7-407c-8c8b-009a4c70b2d6.jpg', '2023-07-28 13:32:17', '1', '2', 0, '1', NULL, NULL, '/profile/access\\2023/07/28\\efaee304-aa83-422a-8149-477f7eb16cfc.jpg', '1', '1', '5', '0', '', '2023-07-28 13:39:08', '', '2023-09-27 15:45:33', '已处理');
INSERT INTO `b_access_log` VALUES (8041266, 'Q220800068', '金凤新苑', 'E220900037', '东南门出口', 3, 47195, '黄丽梅', '350125199204030062', 50626, NULL, '11', '/profile/photo/2022/10/04/33c96997-29f7-407c-8c8b-009a4c70b2d6.jpg', '2023-07-29 08:41:03', '1', '2', 0, '1', NULL, NULL, '/profile/access\\2023/07/29\\853ee262-e7d1-4212-b410-ce1c64ef9027.jpg', '1', '1', '5', '0', '', '2023-07-29 08:47:54', '陈警官', '2023-09-27 22:47:49', 'dsfsdf');
INSERT INTO `b_access_log` VALUES (8164943, 'Q220800068', '金凤新苑', 'E220900037', '东南门出口', 2, 47195, '黄丽梅', '350125199204030062', 50626, NULL, '11', '/profile/photo/2022/10/04/33c96997-29f7-407c-8c8b-009a4c70b2d6.jpg', '2023-08-02 08:39:14', '1', '2', 0, '1', NULL, NULL, '/profile/access\\2023/08/02\\e090b006-d0a8-4673-b9d7-ba40cfb41e49.jpg', '1', '1', '0', '0', '', '2023-08-02 08:46:11', '', '2023-08-04 03:40:14', NULL);
INSERT INTO `b_access_log` VALUES (8239020, 'Q220800068', '金凤新苑', 'E220900037', '东南门出口', 2, 47195, '黄丽梅', '350125199204030062', 50626, NULL, '11', '/profile/photo/2022/10/04/33c96997-29f7-407c-8c8b-009a4c70b2d6.jpg', '2023-08-04 08:46:22', '1', '2', 0, '1', NULL, NULL, '/profile/access\\2023/08/04\\1e0f5dd4-c3d3-4c5b-a813-afaedc1eec70.jpg', '1', '1', '0', '0', '', '2023-08-04 08:53:25', '', '2023-08-06 03:54:23', NULL);
INSERT INTO `b_access_log` VALUES (8368644, 'Q220800068', '金凤新苑', 'E220900037', '东南门出口', 1, 47195, '黄丽梅', '350125199204030062', 50626, NULL, '11', '/profile/photo/2022/10/04/33c96997-29f7-407c-8c8b-009a4c70b2d6.jpg', '2023-08-07 12:32:16', '1', '2', 0, '1', NULL, NULL, '/profile/access\\2023/08/07\\93d45eec-c161-4f55-adc5-f5ef01e8e7b4.jpg', '1', '1', '0', '0', '', '2023-08-07 12:39:26', '', '2023-08-08 03:56:10', NULL);
INSERT INTO `b_access_log` VALUES (8462350, 'Q220800068', '金凤新苑', 'E220900037', '东南门出口', 2, 47195, '黄丽梅', '350125199204030062', 50626, NULL, '11', '/profile/photo/2022/10/04/33c96997-29f7-407c-8c8b-009a4c70b2d6.jpg', '2023-08-10 13:21:41', '1', '2', 0, '1', NULL, NULL, '/profile/access\\2023/08/10\\c04f1a63-2cb4-4924-8a7d-3140f41af8eb.jpg', '1', '1', '0', '0', '', '2023-08-10 13:28:57', '', '2023-08-12 03:49:49', NULL);
INSERT INTO `b_access_log` VALUES (8478730, 'Q220800068', '金凤新苑', 'E220900038', '东南门入口', 2, 47195, '黄丽梅', '350125199204030062', 50626, NULL, '11', '/profile/photo/2022/10/04/33c96997-29f7-407c-8c8b-009a4c70b2d6.jpg', '2023-08-10 23:42:56', '0', '2', 0, '1', NULL, NULL, '/profile/access\\2023/08/10\\c41437ee-58af-4957-baa9-ff0941132473.jpg', '1', '1', '0', '0', '', '2023-08-10 23:47:47', '', '2023-08-11 03:49:45', NULL);
INSERT INTO `b_access_log` VALUES (8485922, 'Q220800068', '金凤新苑', 'E220900037', '东南门出口', 3, 47195, '黄丽梅', '350125199204030062', 50626, NULL, '11', '/profile/photo/2022/10/04/33c96997-29f7-407c-8c8b-009a4c70b2d6.jpg', '2023-08-11 08:41:19', '1', '2', 0, '1', NULL, NULL, '/profile/access\\2023/08/11\\4a558e4c-0200-4a16-baf2-d48866ae2d12.jpg', '1', '1', '5', '0', '', '2023-08-11 08:48:36', '', '2023-09-27 18:21:32', '啊啊啊啊');
INSERT INTO `b_access_log` VALUES (8524794, 'Q220800068', '金凤新苑', 'E220900037', '东南门出口', 1, 47195, '黄丽梅', '350125199204030062', 50626, NULL, '11', '/profile/photo/2022/10/04/33c96997-29f7-407c-8c8b-009a4c70b2d6.jpg', '2023-08-12 13:26:44', '1', '2', 0, '1', NULL, NULL, '/profile/access\\2023/08/12\\6934a37c-b2ad-4bf4-a687-3c7909a05129.jpg', '1', '1', '0', '0', '', '2023-08-12 13:34:03', '', '2023-08-15 09:33:07', NULL);
INSERT INTO `b_access_log` VALUES (8612001, 'Q220800068', '金凤新苑', 'E220900037', '东南门出口', 1, 47195, '黄丽梅', '350125199204030062', 50626, NULL, '11', '/profile/photo/2022/10/04/33c96997-29f7-407c-8c8b-009a4c70b2d6.jpg', '2023-08-15 08:45:10', '1', '2', 0, '1', NULL, NULL, '/profile/access\\2023/08/15\\ec59248c-e908-40f8-ab55-28da16db7cea.jpg', '1', '1', '0', '0', '', '2023-08-15 08:52:33', '', '2023-08-17 04:01:35', NULL);
INSERT INTO `b_access_log` VALUES (8779047, 'Q220800037', '迎霞新城南区', 'E220900056', '迎霞南区大门出口', 1, 16455, '胡亮', '350702199112300318', 16584, NULL, '12', '/profile/photo/2022/09/15/da0713da-7f0e-42ab-8e14-5d1519e11c92.jpg', '2023-08-20 09:39:41', '1', '2', 0, '1', NULL, NULL, '/profile/access\\2023/08/20\\97a22820-0de6-45c3-b443-7632c03da52d.jpg', '1', '1', '0', '0', '', '2023-08-20 09:39:42', '', '2023-08-22 03:13:36', NULL);
INSERT INTO `b_access_log` VALUES (8812754, 'Q220800068', '金凤新苑', 'E220900037', '东南门出口', 1, 47195, '黄丽梅', '350125199204030062', 50626, NULL, '11', '/profile/photo/2022/10/04/33c96997-29f7-407c-8c8b-009a4c70b2d6.jpg', '2023-08-21 08:59:43', '1', '2', 0, '1', NULL, NULL, '/profile/access\\2023/08/21\\1e891700-0d5d-4c3f-b7a0-c3bd1b49eccd.jpg', '1', '1', '0', '0', '', '2023-08-21 09:07:16', '', '2023-08-23 03:34:41', NULL);
INSERT INTO `b_access_log` VALUES (8850324, 'Q220800068', '金凤新苑', 'E220900037', '东南门出口', 1, 47195, '黄丽梅', '350125199204030062', 50626, NULL, '11', '/profile/photo/2022/10/04/33c96997-29f7-407c-8c8b-009a4c70b2d6.jpg', '2023-08-22 13:39:38', '1', '2', 0, '1', NULL, NULL, '/profile/access\\2023/08/22\\458639f5-4571-4a76-a866-8943c40d9da1.jpg', '1', '1', '0', '0', '', '2023-08-22 13:47:13', '', '2023-08-24 03:57:13', NULL);
INSERT INTO `b_access_log` VALUES (8947083, 'Q220800145', '福建商贸学校', 'E220900119', '福建商贸学校首山校区大门入	', 3, 47195, '黄丽梅', '350125199204030062', 50626, NULL, '11', '/profile/photo/2022/10/04/33c96997-29f7-407c-8c8b-009a4c70b2d6.jpg', '2023-12-22 12:31:23', '1', '2', 0, '1', NULL, NULL, '/profile/access\\2023/08/25\\70d6319e-17ba-4904-ab39-9a4ad1280b6b.jpg', '1', '0', '0', '0', '', '2023-08-25 08:39:03', '', '2023-08-26 04:30:21', NULL);

SET FOREIGN_KEY_CHECKS = 1;
