package com.chjim.basedata.domain;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.chjim.common.core.domain.BaseEntity;
import com.chjim.common.core.domain.IDatum;
import com.chjim.common.annotation.Excel;
import com.chjim.common.annotation.Excel.Type;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 人员对象 b_person
 *
 * @author chjim
 */
@Data
public class Person extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 记录ID
     */
    private Long recordId;

    /**
     * 小区编号
     */
    @Excel(name = "小区编号", type = Type.EXPORT)
    private String townNo;
    private String townName;
//    private Town town;

    /**
     * 姓名
     */
    @Excel(name = "姓名")
    private String name;

    /**
     * 性别
     */
    @Excel(name = "性别")
    private String sex;

    /**
     * 政治面貌
     */
    @Excel(name = "政治面貌")
    private String zzmm;

    private List<Card> cardList;

    /**
     * 证件类型
     */
    @Excel(name = "证件类型")
    private String cardType;

    /**
     * 证件号码
     */
    @Excel(name = "证件号码")
    private String idNumber;

    /**
     * 年龄
     */
    private int age;

    /**
     * 出生日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "出生日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date bornDate;

    /**
     * 身份证地址
     */
    @Excel(name = "身份证地址")
    private String idCardAddr;

    /**
     * 职业
     */
    @Excel(name = "职业")
    private String job;

    /**
     * 联系电话
     */
    @Excel(name = "联系电话")
    private String phone;

    /**
     * 照片
     */
    @Excel(name = "照片")
    private String photo;

    /**
     * 新大陆CTID认证bid
     */
    @Excel(name = "新大陆CTID认证bid")
    private String bid;

    /**
     * CTID认证(0认证1不认证)
     */
    @Excel(name = "CTID认证(0认证1不认证)")
    private String ctidFlag;

    /**
     * 车牌号码
     */
    @Excel(name = "车牌号码")
    private String carNumber;

    /**
     * 是否特殊人员
     */
    @Excel(name = "是否特殊人员")
    private String isSpecial;

    /**
     * 特殊情况说明
     */
    @Excel(name = "特殊情况说明")
    private String specialInfo;

    /**
     * 关注类型
     */
    @Excel(name = "关注类型")
    private String stare;

    /**
     * 关怀类型
     */
    @Excel(name = "关怀类型")
    private String care;

    /**
     * 关怀原因
     */
    @Excel(name = "关怀原因")
    private String careInfo;

    private Integer zoneId;

    private Integer ptype;

    /**
     * 楼栋
     */
    @Excel(name = "楼栋")
    private String buildName;

    /**
     * 房屋
     */
    @Excel(name = "房屋")
    private String houseName;

    /**
     * 人员类型
     */
    @Excel(name = "人员类型")
    private String personType;

    /**
     * 是否房主
     */
    @Excel(name = "是否房主")
    private String isOwner;

    /**
     * 是否负责人
     */
    @Excel(name = "是否负责人")
    private String isLead;

//    @Excel(name = "健康码")
//    private Integer healthCode;
//
//    @Excel(name = "疫苗")
//    private Integer antiRet;
//
//    @Excel(name = "通行相片")
//    private String antiSrc;
//
//    @Excel(name = "核酸")
//    private Integer rnaRet;
//
//    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
//    @Excel(name = "核酸时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm")
//    private Date rnaTime;
//
//    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
//    @Excel(name = "健康信息更新时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm")
//    private Date healthTime;

    private String signWay;

    /**
     * 状态
     */
    private String status;

    /**
     * 删除标志
     */
    private String delFlag;

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("recordId", getRecordId())
                .append("name", getName())
                .append("sex", getSex())
                .append("zzmm", getZzmm())
                .append("cardType", getCardType())
                .append("idNumber", getIdNumber())
                .append("bornDate", getBornDate())
                .append("idCardAddr", getIdCardAddr())
                .append("job", getJob())
                .append("phone", getPhone())
                .append("photo", getPhoto())
                .append("bid", getBid())
                .append("ctidFlag", getCtidFlag())
                .append("carNumber", getCarNumber())
                .append("care", getCare())
                .append("isSpecial", getIsSpecial())
                .append("specialInfo", getSpecialInfo())
                .append("status", getStatus())
                .append("delFlag", getDelFlag())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .append("remark", getRemark())
                .toString();
    }
}
