package com.chjim.basedata.service.impl;

import com.alibaba.druid.sql.dialect.mysql.ast.clause.ConditionValue;
import com.chjim.basedata.domain.Area;
import com.chjim.basedata.domain.Town;
import com.chjim.basedata.mapper.AreaMapper;
import com.chjim.basedata.service.IAreaService;
import com.chjim.common.annotation.DataScope;
import com.chjim.common.constant.UserConstants;
import com.chjim.common.core.domain.Ztree;
import com.chjim.common.core.domain.entity.SysRole;
import com.chjim.common.core.domain.entity.SysUser;
import com.chjim.common.exception.BusinessException;
import com.chjim.common.utils.Convert;
import com.chjim.common.utils.StringUtils;
import com.chjim.system.service.ISysUserService;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * 部门管理 服务实现
 */
@Service
public class AreaServiceImpl implements IAreaService {
    @Autowired
    private AreaMapper areaMapper;

    @Autowired
    private ISysUserService sysUserService;

    /**
     * 查询部门管理数据
     *
     * @param area 部门信息
     * @return 部门信息集合
     */
    @Override
    //@DataScope(areaAlias = "d")
    public List<Area> selectAreaList(Area area) {
        return areaMapper.selectAreaList(area);
    }

    @Override
    public List<Area> selectAllAreaList() {
        return areaMapper.selectAllAreaList();
    }

    @Override
    public List<Area> selectSubAreaList() {
        return areaMapper.selectSubAreaList();
    }

    @Override
    public int getAreaCount(List<Town> townList) {
        int count = 0;
        String areas = "";
        for (Town town : townList) {
            if (areas.indexOf(town.getAreaId() + " ") < 0) {
                areas += town.getAreaId() + " ";
                count++;
            }
        }
        return count;
    }

    @Override
    public long getCount(Area area){
        return areaMapper.getCount(area);
    }

    /**
     * 查询部门管理树
     *
     * @param area 部门信息
     * @return 所有部门信息
     */
    @Override
    //@DataScope(areaAlias = "d")
    public List<Ztree> selectAreaTree(Area area) {
        List<Area> areaList = areaMapper.selectAreaList(area);
        List<Ztree> ztrees = initZtree(areaList);
        return ztrees;
    }

    @Override
    //@DataScope(areaAlias = "d")
    public List<Ztree> selectAreaTreeWithCamNum(Area area) {
        List<Area> areaList = areaMapper.selectListWithCamNum(area);
        List<Ztree> ztrees = initZtree(areaList);
        return ztrees;
    }

    /**
     * 查询部门管理树（排除下级）
     *
     * @param
     * @return 所有部门信息
     */
    @Override
    //@DataScope(areaAlias = "d")
    public List<Ztree> selectAreaTreeExcludeChild(Area area) {
        Long areaId = area.getAreaId();
        List<Area> areaList = areaMapper.selectAreaList(area);
        Iterator<Area> it = areaList.iterator();
        while (it.hasNext()) {
            Area d = (Area) it.next();
            if (d.getAreaId().intValue() == areaId
                    || ArrayUtils.contains(StringUtils.split(d.getAncestors(), ","), areaId + "")) {
                it.remove();
            }
        }
        List<Ztree> ztrees = initZtree(areaList);
        return ztrees;
    }

    /**
     * 根据角色ID查询部门（数据权限）
     *
     * @param role 角色对象
     * @return 部门列表（数据权限）
     */
    @Override
    public List<Ztree> roleAreaTreeData(SysRole role) {
        Long roleId = role.getRoleId();
        List<Ztree> ztrees = new ArrayList<Ztree>();
        List<Area> areaList = selectAreaList(new Area());
        if (StringUtils.isNotNull(roleId)) {
            List<String> roleAreaList = areaMapper.selectRoleAreaTree(roleId);
            ztrees = initZtree(areaList, roleAreaList);
        } else {
            ztrees = initZtree(areaList);
        }
        return ztrees;
    }

    /**
     * 对象转部门树
     *
     * @param areaList 部门列表
     * @return 树结构列表
     */
    public List<Ztree> initZtree(List<Area> areaList) {
        return initZtree(areaList, null);
    }

    /**
     * 对象转部门树
     *
     * @param areaList     部门列表
     * @param roleAreaList 角色已存在菜单列表
     * @return 树结构列表
     */
    public List<Ztree> initZtree(List<Area> areaList, List<String> roleAreaList) {

        List<Ztree> ztrees = new ArrayList<>();
        boolean isCheck = StringUtils.isNotNull(roleAreaList);
        for (Area area : areaList) {
            if (UserConstants.DEPT_NORMAL.equals(area.getStatus())) {
                Ztree ztree = new Ztree();
                ztree.setId(area.getAreaId());
                ztree.setpId(area.getParentId());
                ztree.setName(area.getAreaName());
                ztree.setTitle(area.getAreaName());
                if (isCheck) {
                    ztree.setChecked(roleAreaList.contains(area.getAreaId() + area.getAreaName()));
                }
                ztrees.add(ztree);
            }
        }
        return ztrees;
    }

    /**
     * 查询部门人数
     *
     * @param parentId 部门ID
     * @return 结果
     */
    @Override
    public int selectAreaCount(Long parentId) {
        Area area = new Area();
        area.setParentId(parentId);
        return areaMapper.selectAreaCount(area);
    }

    /**
     * 查询部门是否存在用户
     *
     * @param areaId 部门ID
     * @return 结果 true 存在 false 不存在
     */
    @Override
    public boolean checkAreaExistTown(Long areaId) {
        int result = areaMapper.checkAreaExistTown(areaId);
        return result > 0 ? true : false;
    }

    /**
     * 删除部门管理信息
     *
     * @param areaId 部门ID
     * @return 结果
     */
    @Override
    public int deleteAreaById(Long areaId) {
        return areaMapper.deleteAreaById(areaId);
    }

    /**
     * 新增保存部门信息
     *
     * @param area 部门信息
     * @return 结果
     */
    @Override
    public int insertArea(Area area) {
        Area info = areaMapper.selectAreaById(area.getParentId());
        // 如果父节点不为"正常"状态,则不允许新增子节点
        if (!UserConstants.DEPT_NORMAL.equals(info.getStatus())) {
            throw new BusinessException("部门停用，不允许新增");
        }
        area.setAncestors(info.getAncestors() + "," + area.getParentId());
        setLevel(area);
        return areaMapper.insertArea(area);
    }

    /**
     * 修改保存部门信息
     *
     * @param area 部门信息
     * @return 结果
     */
    @Override
    @Transactional
    public int updateArea(Area area) {
        Area newParentArea = areaMapper.selectAreaById(area.getParentId());
        Area oldArea = selectAreaById(area.getAreaId());
        if (StringUtils.isNotNull(newParentArea) && StringUtils.isNotNull(oldArea)) {
            String newAncestors = newParentArea.getAncestors() + "," + newParentArea.getAreaId();
            String oldAncestors = oldArea.getAncestors();
            area.setAncestors(newAncestors);
            setLevel(area);
            updateAreaChildren(area.getAreaId(), newAncestors, oldAncestors);
        }
        int result = areaMapper.updateArea(area);
        if (UserConstants.DEPT_NORMAL.equals(area.getStatus())) {
            // 如果该部门是启用状态，则启用该部门的所有上级部门
            updateParentAreaStatus(area);
        }
        return result;
    }

    /**
     * 修改该部门的父级部门状态
     *
     * @param area 当前部门
     */
    private void updateParentAreaStatus(Area area) {
        String updateBy = area.getUpdateBy();
        area = areaMapper.selectAreaById(area.getAreaId());
        area.setUpdateBy(updateBy);
        areaMapper.updateAreaStatus(area);
    }

    /**
     * 修改子元素关系
     *
     * @param areaId       被修改的部门ID
     * @param newAncestors 新的父ID集合
     * @param oldAncestors 旧的父ID集合
     */
    public void updateAreaChildren(Long areaId, String newAncestors, String oldAncestors) {
        List<Area> children = areaMapper.selectChildrenAreaById(areaId);
        for (Area child : children) {
            child.setAncestors(child.getAncestors().replace(oldAncestors, newAncestors));
            setLevel(child);
            areaMapper.updateArea(child);
        }
//        if (children.size() > 0) {
//            areaMapper.updateAreaChildren(children);
//        }
    }

    //计算层级 1市 2区县 3街镇 4社区/园区/村
    private void setLevel(Area area) {
        String ancestors = Convert.toStr(area.getAncestors()).trim();
        area.setLevel(ancestors.split(",").length);
    }

    @Override
    public int setAllLevel() {
        List<Area> areaList = selectAllAreaList();
        int count = 0;
        for (Area area : areaList) {
            setLevel(area);
            count += areaMapper.updateArea(area);
        }
        return count;
    }

    /**
     * 根据部门ID查询信息
     *
     * @param areaId 部门ID
     * @return 部门信息
     */
    @Override
    public Area selectAreaById(Long areaId) {
        return areaMapper.selectAreaById(areaId);
    }

    /**
     * 根据ID查询所有子部门（正常状态）
     *
     * @param areaId 部门ID
     * @return 子部门数
     */
    @Override
    public int selectNormalChildrenAreaById(Long areaId) {
        return areaMapper.selectNormalChildrenAreaById(areaId);
    }

    /**
     * 校验部门名称是否唯一
     *
     * @param area 部门信息
     * @return 结果
     */
    @Override
    public String checkAreaNameUnique(Area area) {
        Long areaId = StringUtils.isNull(area.getAreaId()) ? -1L : area.getAreaId();
        Area info = areaMapper.checkAreaNameUnique(area.getAreaName(), area.getParentId());
        if (StringUtils.isNotNull(info) && info.getAreaId().longValue() != areaId.longValue()) {
            return UserConstants.DEPT_NAME_NOT_UNIQUE;
        }
        return UserConstants.DEPT_NAME_UNIQUE;
    }
}
