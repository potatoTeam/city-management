# city-management

#### 需求
城市管理项目  
用jeecg框架做  
PC页面：  
小区列表、新增/编辑页、详情页，左侧有地区树形菜单组件，和列表内容联动  、小区数据导入  
人员列表、新增/编辑页（显示相片和修改）、详情页  
通行记录列表、详情页（显示相片）  
登录页  

移动端页面：  
小区列表、详情页；地区用下拉菜单选择  
人员、通行记录列表、详情页  
登录页  

#### 软件架构
软件架构说明  

city-management-server 后台服务  
city-management-server-web 后台服务前端  
city-management-ui 用户移动端  


后台服务 https://github.com/jeecgboot/jeecg-boot v3.6.1  
后台服务前端  https://github.com/jeecgboot/jeecgboot-vue3 v3.6.0  


安装教程
https://help.jeecg.com/java/setup/idea/startup.html#%E4%BA%8C%E5%90%AF%E5%8A%A8vue3%E5%89%8D%E7%AB%AF--jeecgboot-vue3  
采用jeecg-system-start 单体启动项目开发 , 启动类：jeecg-module-system --> jeecg-system-start --> JeecgSystemApplication  

前端初始化 pnpm i
前端打包： pnpm install   pnpm run build


项目代码在jeecg-module-city模块下

代码生成：https://help.jeecg.com/vue3/codegen/online.html  

#### 安装教程


#### 使用说明
注意：
1. 更换天地图密钥



