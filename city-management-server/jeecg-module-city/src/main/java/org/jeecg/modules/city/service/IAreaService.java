package org.jeecg.modules.city.service;

import java.util.List;

import org.jeecg.common.exception.JeecgBootException;
import org.jeecg.common.system.vo.SelectTreeModel;
import org.jeecg.modules.city.entity.Area;
import org.jeecg.modules.city.model.dto.AreaDto;
import org.jeecg.modules.city.model.req.QueryRootListReq;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 行政区划
 * @Author: jeecg-boot
 * @Date:   2023-12-12
 * @Version: V1.0
 */
public interface IAreaService extends IService<Area> {

	/**根节点父ID的值*/
	public static final String ROOT_PID_VALUE = "0";
	
	/**树节点有子节点状态值*/
	public static final String HASCHILD = "1";
	
	/**树节点无子节点状态值*/
	public static final String NOCHILD = "0";

	/**
	 * 新增节点
	 *
	 * @param area
	 */
	void addArea(Area area);
	
	/**
   * 修改节点
   *
   * @param area
   * @throws JeecgBootException
   */
	void updateArea(Area area) throws JeecgBootException;
	
	/**
	 * 删除节点
	 *
	 * @param id
   * @throws JeecgBootException
	 */
	void deleteArea(String id) throws JeecgBootException;

	  /**
	   * 查询所有根数据，无分页
	   *
	   * @return List<Area>
	   */
    List<AreaDto> queryRootList();
    List<AreaDto> queryList(QueryRootListReq req);

	/**
	 * 【vue3专用】根据父级编码加载分类字典的数据
	 *
	 * @param parentCode
	 * @return
	 */
	List<SelectTreeModel> queryListByCode(Boolean async,String parentCode);

	/**
	 * 【vue3专用】根据pid查询子节点集合
	 *
	 * @param pid
	 * @return
	 */
	List<SelectTreeModel> queryListByPid(String pid);

}
