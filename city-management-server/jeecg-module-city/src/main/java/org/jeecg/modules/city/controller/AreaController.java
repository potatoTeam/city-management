package org.jeecg.modules.city.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.vo.SelectTreeModel;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.city.entity.Area;
import org.jeecg.modules.city.model.dto.AreaDto;
import org.jeecg.modules.city.model.req.QueryRootListReq;
import org.jeecg.modules.city.service.IAreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

 /**
 * @Description: 行政区划
 * @Author: jeecg-boot
 * @Date:   2023-12-12
 * @Version: V1.0
 */
@Api(tags="行政区划")
@RestController
@RequestMapping("/city/area")
@Slf4j
public class AreaController extends JeecgController<Area, IAreaService>{
	@Autowired
	private IAreaService areaService;

	/**
	 * 分页列表查询
	 *
	 * @param area
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	//@AutoLog(value = "行政区划-分页列表查询")
	@ApiOperation(value="行政区划-分页列表查询", notes="行政区划-分页列表查询")
	@GetMapping(value = "/rootList")
	public Result<IPage<AreaDto>> queryRootList(QueryRootListReq req) {
		
        if(StringUtils.isNotBlank(req.getAreaName()) || StringUtils.isNotBlank(req.getAreaNo())){//有查询参数
            List<AreaDto> list = areaService.queryList(req);
            IPage<AreaDto> pageList = new Page<>(1, 10, list.size());
            pageList.setRecords(list);
            return Result.OK(pageList);
        }else{//无参数查询
            List<AreaDto> list = areaService.queryRootList();
            IPage<AreaDto> pageList = new Page<>(1, 10, list.size());
            pageList.setRecords(list);
            return Result.OK(pageList);
        }
	}

	 /**
	  * 【vue3专用】加载节点的子数据
	  *
	  * @param pid
	  * @return
	  */
	 @RequestMapping(value = "/loadTreeChildren", method = RequestMethod.GET)
	 public Result<List<SelectTreeModel>> loadTreeChildren(@RequestParam(name = "pid") String pid) {
		 Result<List<SelectTreeModel>> result = new Result<>();
		 try {
			 List<SelectTreeModel> ls = areaService.queryListByPid(pid);
			 result.setResult(ls);
			 result.setSuccess(true);
		 } catch (Exception e) {
			 e.printStackTrace();
			 result.setMessage(e.getMessage());
			 result.setSuccess(false);
		 }
		 return result;
	 }

	 /**
	  * 【vue3专用】加载一级节点/如果是同步 则所有数据
	  *
	  * @param async
	  * @param pcode
	  * @return
	  */
	 @RequestMapping(value = "/loadTreeRoot", method = RequestMethod.GET)
	 public Result<List<SelectTreeModel>> loadTreeRoot(@RequestParam(name = "async") Boolean async, @RequestParam(name = "pcode",required = false) String pcode) {
		 Result<List<SelectTreeModel>> result = new Result<>();
		 try {
			 List<SelectTreeModel> ls = areaService.queryListByCode(async,pcode);
			 result.setResult(ls);
			 result.setSuccess(true);
		 } catch (Exception e) {
			 e.printStackTrace();
			 result.setMessage(e.getMessage());
			 result.setSuccess(false);
		 }
		 return result;
	 }


	 /**
      * 获取子数据
      * @param area
      * @param req
      * @return
      */
	//@AutoLog(value = "行政区划-获取子数据")
	@ApiOperation(value="行政区划-获取子数据", notes="行政区划-获取子数据")
	@GetMapping(value = "/childList")
	public Result<IPage<Area>> queryPageList(Area area,HttpServletRequest req) {
		QueryWrapper<Area> queryWrapper = QueryGenerator.initQueryWrapper(area, req.getParameterMap());
		queryWrapper.orderByAsc("order_num");
		List<Area> list = areaService.list(queryWrapper);
		IPage<Area> pageList = new Page<>(1, 10, list.size());
        pageList.setRecords(list);
		return Result.OK(pageList);
	}

    /**
      * 批量查询子节点
      * @param parentIds 父ID（多个采用半角逗号分割）
      * @return 返回 IPage
      * @param parentIds
      * @return
      */
	//@AutoLog(value = "行政区划-批量获取子数据")
    @ApiOperation(value="行政区划-批量获取子数据", notes="行政区划-批量获取子数据")
    @GetMapping("/getChildListBatch")
    public Result getChildListBatch(@RequestParam("parentIds") String parentIds) {
        try {
            QueryWrapper<Area> queryWrapper = new QueryWrapper<>();
            List<String> parentIdList = Arrays.asList(parentIds.split(","));
            queryWrapper.in("parent_id", parentIdList);
            List<Area> list = areaService.list(queryWrapper);
            IPage<Area> pageList = new Page<>(1, 10, list.size());
            pageList.setRecords(list);
            return Result.OK(pageList);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Result.error("批量查询子节点失败：" + e.getMessage());
        }
    }
	
	/**
	 *   添加
	 *
	 * @param area
	 * @return
	 */
	@AutoLog(value = "行政区划-添加")
	@ApiOperation(value="行政区划-添加", notes="行政区划-添加")
    @RequiresPermissions("city:b_area:add")
	@PostMapping(value = "/add")
	public Result<String> add(@RequestBody Area area) {
		areaService.addArea(area);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param area
	 * @return
	 */
	@AutoLog(value = "行政区划-编辑")
	@ApiOperation(value="行政区划-编辑", notes="行政区划-编辑")
    @RequiresPermissions("city:b_area:edit")
	@RequestMapping(value = "/edit", method = {RequestMethod.PUT,RequestMethod.POST})
	public Result<String> edit(@RequestBody Area area) {
		areaService.updateArea(area);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "行政区划-通过id删除")
	@ApiOperation(value="行政区划-通过id删除", notes="行政区划-通过id删除")
    @RequiresPermissions("city:b_area:delete")
	@DeleteMapping(value = "/delete")
	public Result<String> delete(@RequestParam(name="id",required=true) String id) {
		areaService.deleteArea(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "行政区划-批量删除")
	@ApiOperation(value="行政区划-批量删除", notes="行政区划-批量删除")
    @RequiresPermissions("city:b_area:deleteBatch")
	@DeleteMapping(value = "/deleteBatch")
	public Result<String> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.areaService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功！");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	//@AutoLog(value = "行政区划-通过id查询")
	@ApiOperation(value="行政区划-通过id查询", notes="行政区划-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<Area> queryById(@RequestParam(name="id",required=true) String id) {
		Area area = areaService.getById(id);
		if(area==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(area);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param area
    */
    @RequiresPermissions("city:b_area:exportXls")
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, Area area) {
		return super.exportXls(request, area, Area.class, "行政区划");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequiresPermissions("city:b_area:importExcel")
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
		return super.importExcel(request, response, Area.class);
    }

}
