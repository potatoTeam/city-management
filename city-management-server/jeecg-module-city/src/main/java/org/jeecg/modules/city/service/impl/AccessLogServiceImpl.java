package org.jeecg.modules.city.service.impl;

import org.jeecg.modules.city.entity.AccessLog;
import org.jeecg.modules.city.mapper.AccessLogMapper;
import org.jeecg.modules.city.service.IAccessLogService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 通行记录
 * @Author: jeecg-boot
 * @Date:   2023-12-16
 * @Version: V1.0
 */
@Service
public class AccessLogServiceImpl extends ServiceImpl<AccessLogMapper, AccessLog> implements IAccessLogService {

}
