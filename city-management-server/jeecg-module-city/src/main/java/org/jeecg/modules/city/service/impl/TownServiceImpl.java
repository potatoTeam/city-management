package org.jeecg.modules.city.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.jeecg.modules.city.entity.Town;
import org.jeecg.modules.city.mapper.TownMapper;
import org.jeecg.modules.city.service.ITownService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import cn.hutool.core.collection.CollUtil;

/**
 * @Description: 小区
 * @Author: jeecg-boot
 * @Date:   2023-12-13
 * @Version: V1.0
 */
@Service
public class TownServiceImpl extends ServiceImpl<TownMapper, Town> implements ITownService {

	@Override
	public void add(Town town) {
		String resFiles = town.getResFiles();
		if(StringUtils.isNotBlank(resFiles)) {
			String[] split = resFiles.split(",");
			List<String> list = new ArrayList<>();
			for (String str : split) {
				list.add(str.substring(str.lastIndexOf("/")+1));
			}
			town.setResNames(CollUtil.join(list, ","));
		}else {
			town.setResFiles("");
			town.setResNames("");
		}
		
		this.save(town);
	}

	@Override
	public void edit(Town town) {
		String resFiles = town.getResFiles();
		String resNames = town.getResNames();
		if(StringUtils.isBlank(resFiles)) {
			town.setResFiles("");
			town.setResNames("");
		}else {
			
			
			String[] resFilesSplit = resFiles.split(",");
			String[] resNamesSplit = null;
			if(StringUtils.isNotBlank(resNames)) {
				resNamesSplit = resNames.split(",");
			}
			
			List<String> list = new ArrayList<>();
			
			for (int i = 0; i < resFilesSplit.length; i++) {
				String str = resFilesSplit[i];
				if(resNamesSplit != null && i <= resNamesSplit.length - 1) {
					list.add(resNamesSplit[i]);
				}else {
					list.add(str.substring(str.lastIndexOf("/")+1));
				}
			}
			
			if(list.size() > 0) {
				town.setResNames(CollUtil.join(list, ","));
			}else {
				town.setResFiles("");
				town.setResNames("");
			}
		}
		
		
		this.updateById(town);
	}

}
