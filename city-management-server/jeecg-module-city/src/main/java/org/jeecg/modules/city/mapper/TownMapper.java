package org.jeecg.modules.city.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.city.entity.Town;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 小区
 * @Author: jeecg-boot
 * @Date:   2023-12-13
 * @Version: V1.0
 */
public interface TownMapper extends BaseMapper<Town> {

}
