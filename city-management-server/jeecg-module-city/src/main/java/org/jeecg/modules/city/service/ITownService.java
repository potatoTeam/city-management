package org.jeecg.modules.city.service;

import org.jeecg.modules.city.entity.Town;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 小区
 * @Author: jeecg-boot
 * @Date:   2023-12-13
 * @Version: V1.0
 */
public interface ITownService extends IService<Town> {

	void add(Town town);

	void edit(Town town);

}
