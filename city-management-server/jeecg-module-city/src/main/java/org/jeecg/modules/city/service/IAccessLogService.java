package org.jeecg.modules.city.service;

import org.jeecg.modules.city.entity.AccessLog;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 通行记录
 * @Author: jeecg-boot
 * @Date:   2023-12-16
 * @Version: V1.0
 */
public interface IAccessLogService extends IService<AccessLog> {

}
