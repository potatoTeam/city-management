package org.jeecg.modules.city.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.city.entity.Town;
import org.jeecg.modules.city.service.ITownService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.apache.shiro.authz.annotation.RequiresPermissions;

 /**
 * @Description: 小区
 * @Author: jeecg-boot
 * @Date:   2023-12-13
 * @Version: V1.0
 */
@Api(tags="小区")
@RestController
@RequestMapping("/city/town")
@Slf4j
public class TownController extends JeecgController<Town, ITownService> {
	@Autowired
	private ITownService townService;
	
	/**
	 * 分页列表查询
	 *
	 * @param town
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	//@AutoLog(value = "小区-分页列表查询")
	@ApiOperation(value="小区-分页列表查询", notes="小区-分页列表查询")
	@GetMapping(value = "/list")
	public Result<IPage<Town>> queryPageList(Town town,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<Town> queryWrapper = QueryGenerator.initQueryWrapper(town, req.getParameterMap());
		Page<Town> page = new Page<Town>(pageNo, pageSize);
		IPage<Town> pageList = townService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param town
	 * @return
	 */
	@AutoLog(value = "小区-添加")
	@ApiOperation(value="小区-添加", notes="小区-添加")
	@RequiresPermissions("city:b_town:add")
	@PostMapping(value = "/add")
	public Result<String> add(@RequestBody Town town) {
		townService.add(town);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param town
	 * @return
	 */
	@AutoLog(value = "小区-编辑")
	@ApiOperation(value="小区-编辑", notes="小区-编辑")
	@RequiresPermissions("city:b_town:edit")
	@RequestMapping(value = "/edit", method = {RequestMethod.PUT,RequestMethod.POST})
	public Result<String> edit(@RequestBody Town town) {
		townService.edit(town);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "小区-通过id删除")
	@ApiOperation(value="小区-通过id删除", notes="小区-通过id删除")
	@RequiresPermissions("city:b_town:delete")
	@DeleteMapping(value = "/delete")
	public Result<String> delete(@RequestParam(name="id",required=true) String id) {
		townService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "小区-批量删除")
	@ApiOperation(value="小区-批量删除", notes="小区-批量删除")
	@RequiresPermissions("city:b_town:deleteBatch")
	@DeleteMapping(value = "/deleteBatch")
	public Result<String> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.townService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	//@AutoLog(value = "小区-通过id查询")
	@ApiOperation(value="小区-通过id查询", notes="小区-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<Town> queryById(@RequestParam(name="id",required=true) String id) {
		Town town = townService.getById(id);
		if(town==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(town);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param town
    */
    @RequiresPermissions("city:b_town:exportXls")
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, Town town) {
        return super.exportXls(request, town, Town.class, "小区");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequiresPermissions("city:b_town:importExcel")
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, Town.class);
    }

}
