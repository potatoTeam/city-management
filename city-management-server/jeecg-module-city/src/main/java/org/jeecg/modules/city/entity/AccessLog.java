package org.jeecg.modules.city.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.Data;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 通行记录
 * @Author: jeecg-boot
 * @Date:   2023-12-16
 * @Version: V1.0
 */
@Data
@TableName("b_access_log")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="b_access_log对象", description="通行记录")
public class AccessLog implements Serializable {
    private static final long serialVersionUID = 1L;

	/**记录ID*/
	@TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "记录ID")
    private java.lang.Integer id;
	/**小区编号*/
	@Excel(name = "小区编号", width = 15)
    @ApiModelProperty(value = "小区编号")
    private java.lang.String townNo;
	/**小区名称*/
	@Excel(name = "小区名称", width = 15)
    @ApiModelProperty(value = "小区名称")
    private java.lang.String townName;
	/**门禁编号*/
	@Excel(name = "门禁编号", width = 15)
    @ApiModelProperty(value = "门禁编号")
    private java.lang.String entNo;
	/**门禁名称*/
	@Excel(name = "门禁名称", width = 15)
    @ApiModelProperty(value = "门禁名称")
    private java.lang.String entName;
	/**门禁平台*/
	@Excel(name = "门禁平台", width = 15, dicCode = "ent_platform")
	@Dict(dicCode = "ent_platform")
    @ApiModelProperty(value = "门禁平台")
	@JsonFormat(shape = JsonFormat.Shape.STRING)
    private java.lang.Integer entPlatform;
	/**人员ID*/
	@Excel(name = "人员ID", width = 15)
    @ApiModelProperty(value = "人员ID")
    private java.lang.Integer personId;
	/**人员姓名*/
	@Excel(name = "人员姓名", width = 15)
    @ApiModelProperty(value = "人员姓名")
    private java.lang.String personName;
	/**证件号码*/
	@Excel(name = "证件号码", width = 15)
    @ApiModelProperty(value = "证件号码")
    private java.lang.String idNumber;
	/**居民ID*/
	@Excel(name = "居民ID", width = 15)
    @ApiModelProperty(value = "居民ID")
    private java.lang.Integer residentId;
	/**拜访记录ID*/
	@Excel(name = "拜访记录ID", width = 15)
    @ApiModelProperty(value = "拜访记录ID")
    private java.lang.Integer visitId;
	/**人员类型*/
	@Excel(name = "人员类型", width = 15, dicCode = "access_person_type")
	@Dict(dicCode = "access_person_type")
    @ApiModelProperty(value = "人员类型")
    private java.lang.String personType;
	/**人员相片*/
	@Excel(name = "人员相片", width = 15)
    @ApiModelProperty(value = "人员相片")
    private java.lang.String personPhoto;
	/**通行时间*/
	@Excel(name = "通行时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "通行时间")
    private java.util.Date accessTime;
	/**通行类型*/
	@Excel(name = "通行类型", width = 15, dicCode = "access_type")
	@Dict(dicCode = "access_type")
    @ApiModelProperty(value = "通行类型")
    private java.lang.String accessType;
	/**比对方式*/
	@Excel(name = "比对方式", width = 15, dicCode = "cmp_type")
	@Dict(dicCode = "cmp_type")
    @ApiModelProperty(value = "比对方式")
    private java.lang.String cmpType;
	/**比对分值*/
	@Excel(name = "比对分值", width = 15)
    @ApiModelProperty(value = "比对分值")
    private java.lang.Integer cmpScore;
	/**比对结果*/
	@Excel(name = "比对结果", width = 15, dicCode = "cmp_result")
	@Dict(dicCode = "cmp_result")
    @ApiModelProperty(value = "比对结果")
    private java.lang.String cmpResult;
	/**体温*/
	@Excel(name = "体温", width = 15)
    @ApiModelProperty(value = "体温")
    private java.math.BigDecimal temperature;
	/**背景图*/
	@Excel(name = "背景图", width = 15)
    @ApiModelProperty(value = "背景图")
    private java.lang.String backImg;
	/**通行相片*/
	@Excel(name = "通行相片", width = 15)
    @ApiModelProperty(value = "通行相片")
    private java.lang.String captureImg;
	/**tc上报状态*/
	@Excel(name = "tc上报状态", width = 15, dicCode = "report_status")
	@Dict(dicCode = "report_status")
    @ApiModelProperty(value = "tc上报状态")
    private java.lang.String statusPost;
	/**zt上报状态*/
	@Excel(name = "zt上报状态", width = 15, dicCode = "report_status")
	@Dict(dicCode = "report_status")
    @ApiModelProperty(value = "zt上报状态")
    private java.lang.String statusZt;
	/**状态(0无效1有效)*/
	@Excel(name = "状态(0无效1有效)", width = 15, dicCode = "common_status")
	@Dict(dicCode = "common_status")
    @ApiModelProperty(value = "状态(0无效1有效)")
    private java.lang.String status;
	/**删除标志(0存在1删除)*/
	@Excel(name = "删除标志(0存在1删除)", width = 15)
    @ApiModelProperty(value = "删除标志(0存在1删除)")
    @TableLogic
    private java.lang.String delFlag;
	/**创建者*/
    @ApiModelProperty(value = "创建者")
    private java.lang.String createBy;
	/**创建时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建时间")
    private java.util.Date createTime;
	/**更新者*/
    @ApiModelProperty(value = "更新者")
    private java.lang.String updateBy;
	/**更新时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新时间")
    private java.util.Date updateTime;
	/**备注*/
	@Excel(name = "备注", width = 15)
    @ApiModelProperty(value = "备注")
    private java.lang.String remark;
}
