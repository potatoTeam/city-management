package org.jeecg.modules.city.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jeecg.common.exception.JeecgBootException;
import org.jeecg.common.system.vo.SelectTreeModel;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.city.entity.Area;
import org.jeecg.modules.city.mapper.AreaMapper;
import org.jeecg.modules.city.model.dto.AreaDto;
import org.jeecg.modules.city.model.req.QueryRootListReq;
import org.jeecg.modules.city.service.IAreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;

/**
 * @Description: 行政区划
 * @Author: jeecg-boot
 * @Date:   2023-12-12
 * @Version: V1.0
 */
@Service
public class AreaServiceImpl extends ServiceImpl<AreaMapper, Area> implements IAreaService {

	@Autowired
	private AreaMapper areaMapper;
	
	@Override
	public void addArea(Area area) {
	   //新增时设置hasChild为0
	    area.setHasChild(IAreaService.NOCHILD);
		if(oConvertUtils.isEmpty(area.getParentId())){
			area.setParentId(IAreaService.ROOT_PID_VALUE);
			area.setAncestors("0");
			area.setLevel(1);
		}else{
			//如果当前节点父ID不为空 则设置父节点的hasChildren 为1
			Area parent = baseMapper.selectById(area.getParentId());
			if(parent!=null && !"1".equals(parent.getHasChild())){
				parent.setHasChild("1");
				baseMapper.updateById(parent);
			}
			
			List<Long> pids = getTreeParentIds(null, Long.valueOf(area.getParentId()));
			area.setAncestors(CollUtil.join(pids, ","));
			area.setLevel(pids.size());
		}
		area.setStatus("0");
		area.setDelFlag("0");
		baseMapper.insert(area);
	}
	
	@Override
	public void updateArea(Area area) {
		Area entity = this.getById(area.getId());
		if(entity==null) {
			throw new JeecgBootException("未找到对应实体");
		}
		String old_pid = entity.getParentId();
		String new_pid = area.getParentId();
		if(!old_pid.equals(new_pid)) {
			updateOldParentNode(old_pid);
			if(oConvertUtils.isEmpty(new_pid)){
				area.setParentId(IAreaService.ROOT_PID_VALUE);
			}
			if(!IAreaService.ROOT_PID_VALUE.equals(area.getParentId())) {
				baseMapper.updateTreeNodeStatus(area.getParentId(), IAreaService.HASCHILD);
			}
			
			List<Long> pids = getTreeParentIds(null, Long.valueOf(new_pid));
			area.setAncestors(CollUtil.join(pids, ","));
			area.setLevel(pids.size());
		}
		baseMapper.updateById(area);
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void deleteArea(String id) throws JeecgBootException {
		//查询选中节点下所有子节点一并删除
        id = this.queryTreeChildIds(id);
        if(id.indexOf(",")>0) {
            StringBuffer sb = new StringBuffer();
            String[] idArr = id.split(",");
            for (String idVal : idArr) {
                if(idVal != null){
                    Area area = this.getById(idVal);
                    String pidVal = area.getParentId();
                    //查询此节点上一级是否还有其他子节点
                    List<Area> dataList = baseMapper.selectList(new QueryWrapper<Area>().eq("parent_id", pidVal).eq("del_flag", "0").notIn("id",Arrays.asList(idArr)));
                    boolean flag = (dataList == null || dataList.size() == 0) && !Arrays.asList(idArr).contains(pidVal) && !sb.toString().contains(pidVal);
                    if(flag){
                        //如果当前节点原本有子节点 现在木有了，更新状态
                        sb.append(pidVal).append(",");
                    }
                }
            }
            //批量删除节点
            baseMapper.deleteBatchIds(Arrays.asList(idArr));
            //修改已无子节点的标识
            String[] pidArr = sb.toString().split(",");
            for(String pid : pidArr){
                this.updateOldParentNode(pid);
            }
        }else{
            Area area = this.getById(id);
            if(area==null) {
                throw new JeecgBootException("未找到对应实体");
            }
            updateOldParentNode(area.getParentId());
            baseMapper.deleteById(id);
        }
	}
	
	@Override
	public List<AreaDto> queryList(QueryRootListReq req) {
		List<AreaDto> dataList = areaMapper.getAreaList(req);
		return dataList;
	}
	@Override
    public List<AreaDto> queryRootList() {
		QueryWrapper<Area> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("del_flag", "0");
		queryWrapper.eq("parent_id", "0");
		queryWrapper.orderByAsc("order_num");
        List<Area> dataList = baseMapper.selectList(queryWrapper);
        List<AreaDto> areas = new ArrayList<>();
		for (Area area : dataList) {
			AreaDto dto = new AreaDto();
			BeanUtil.copyProperties(area, dto, true);
			areas.add(dto);
		}
		return areas;
    }

	private List<AreaDto> buildDataTree(List<AreaDto> dataList) {
        Map<Integer, AreaDto> areasMap = new HashMap<Integer, AreaDto>();
		for (AreaDto dto : dataList) {
			areasMap.put(dto.getId(), dto);
		}

        List<AreaDto> tree = new ArrayList<>();
        for (AreaDto area : dataList) {
            if (area.getParentId().equals(ROOT_PID_VALUE)) {
                buildTree(area, areasMap);
                tree.add(area);
            }
        }
        
        return tree;
	}
    private void buildTree(AreaDto area, Map<Integer, AreaDto> areasMap) {
        for (AreaDto childArea : areasMap.values()) {
            if (childArea.getParentId() != null && childArea.getParentId().equals(area.getId().toString())) {
            	List<AreaDto> children = area.getChildren();
            	if(children == null) {
            		children = new ArrayList<>();
            		area.setChildren(children);
            	}
                children.add(childArea);
                buildTree(childArea, areasMap);
            }
        }
    }
        
        
        
       

    @Override
    public List<SelectTreeModel> queryListByCode(Boolean async,String parentCode) {
        String pid = ROOT_PID_VALUE;
        if (oConvertUtils.isNotEmpty(parentCode)) {
            LambdaQueryWrapper<Area> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.eq(Area::getParentId, parentCode);
            queryWrapper.eq(Area::getDelFlag, "0");
            List<Area> list = baseMapper.selectList(queryWrapper);
            if (list == null || list.size() == 0) {
                throw new JeecgBootException("该编码【" + parentCode + "】不存在，请核实!");
            }
            if (list.size() > 1) {
                throw new JeecgBootException("该编码【" + parentCode + "】存在多个，请核实!");
            }
            pid = list.get(0).getId().toString();
        }
        List<SelectTreeModel> ls = baseMapper.queryListByPid(pid, null);
        if (!async) {
        	loadAllChildren(ls);
		 }
        
        return ls;
    }
    
    /**
	  * 【vue3专用】递归求子节点 同步加载用到
	  *
	  * @param ls
	  */
	 private void loadAllChildren(List<SelectTreeModel> ls) {
		 QueryWrapper<Area> queryWrapper = new QueryWrapper<>();
		 queryWrapper.eq("del_flag", "0");
		 queryWrapper.orderByAsc("order_num");
         List<Area> dataList = baseMapper.selectList(queryWrapper);
         
         
         Map<Integer, SelectTreeModel> areasMap = new HashMap<>();
 		 for (Area dto : dataList) {
 			SelectTreeModel model = new SelectTreeModel();
 			boolean isLeaf = false;
 			if(dto.getHasChild() == null) {
 				isLeaf = false;
 			}else {
 				if(dto.getHasChild().equals("1")) {
 					isLeaf = false;
 				}else {
 					isLeaf = true;
 				}
 			}
 			
 			model.setKey(dto.getId().toString());
 			model.setTitle(dto.getAreaName());
 			model.setParentId(dto.getParentId());
 			model.setLeaf(isLeaf);
 			areasMap.put(dto.getId(), model);
 		}

		 for (SelectTreeModel tsm : ls) {
			 buildTree(tsm, areasMap);
		 }
	 }
   private void buildTree(SelectTreeModel area, Map<Integer, SelectTreeModel> areasMap) {
       for (SelectTreeModel childArea : areasMap.values()) {
           if (childArea.getParentId() != null && childArea.getParentId().equals(area.getKey())) {
           	List<SelectTreeModel> children = area.getChildren();
           	if(children == null) {
           		children = new ArrayList<>();
           		area.setChildren(children);
           	}
               children.add(childArea);
               buildTree(childArea, areasMap);
           }
       }
   }

    @Override
    public List<SelectTreeModel> queryListByPid(String pid) {
        if (oConvertUtils.isEmpty(pid)) {
            pid = ROOT_PID_VALUE;
        }
        return baseMapper.queryListByPid(pid, null);
    }

	/**
	 * 根据所传pid查询旧的父级节点的子节点并修改相应状态值
	 * @param pid
	 */
	private void updateOldParentNode(String pid) {
		if(!IAreaService.ROOT_PID_VALUE.equals(pid)) {
			Long count = baseMapper.selectCount(new QueryWrapper<Area>().eq("parent_id", pid).eq("del_flag", "0"));
			if(count==null || count<=1) {
				baseMapper.updateTreeNodeStatus(pid, IAreaService.NOCHILD);
			}
		}
	}

	/**
     * 递归查询节点的根节点
     * @param pidVal
     * @return
     */
    private Area getTreeRoot(String pidVal){
        Area data =  baseMapper.selectById(pidVal);
        if(data != null && !IAreaService.ROOT_PID_VALUE.equals(data.getParentId())){
            return this.getTreeRoot(data.getParentId());
        }else{
            return data;
        }
    }

    /**
     * 根据id查询所有子节点id
     * @param ids
     * @return
     */
    private String queryTreeChildIds(String ids) {
        //获取id数组
        String[] idArr = ids.split(",");
        StringBuffer sb = new StringBuffer();
        for (String pidVal : idArr) {
            if(pidVal != null){
                if(!sb.toString().contains(pidVal)){
                    if(sb.toString().length() > 0){
                        sb.append(",");
                    }
                    sb.append(pidVal);
                    this.getTreeChildIds(pidVal,sb);
                }
            }
        }
        return sb.toString();
    }

    /**
     * 递归查询所有子节点
     * @param pidVal
     * @param sb
     * @return
     */
    private StringBuffer getTreeChildIds(String pidVal,StringBuffer sb){
        List<Area> dataList = baseMapper.selectList(new QueryWrapper<Area>().eq("parent_id", pidVal).eq("del_flag", "0"));
        if(dataList != null && dataList.size()>0){
            for(Area tree : dataList) {
                if(!sb.toString().contains(tree.getId().toString())){
                    sb.append(",").append(tree.getId());
                }
                this.getTreeChildIds(tree.getId().toString(),sb);
            }
        }
        return sb;
    }
    
    /**
     * 获取当前区域所有的父级
     * @param pids
     * @param pidVal
     * @return 顺序 从根id 到 当前区域的父级id 
     */
    private List<Long> getTreeParentIds(List<Long> pids,Long pidVal){
    	if(pids == null) {
    		pids = new ArrayList<>();
    	}
        List<Area> dataList = baseMapper.selectList(new QueryWrapper<Area>().eq("id", pidVal).eq("del_flag", "0"));
        if(dataList != null && dataList.size()>0){
        	Area area = dataList.get(0);
        	if(area.getParentId() != null) {
        		
        		Long parseLong = Long.valueOf(area.getParentId());
    			pids.add(parseLong);
    			
    			if(parseLong != 0L) {
    				pids = this.getTreeParentIds(pids, parseLong);
    				return pids;
        		}
        	}
        }
        CollUtil.reverse(pids);//反转一下
        return pids;
    }
}
