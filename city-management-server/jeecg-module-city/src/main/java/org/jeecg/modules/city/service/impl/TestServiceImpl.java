package org.jeecg.modules.city.service.impl;

import java.util.List;
import java.util.Map;

import org.jeecg.common.system.api.ISysBaseAPI;
import org.jeecg.modules.city.mapper.TestMapper;
import org.jeecg.modules.city.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TestServiceImpl implements TestService {

	
	@Autowired
	private ISysBaseAPI sysBaseAPI;
	@Autowired
	private TestMapper testMapper;
	
	
	
	@Override
	public void initArea() {
		List<Map<String, Object>> areaList = testMapper.getAreaList();
		
		for (Map<String, Object> map : areaList) {
			Long id = (Long)map.get("id");
			
			String hasChild = hasChild(id, areaList);
			testMapper.updateArea(id, hasChild);
		}
	}
	
	private String hasChild(Long id,List<Map<String, Object>> areaList) {
		String hasChild = "0";
		for (Map<String, Object> map : areaList) {
			String parent_id = (String)map.get("parent_id");
			if(parent_id != null && parent_id.equals(id.toString())) {
				hasChild = "1";
				break;
			}
		}
		return hasChild;
		
	}
	
	
}
