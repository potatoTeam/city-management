package org.jeecg.modules.city.entity;

import java.io.Serializable;
import java.util.List;

import org.jeecg.common.aspect.annotation.Dict;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description: 行政区划
 * @Author: jeecg-boot
 * @Date:   2023-12-12
 * @Version: V1.0
 */
@Data
@TableName("b_area")
@ApiModel(value="b_area对象", description="行政区划")
public class Area implements Serializable {
    private static final long serialVersionUID = 1L;

	/**区划id*/
	@TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "区划id")
    private java.lang.Integer id;
	/**祖级列表*/
	@Excel(name = "祖级列表", width = 15)
    @ApiModelProperty(value = "祖级列表")
    private java.lang.String ancestors;
	/**层级*/
	@Excel(name = "层级", width = 15)
    @ApiModelProperty(value = "层级")
    private java.lang.Integer level;
	/**区划类型*/
	@Excel(name = "区划类型", width = 15, dicCode = "area_type")
	@Dict(dicCode = "area_type")
    @ApiModelProperty(value = "区划类型")
    private java.lang.String areaType;
	/**区划名称*/
	@Excel(name = "区划名称", width = 15)
    @ApiModelProperty(value = "区划名称")
    private java.lang.String areaName;
	/**编码*/
	@Excel(name = "编码", width = 15)
    @ApiModelProperty(value = "编码")
    private java.lang.String areaNo;
	/**显示顺序*/
	@Excel(name = "显示顺序", width = 15)
    @ApiModelProperty(value = "显示顺序")
    private java.lang.Integer orderNum;
	/**状态*/
	@Excel(name = "状态", width = 15, dicCode = "common_status")
	@Dict(dicCode = "common_status")
    @ApiModelProperty(value = "状态")
    private java.lang.String status;
	/**删除标志*/
    @ApiModelProperty(value = "删除标志")
    @TableLogic
    private java.lang.String delFlag;
	/**创建者*/
    @ApiModelProperty(value = "创建者")
    private java.lang.String createBy;
	/**创建时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建时间")
    private java.util.Date createTime;
	/**更新者*/
    @ApiModelProperty(value = "更新者")
    private java.lang.String updateBy;
	/**更新时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新时间")
    private java.util.Date updateTime;
	/**父级节点*/
	@Excel(name = "父级节点", width = 15, dictTable = "b_area", dicText = "area_name", dicCode = "id")
	@Dict(dictTable = "b_area", dicText = "area_name", dicCode = "id")
    @ApiModelProperty(value = "父级节点")
    private java.lang.String parentId;
	/**是否有子节点*/
	@Excel(name = "是否有子节点", width = 15, dicCode = "yn")
	@Dict(dicCode = "yn")
    @ApiModelProperty(value = "是否有子节点")
    private java.lang.String hasChild;
	
	
}
