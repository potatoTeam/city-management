package org.jeecg.modules.city.model.req;

import lombok.Data;

@Data
public class QueryRootListReq {
	
	
	private String areaName ;
	private String areaNo;

}
