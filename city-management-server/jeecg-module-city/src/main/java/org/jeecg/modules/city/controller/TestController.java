package org.jeecg.modules.city.controller;

import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.city.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;


@Slf4j
@RestController
@RequestMapping("/test")
public class TestController {

	@Autowired
	private TestService testService;
    
    @PostMapping("/initArea")
    public Result<?> initArea() {
    	testService.initArea();
        return Result.OK();
    }

 

}
