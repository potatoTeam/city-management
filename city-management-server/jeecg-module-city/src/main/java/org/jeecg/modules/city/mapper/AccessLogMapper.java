package org.jeecg.modules.city.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.city.entity.AccessLog;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 通行记录
 * @Author: jeecg-boot
 * @Date:   2023-12-16
 * @Version: V1.0
 */
public interface AccessLogMapper extends BaseMapper<AccessLog> {

}
