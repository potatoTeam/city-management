package org.jeecg.modules.city.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 人员
 * @Author: jeecg-boot
 * @Date:   2023-12-16
 * @Version: V1.0
 */
@Data
@TableName("b_person")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="b_person对象", description="人员")
public class Person implements Serializable {
    private static final long serialVersionUID = 1L;

	/**记录ID*/
	@TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "记录ID")
    private java.lang.Integer id;
	/**注册地小区*/
	@Excel(name = "小区编号", width = 15)
    @ApiModelProperty(value = "小区编号")
    private java.lang.String townNo;
	/**小区名称*/
	@Excel(name = "小区名称", width = 15)
    @ApiModelProperty(value = "小区名称")
    private java.lang.String townName;
	/**姓名*/
	@Excel(name = "姓名", width = 15)
    @ApiModelProperty(value = "姓名")
    private java.lang.String name;
	/**性别*/
	@Excel(name = "性别", width = 15, dicCode = "info_sex")
	@Dict(dicCode = "info_sex")
    @ApiModelProperty(value = "性别")
    private java.lang.String sex;
	/**政治面貌*/
	@Excel(name = "政治面貌", width = 15, dicCode = "info_zzmm")
	@Dict(dicCode = "info_zzmm")
    @ApiModelProperty(value = "政治面貌")
    private java.lang.String zzmm;
	/**证件类型*/
	@Excel(name = "证件类型", width = 15, dicCode = "info_card_type")
	@Dict(dicCode = "info_card_type")
    @ApiModelProperty(value = "证件类型")
    private java.lang.String cardType;
	/**证件号码*/
	@Excel(name = "证件号码", width = 15)
    @ApiModelProperty(value = "证件号码")
    private java.lang.String idNumber;
	/**出生日期*/
	@Excel(name = "出生日期", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "出生日期")
    private java.util.Date bornDate;
	/**身份证地址*/
	@Excel(name = "身份证地址", width = 15)
    @ApiModelProperty(value = "身份证地址")
    private java.lang.String idCardAddr;
	/**职业*/
	@Excel(name = "职业", width = 15)
    @ApiModelProperty(value = "职业")
    private java.lang.String job;
	/**zoneId*/
	@Excel(name = "zoneId", width = 15)
    @ApiModelProperty(value = "zoneId")
    private java.lang.Integer zoneId;
	/**ptype*/
	@Excel(name = "ptype", width = 15)
    @ApiModelProperty(value = "ptype")
    private java.lang.Integer ptype;
	/**楼栋*/
	@Excel(name = "楼栋", width = 15)
    @ApiModelProperty(value = "楼栋")
    private java.lang.String buildName;
	/**房屋*/
	@Excel(name = "房屋", width = 15)
    @ApiModelProperty(value = "房屋")
    private java.lang.String houseName;
	/**人员类型*/
	@Excel(name = "人员类型", width = 15, dicCode = "person_type")
	@Dict(dicCode = "person_type")
    @ApiModelProperty(value = "人员类型")
    private java.lang.String personType;
	/**是否房主*/
	@Excel(name = "是否房主", width = 15, dicCode = "yes_no_status")
	@Dict(dicCode = "yes_no_status")
    @ApiModelProperty(value = "是否房主")
    private java.lang.String isOwner;
	/**是否负责人*/
	@Excel(name = "是否负责人", width = 15, dicCode = "yes_no_status")
	@Dict(dicCode = "yes_no_status")
    @ApiModelProperty(value = "是否负责人")
    private java.lang.String isLead;
	/**联系电话*/
	@Excel(name = "联系电话", width = 15)
    @ApiModelProperty(value = "联系电话")
    private java.lang.String phone;
	/**照片*/
	@Excel(name = "照片", width = 15)
    @ApiModelProperty(value = "照片")
    private java.lang.String photo;
	/**新大陆CTID认证bid*/
	@Excel(name = "新大陆CTID认证bid", width = 15)
    @ApiModelProperty(value = "新大陆CTID认证bid")
    private java.lang.String bid;
	/**CTID认证*/
	@Excel(name = "CTID认证", width = 15, dicCode = "ctid_flag")
	@Dict(dicCode = "ctid_flag")
    @ApiModelProperty(value = "CTID认证")
    private java.lang.String ctidFlag;
	/**车牌号码*/
	@Excel(name = "车牌号码", width = 15)
    @ApiModelProperty(value = "车牌号码")
    private java.lang.String carNumber;
	/**是否特殊人员*/
	@Excel(name = "是否特殊人员", width = 15, dicCode = "yes_no_status")
	@Dict(dicCode = "yes_no_status")
    @ApiModelProperty(value = "是否特殊人员")
    private java.lang.String isSpecial;
	/**特殊情况说明*/
	@Excel(name = "特殊情况说明", width = 15)
    @ApiModelProperty(value = "特殊情况说明")
    private java.lang.String specialInfo;
	/**关注类型*/
	@Excel(name = "关注类型", width = 15, dicCode = "info_stare_type")
	@Dict(dicCode = "info_stare_type")
    @ApiModelProperty(value = "关注类型")
    private java.lang.String stare;
	/**关怀类型*/
	@Excel(name = "关怀类型", width = 15, dicCode = "info_care_type")
	@Dict(dicCode = "info_care_type")
    @ApiModelProperty(value = "关怀类型")
    private java.lang.String care;
	/**关怀原因*/
	@Excel(name = "关怀原因", width = 15)
    @ApiModelProperty(value = "关怀原因")
    private java.lang.String careInfo;
	/**登记方式*/
	@Excel(name = "登记方式", width = 15, dicCode = "sign_way")
	@Dict(dicCode = "sign_way")
    @ApiModelProperty(value = "登记方式")
    private java.lang.String signWay;
	/**状态*/
	@Excel(name = "状态", width = 15, dicCode = "common_status")
	@Dict(dicCode = "common_status")
    @ApiModelProperty(value = "状态")
    private java.lang.String status;
	/**删除标志*/
    @ApiModelProperty(value = "删除标志")
    @TableLogic
    private java.lang.String delFlag;
	/**创建者*/
    @ApiModelProperty(value = "创建者")
    private java.lang.String createBy;
	/**创建时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建时间")
    private java.util.Date createTime;
	/**更新者*/
    @ApiModelProperty(value = "更新者")
    private java.lang.String updateBy;
	/**更新时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新时间")
    private java.util.Date updateTime;
	/**备注*/
	@Excel(name = "备注", width = 15)
    @ApiModelProperty(value = "备注")
    private java.lang.String remark;
}
