package org.jeecg.modules.city.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.city.entity.Person;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 人员
 * @Author: jeecg-boot
 * @Date:   2023-12-16
 * @Version: V1.0
 */
public interface PersonMapper extends BaseMapper<Person> {

}
