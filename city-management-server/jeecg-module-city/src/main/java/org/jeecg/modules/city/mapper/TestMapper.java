package org.jeecg.modules.city.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface TestMapper {

	
	List<Map<String,Object>> getAreaList();
	
	void updateArea(@Param("id") Long id,@Param("hasChild") String hasChild);
	
}
