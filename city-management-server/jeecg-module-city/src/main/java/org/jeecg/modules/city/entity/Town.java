package org.jeecg.modules.city.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 小区
 * @Author: jeecg-boot
 * @Date:   2023-12-13
 * @Version: V1.0
 */
@Data
@TableName("b_town")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="b_town对象", description="小区")
public class Town implements Serializable {
    private static final long serialVersionUID = 1L;

	/**记录ID*/
	@TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "记录ID")
    private java.lang.Integer id;
	/**区划ID路径*/
	@Excel(name = "区划ID路径", width = 15)
    @ApiModelProperty(value = "区划ID路径")
    private java.lang.String areaIds;
	/**区划名称路径*/
	@Excel(name = "区划名称路径", width = 15)
    @ApiModelProperty(value = "区划名称路径")
    private java.lang.String areaNames;
	/**所属区划*/
	@Excel(name = "所属区划", width = 15, dictTable = "b_area", dicText = "area_name", dicCode = "id")
	@Dict(dictTable = "b_area", dicText = "area_name", dicCode = "id")
    @ApiModelProperty(value = "所属区划")
	@JsonFormat(shape = JsonFormat.Shape.STRING)
    private java.lang.Integer areaId;
	/**市局小区编码*/
	@Excel(name = "市局小区编码", width = 15)
    @ApiModelProperty(value = "市局小区编码")
    private java.lang.String ssxqbm;
	/**市局停车场编码*/
	@Excel(name = "市局停车场编码", width = 15)
    @ApiModelProperty(value = "市局停车场编码")
    private java.lang.String tccbh;
	/**政法委小区标识*/
	@Excel(name = "政法委小区标识", width = 15)
    @ApiModelProperty(value = "政法委小区标识")
    private java.lang.String ztxqbm;
	/**辖区公安机构*/
	@Excel(name = "辖区公安机构", width = 15)
    @ApiModelProperty(value = "辖区公安机构")
    private java.lang.String psNo;
	/**公安机构标识*/
	@Excel(name = "公安机构标识", width = 15)
    @ApiModelProperty(value = "公安机构标识")
    private java.lang.String psLevel;
	/**小区类别*/
	@Excel(name = "小区类别", width = 15, dicCode = "town_type")
	@Dict(dicCode = "town_type")
    @ApiModelProperty(value = "小区类别")
    private java.lang.String townType;
	/**小区编码*/
	@Excel(name = "小区编码", width = 15)
    @ApiModelProperty(value = "小区编码")
    private java.lang.String townNo;
	/**小区名称*/
	@Excel(name = "小区名称", width = 15)
    @ApiModelProperty(value = "小区名称")
    private java.lang.String townName;
	/**小区门牌号*/
	@Excel(name = "小区门牌号", width = 15)
    @ApiModelProperty(value = "小区门牌号")
    private java.lang.String townSn;
	/**地址标准码*/
	@Excel(name = "地址标准码", width = 15)
    @ApiModelProperty(value = "地址标准码")
    private java.lang.String addrCode;
	/**ZONE编码*/
	@Excel(name = "ZONE编码", width = 15)
    @ApiModelProperty(value = "ZONE编码")
    private java.lang.String zoneBm;
	/**数据归属单位代码*/
	@Excel(name = "数据归属单位代码", width = 15)
    @ApiModelProperty(value = "数据归属单位代码")
    private java.lang.String gajgjgdm;
	/**数据归属单位名称*/
	@Excel(name = "数据归属单位名称", width = 15)
    @ApiModelProperty(value = "数据归属单位名称")
    private java.lang.String gajgjgmc;
	/**警务区编号*/
	@Excel(name = "警务区编号", width = 15)
    @ApiModelProperty(value = "警务区编号")
    private java.lang.String jwwgdm;
	/**警务区名称*/
	@Excel(name = "警务区名称", width = 15)
    @ApiModelProperty(value = "警务区名称")
    private java.lang.String jwwgmc;
	/**地名编号*/
	@Excel(name = "地名编号", width = 15)
    @ApiModelProperty(value = "地名编号")
    private java.lang.String dmdm;
	/**地名*/
	@Excel(name = "地名", width = 15)
    @ApiModelProperty(value = "地名")
    private java.lang.String dmmc;
	/**负责人姓名*/
	@Excel(name = "负责人姓名", width = 15)
    @ApiModelProperty(value = "负责人姓名")
    private java.lang.String leadName;
	/**负责人电话*/
	@Excel(name = "负责人电话", width = 15)
    @ApiModelProperty(value = "负责人电话")
    private java.lang.String leadPhone;
	/**经度*/
	@Excel(name = "经度", width = 15)
    @ApiModelProperty(value = "经度")
    private java.lang.String mapx;
	/**纬度*/
	@Excel(name = "纬度", width = 15)
    @ApiModelProperty(value = "纬度")
    private java.lang.String mapy;
	/**边界点*/
	@Excel(name = "边界点", width = 15)
    @ApiModelProperty(value = "边界点")
    private java.lang.String points;
	/**附件文件*/
	@Excel(name = "附件文件", width = 15)
    @ApiModelProperty(value = "附件文件")
    private java.lang.String resFiles;
	/**附件名称*/
	@Excel(name = "附件名称", width = 15)
    @ApiModelProperty(value = "附件名称")
    private java.lang.String resNames;
	/**市局上报标识*/
	@Excel(name = "市局上报标识", width = 15, dicCode = "report_status")
	@Dict(dicCode = "report_status")
    @ApiModelProperty(value = "市局上报标识")
    private java.lang.String tcStatus;
	/**政法委上报标识*/
	@Excel(name = "政法委上报标识", width = 15, dicCode = "report_status")
	@Dict(dicCode = "report_status")
    @ApiModelProperty(value = "政法委上报标识")
    private java.lang.String ztStatus;
	/**闽清上报标识*/
	@Excel(name = "闽清上报标识", width = 15, dicCode = "report_status")
	@Dict(dicCode = "report_status")
    @ApiModelProperty(value = "闽清上报标识")
    private java.lang.String mqStatus;
	/**状态(0无效1有效)*/
	@Excel(name = "状态(0无效1有效)", width = 15, dicCode = "common_status")
	@Dict(dicCode = "common_status")
    @ApiModelProperty(value = "状态(0无效1有效)")
    private java.lang.String status;
	/**删除标志(0存在1删除)*/
	@Dict(dicCode = "del_flag")
    @ApiModelProperty(value = "删除标志(0存在1删除)")
    @TableLogic
    private java.lang.String delFlag;
	/**创建者*/
    @ApiModelProperty(value = "创建者")
    private java.lang.String createBy;
	/**创建时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建时间")
    private java.util.Date createTime;
	/**更新者*/
    @ApiModelProperty(value = "更新者")
    private java.lang.String updateBy;
	/**更新时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新时间")
    private java.util.Date updateTime;
	/**备注*/
	@Excel(name = "备注", width = 15)
    @ApiModelProperty(value = "备注")
    private java.lang.String remark;
}
