package org.jeecg.modules.city.service;

import org.jeecg.modules.city.entity.Person;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 人员
 * @Author: jeecg-boot
 * @Date:   2023-12-16
 * @Version: V1.0
 */
public interface IPersonService extends IService<Person> {

}
