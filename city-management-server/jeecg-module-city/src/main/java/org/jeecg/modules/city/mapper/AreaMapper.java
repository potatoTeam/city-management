package org.jeecg.modules.city.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.jeecg.common.system.vo.SelectTreeModel;
import org.jeecg.modules.city.entity.Area;
import org.jeecg.modules.city.model.dto.AreaDto;
import org.jeecg.modules.city.model.req.QueryRootListReq;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 行政区划
 * @Author: jeecg-boot
 * @Date:   2023-12-12
 * @Version: V1.0
 */
public interface AreaMapper extends BaseMapper<Area> {

	/**
	 * 编辑节点状态
	 * @param id
	 * @param status
	 */
	void updateTreeNodeStatus(@Param("id") String id,@Param("status") String status);

	/**
	 * 【vue3专用】根据父级ID查询树节点数据
	 *
	 * @param pid
	 * @param query
	 * @return
	 */
	List<SelectTreeModel> queryListByPid(@Param("pid") String pid, @Param("query") Map<String, String> query);

	
	List<AreaDto> getAreaList(QueryRootListReq req);

}
