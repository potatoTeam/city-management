package org.jeecg.modules.city.service.impl;

import org.jeecg.modules.city.entity.Person;
import org.jeecg.modules.city.mapper.PersonMapper;
import org.jeecg.modules.city.service.IPersonService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 人员
 * @Author: jeecg-boot
 * @Date:   2023-12-16
 * @Version: V1.0
 */
@Service
public class PersonServiceImpl extends ServiceImpl<PersonMapper, Person> implements IPersonService {

}
